# Scaling the Cascades -- RTL Design

This documentation provides insight into the RTL infrastructure used for the paper "Scaling the Cascades".

>    *Ananda Samajdar, Tushar Garg, Tushar Krishna, and Nachiket Kapre*

>    [**"Scaling the Cascades: Interconnect-aware FPGA implementation of Machine Learning problems"**](https://nachiket.github.io/publications/stc_fpl-2019.pdf),

>    International Conference on Field-Programmable Logic and Applications, Sep 2019 

To get started with this repository, please check it out as follows:
```zsh
git clone gitlab@git.uwaterloo.ca:watcag-public/fpga-cascades-rtl.git
TOP=`pwd`/fpga-cascades-rtl
cd $TOP
```

## Hardware Organization

The respository contains the following structure:
```zsh
fpga-cascades-rtl
├───sim                     # Wrappers for functional simulation of single blocks.
├───synth/dsp_conv_block    # Single convolution block
├───synth/dsp_conv_tile     # A repeating tile for convolution
├───synth/dsp_conv          # Full-chip convolution design
├───synth/dsp_mm_block      # Single matrix-multiplication block
├───synth/dsp_mm_tile       # A repeating tile for matrix-multiplication
└───synth/dsp_mm            # Full-chip matrix-multiplication design
```

## Simulation

We provide a wrapper for simulating the individual blocks of the design on simple examples to demonstrate correctness. 


**Convolution** The top module for simulation is `dsp_conv_top.v` which uses `dsp_conv.v`. 
We load the kernel BRAM `k_pixel_0.hex` file, the URAM with `new_pixel_4.hex` file.

To run the simulation:
```zsh
$ make dspconv_top
```
This will simulate this design with `verilator` and generate the GtkWave waveform file in the `bin` folder.

**Matrix-Multiplication**  The top module for simulation is `dsp_mm_top.v` which uses `dsp_mm.v`. 
There are 9 BRAMs in this design which we load with hex files named -- `new_pixel_6...14.hex`. 
We initialize the URAM with `new_pixel_5.hex` and the output URAM with `new_pixel_15.hex` to simulate the read-modify write functionality. 
For the sake of simulation simplicity, the URAM simulation model is performed with a BRAM instead.

To run the simulation:
```zsh
$ make dspmm_top
```

User should use `make dspmm_top` to simulate this design which will generate the gtkwave waveform file in the `bin` folder.


## FPGA Implementation
=========
FPGA implementation of the designs targets Xilinx Ultrascale VU37P device with a -3 speed grade. 
We generate FPGA mappings for both convolution and matrix-multiplication overlays at `tile`, `block`, and `chip` level.
We provide a Vivado Tcl script to automate DCP file generation of placed and routed designs. 
This can then be used to produce a bitstream. We provide a manually constrained XDC file that carefully maps the DSP48, RAMB18, and URAM288 blocks across the entire chip. 

To compile the design for convolution you can run the provided Tcl script, and revisit the build computer after 7-8 hours.
```zsh
$ source dsp_conv_chip.tcl
```

To compile the design for matrix multiplication you can run the provided Tcl script, and revisit the build computer after 7-8 hours.
```zsh
$ source dsp_mm_chip.tcl
```

You can also try to compile single block or tile as well with appropriate Tcl scripts in their respective folders.

## LICENSE

MIT License

Copyright (c) 2019 Tushar Garg, Nachiket Kapre

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
