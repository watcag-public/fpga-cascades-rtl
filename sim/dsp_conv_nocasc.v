module dsp_conv_nocasc #(
	parameter A_W = 14,
	parameter M_W = 18,
        parameter NUMBER_OF_REG = 1,
	parameter URAM_D_W = 48,
	parameter URAM_A_W = 14
)
(
input clk,
input rst,
input ce,
input ce_dsp,
input ce_b_in,
input [A_W-1:0] b1_wr_addr,
input           b1_wr_en,
input [A_W-1:0] b2_wr_addr,
input           b2_wr_en,
input [A_W-1:0] b3_wr_addr,
input           b3_wr_en,
input [A_W-1:0] b1_rd_addr,
input [A_W-1:0] b2_rd_addr,
input [A_W-1:0] b3_rd_addr,
input [A_W-1:0] rdaddr_b,
input [15:0]    data_in,

output reg [15:0] data_out
);

reg                      ce_b;
reg                      ce_b_1;
reg                      ce_b_2;
reg [M_W-1:0]            rd_data_b2_r1;
reg [M_W-1:0]            rd_data_b2_r2;
reg [M_W-1:0]            rd_data_b2_r3;
reg [M_W-1:0]            rd_data_b2_r4;
reg [M_W-1:0]            rd_data_b2_r5;
reg [M_W-1:0]            rd_data_b3_r1;
reg [M_W-1:0]            rd_data_b3_r2;
reg [M_W-1:0]            rd_data_b3_r3;
reg [M_W-1:0]            rd_data_b3_r4;
reg [M_W-1:0]            rd_data_b3_r5;
reg [M_W-1:0]            rd_data_b3_r6;
reg [M_W-1:0]            rd_data_b3_r7;
reg [M_W-1:0]            rd_data_b3_r8;
reg signed [47:0]        final_accumulation;
reg [47:0]               p8_reg;
reg                      ce_dsp_1;
reg                      ce_dsp_2;
reg [2:0]                ce_tmp_r1;
reg [2:0]                ce_tmp_r2;
reg [2:0]                ce_tmp_r3;
reg [2:0]                ce_tmp_r4;
reg [2:0]                ce_tmp_r5;
reg [2:0]                ce_a0;
reg [2:0]                ce_a0_r1;
reg [2:0]                ce_a0_r2;
reg [2:0]                ce_a1;
reg [2:0]                ce_a1_r1;
reg [2:0]                ce_a1_r2;
reg [2:0]                ce_a2;
reg [29:0]               acin0 [1:3];
reg [29:0]               acin01_r1;
reg [29:0]               acin02_r1;
reg [29:0]               acin1 [1:3];
reg [29:0]               acin11_r1;
reg [29:0]               acin12_r1;
reg [29:0]               acin2 [1:3];
reg [29:0]               acin21_r1;
reg [29:0]               acin22_r1;
reg [17:0]               bcin0 [1:3];
reg [17:0]               bcin1 [1:3];
reg [17:0]               bcin2 [1:3];
reg  [M_W-1:0]           dsp_a1;
reg  [M_W-1:0]           dsp_a2;
reg signed [47:0]        cascade_op;
reg signed [47:0]        separate_dsp_op;
reg [M_W-1:0]           dsp_a0_r;
reg [M_W-1:0]           dsp_k0_r;
reg [M_W-1:0]           dsp_a0_1;
reg [M_W-1:0]           dsp_k0_1;
reg [M_W-1:0]           dsp_a0_2;
reg [M_W-1:0]           dsp_k0_2;
reg  [47:0]              new_acc_fab;
reg  [47:0]              new_acc1;

wire [M_W-1:0]           casc_data_b1;
wire [M_W-1:0]           casc_data_b2;
wire [M_W-1:0]           casc_data_b3;
wire [M_W-1:0]           rd_data_b2;
wire [M_W-1:0]           rd_data_b3;
wire [47:0]              p [0:9];
wire [47:0]              p_out [0:1];
wire [29:0]              acin00;
wire [29:0]              acin10;
wire [29:0]              acin20;
wire [17:0]              bcin00;
wire [M_W-1:0]           dsp_a0;
wire [M_W-1:0]           dsp_k0;
wire [47:0]              new_acc;
wire [2:0]               ce_tmp;
//////////////////////// optional register /////////////////
generate if (NUMBER_OF_REG == 1) begin : a0k0_1
  always@(posedge clk) begin
    dsp_a0_r <= dsp_a0;
    dsp_k0_r <= dsp_k0;
    ce_b     <= ce_b_in;
    dsp_a1    <= rd_data_b2_r3;
    dsp_a2    <= rd_data_b3_r6;
    ce_tmp_r1 <= ce_tmp;
  end
end endgenerate

generate if (NUMBER_OF_REG == 2) begin : a0k0_2
  always@(posedge clk) begin
    dsp_a0_1 <= dsp_a0;
    dsp_k0_1 <= dsp_k0;
    dsp_a0_r <= dsp_a0_1;
    dsp_k0_r <= dsp_k0_1;
    ce_b_1   <= ce_b_in;
    ce_b     <= ce_b_1;
    rd_data_b2_r4  <= rd_data_b2_r3;
    dsp_a1    <= rd_data_b2_r4;
    rd_data_b3_r7 <= rd_data_b3_r6;
    dsp_a2        <= rd_data_b3_r7;
    ce_tmp_r3 <= ce_tmp;
    ce_tmp_r2 <= ce_tmp_r3;
    ce_tmp_r1 <= ce_tmp_r2;
  end
end endgenerate

generate if (NUMBER_OF_REG == 3) begin : a0k0_3
  always@(posedge clk) begin
    dsp_a0_1 <= dsp_a0;
    dsp_k0_1 <= dsp_k0;
    dsp_a0_2 <= dsp_a0_1;
    dsp_k0_2 <= dsp_k0_1;
    dsp_a0_r <= dsp_a0_2;
    dsp_k0_r <= dsp_k0_2;
    ce_b_1   <= ce_b_in;
    ce_b_2   <= ce_b_1;
    ce_b     <= ce_b_2;
    rd_data_b2_r4  <= rd_data_b2_r3;
    rd_data_b2_r5  <= rd_data_b2_r4;
    dsp_a1    <= rd_data_b2_r5;
    rd_data_b3_r7 <= rd_data_b3_r6;
    rd_data_b3_r8 <= rd_data_b3_r7;
    dsp_a2  <= rd_data_b3_r8;
    ce_tmp_r5 <= ce_tmp;
    ce_tmp_r4 <= ce_tmp_r5;
    ce_tmp_r3 <= ce_tmp_r4;
    ce_tmp_r2 <= ce_tmp_r3;
    ce_tmp_r1 <= ce_tmp_r2;
  end
end endgenerate
//////////////////////////////////////////////////////////
always@(posedge clk) begin
end


		RAMB18E2 #(
			.DOA_REG(1),.DOB_REG(1),
			.CASCADE_ORDER_A("NONE"),.CASCADE_ORDER_B("NONE"),
			.CLOCK_DOMAINS("COMMON"),
			.WRITE_WIDTH_A(18), .WRITE_WIDTH_B(18),
			.READ_WIDTH_A(18), .READ_WIDTH_B(18))
        	bram_inst_rdc1 (
	                .ADDRARDADDR(b1_rd_addr),
        	        .ADDRBWRADDR(b1_wr_addr),
	                .ADDRENA(1'b1),
	                .ADDRENB(1'b1),
	                .WEA({2{1'b0}}),
	                .WEBWE({4{b1_wr_en}}),
	
	                // horizontal links
	                .DINBDIN(data_in[15:0]), 
	                .DINPBDINP(2'b00),
	                .DOUTADOUT(dsp_a0[15:0]), 
	                .DOUTPADOUTP(dsp_a0[17:16]), 
	
	                // clocking, reset, and enable control
	                .CLKARDCLK(clk),
	                .CLKBWRCLK(clk),
	
	                .ENARDEN(ce),
	                .ENBWREN(ce),
	                .REGCEAREGCE(ce),
	                .REGCEB(ce),
	
	                .RSTRAMARSTRAM(rst),
	                .RSTRAMB(rst),
	                .RSTREGARSTREG(rst),
	                .RSTREGB(rst)
	        );

assign casc_data_b1 = dsp_a0;

		RAMB18E2 #(
			.DOA_REG(1),.DOB_REG(1),
			.CASCADE_ORDER_A("NONE"),.CASCADE_ORDER_B("NONE"),
			.CLOCK_DOMAINS("COMMON"),
			.WRITE_WIDTH_A(18), .WRITE_WIDTH_B(18),
			.READ_WIDTH_A(18), .READ_WIDTH_B(18))
        	bram_inst_rdc2 (
	                .ADDRARDADDR(b2_rd_addr),
        	        .ADDRBWRADDR(b2_wr_addr),
	                .ADDRENA(1'b1),
	                .ADDRENB(1'b1),
	                .WEA({2{1'b0}}),
	                .WEBWE({4{b2_wr_en}}),
	
	                // horizontal links
	                .DINBDIN(casc_data_b1[15:0]), 
	                .DINPBDINP(casc_data_b1[17:16]), 
	                .DOUTADOUT(rd_data_b2[15:0]), 
	                .DOUTPADOUTP(rd_data_b2[17:16]), 
	
	                // clocking, reset, and enable control
	                .CLKARDCLK(clk),
	                .CLKBWRCLK(clk),
	
	                .ENARDEN(ce),
	                .ENBWREN(ce),
	                .REGCEAREGCE(ce),
	                .REGCEB(ce),
	
	                .RSTRAMARSTRAM(rst),
	                .RSTRAMB(rst),
	                .RSTREGARSTREG(rst),
	                .RSTREGB(rst)
	        );

assign casc_data_b2 = rd_data_b2;

		RAMB18E2 #(
			.DOA_REG(1),.DOB_REG(1),
			.CASCADE_ORDER_A("NONE"),.CASCADE_ORDER_B("NONE"),
			.CLOCK_DOMAINS("COMMON"),
			.WRITE_WIDTH_A(18), .WRITE_WIDTH_B(18),
			.READ_WIDTH_A(18), .READ_WIDTH_B(18))
        	bram_inst_rdc3 (
	                .ADDRARDADDR(b3_rd_addr),
        	        .ADDRBWRADDR(b3_wr_addr),
	                .ADDRENA(1'b1),
	                .ADDRENB(1'b1),
	                .WEA({2{1'b0}}),
	                .WEBWE({4{b3_wr_en}}),
	
	                // horizontal links
	                .DINBDIN(casc_data_b2[15:0]), 
	                .DINPBDINP(casc_data_b2[17:16]), 
	                .DOUTADOUT(rd_data_b3[15:0]), 
	                .DOUTPADOUTP(rd_data_b3[17:16]), 
	
	                // clocking, reset, and enable control
	                .CLKARDCLK(clk),
	                .CLKBWRCLK(clk),
	
	                .ENARDEN(ce),
	                .ENBWREN(ce),
	                .REGCEAREGCE(ce),
	                .REGCEB(ce),
	
	                .RSTRAMARSTRAM(rst),
	                .RSTRAMB(rst),
	                .RSTREGARSTREG(rst),
	                .RSTREGB(rst)
	        );

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

always@(posedge clk) begin
  if (rst) acin0[1] <= 30'd0;
  else if (ce_a0[0]) acin0[1] <= acin00;
  
  if (rst) begin
    acin01_r1 <= 30'd0;
    acin0[2]  <= 30'd0;
  end
  else if (ce_a0[1]) begin
    acin01_r1 <= acin0[1];
    acin0[2]  <= acin01_r1;
  end

  if (rst) begin
    acin02_r1 <= 30'd0;
    acin0[3]  <= 30'd0;
  end else if (ce_a0[2]) begin
    acin02_r1 <= acin0[2];
    acin0[3] <= acin02_r1;
  end

  if (rst) acin1[1] <= 30'd0;
  else if (ce_a1[0]) acin1[1] <= acin10;
  
  if (rst) begin
    acin11_r1 <= 30'd0;
    acin1[2]  <= 30'd0;
  end
  else if (ce_a1[1]) begin
    acin11_r1 <= acin1[1];
    acin1[2]  <= acin11_r1;
  end

  if (rst) begin
    acin12_r1 <= 30'd0;
    acin1[3]  <= 30'd0;
  end else if (ce_a1[2]) begin
    acin12_r1 <= acin1[2];
    acin1[3] <= acin12_r1;
  end

  if (rst) acin2[1] <= 30'd0;
  else if (ce_a2[0]) acin2[1] <= acin20;
  
  if (rst) begin
    acin21_r1 <= 30'd0;
    acin2[2]  <= 30'd0;
  end
  else if (ce_a2[1]) begin
    acin21_r1 <= acin2[1];
    acin2[2]  <= acin21_r1;
  end

  if (rst) begin
    acin22_r1 <= 30'd0;
    acin2[3]  <= 30'd0;
  end else if (ce_a2[2]) begin
    acin22_r1 <= acin2[2];
    acin2[3] <= acin22_r1;
  end
end

always@(posedge clk) begin
  if (rst) begin
    bcin0[1] <= 18'd0;
    bcin0[2] <= 18'd0;
    bcin0[3] <= 18'd0;
    bcin1[1] <= 18'd0;
    bcin1[2] <= 18'd0;
    bcin1[3] <= 18'd0;
    bcin2[1] <= 18'd0;
    bcin2[2] <= 18'd0;
    bcin2[3] <= 18'd0;
  end else if (ce_b) begin
    bcin0[1] <= bcin00;
    bcin0[2] <= bcin0[1];
    bcin0[3] <= bcin0[2];
    bcin1[1] <= bcin0[3];
    bcin1[2] <= bcin1[1];
    bcin1[3] <= bcin1[2];
    bcin2[1] <= bcin1[3];
    bcin2[2] <= bcin2[1];
    bcin2[3] <= bcin2[2];
  end
end

always@(posedge clk) begin
    ce_dsp_1 <= ce_dsp;
    ce_dsp_2 <= ce_dsp_1;
end

assign ce_tmp = {ce_dsp_2, ce_dsp_1, ce_dsp};

//registering ce_a for 11 clock cycle
always@(posedge clk) begin
  ce_a0     <= ce_tmp_r1;
  
  ce_a0_r1 <= ce_a0;
  ce_a0_r2 <= ce_a0_r1;
  ce_a1    <= ce_a0_r2;

  ce_a1_r1 <= ce_a1;
  ce_a1_r2 <= ce_a1_r1;
  ce_a2    <= ce_a1_r2;
end

assign acin00 = {{3{1'b0}},dsp_a0_r[15:8],{11{1'b0}},dsp_a0_r[7:0]};
assign bcin00 = {{10{dsp_k0_r[7]}},dsp_k0_r[7:0]};
assign acin10 = {{3{1'b0}},dsp_a1[15:8],{11{1'b0}},dsp_a1[7:0]};
assign acin20 = {{3{1'b0}},dsp_a2[15:8],{11{1'b0}},dsp_a2[7:0]};

always@(posedge clk) begin
  rd_data_b2_r1 <= rd_data_b2;
  rd_data_b2_r2 <= rd_data_b2_r1;
  rd_data_b2_r3 <= rd_data_b2_r2;
  rd_data_b3_r1 <= rd_data_b3;
  rd_data_b3_r2 <= rd_data_b3_r1;
  rd_data_b3_r3 <= rd_data_b3_r2;
  rd_data_b3_r4 <= rd_data_b3_r3;
  rd_data_b3_r5 <= rd_data_b3_r4;
  rd_data_b3_r6 <= rd_data_b3_r5;
end

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// DSP 0 ///////////////////////////////////////

//kernel ram

		RAMB18E2 #(
			.DOA_REG(1),.DOB_REG(1),
			.CASCADE_ORDER_A("NONE"),.CASCADE_ORDER_B("NONE"),
			.CLOCK_DOMAINS("COMMON"),

			.INIT_FILE("k_pixels_3.hex"),
			.WRITE_WIDTH_A(18), .WRITE_WIDTH_B(18),
			.READ_WIDTH_A(18), .READ_WIDTH_B(18))
        	bram_inst_rd_kernel0 (
	                .ADDRARDADDR(rdaddr_b),
        	        .ADDRBWRADDR(),
	                .ADDRENA(1'b1),
	                .ADDRENB(1'b1),
	                .WEA({2{1'b0}}),
	                .WEBWE({4{1'b0}}),
	
	                // horizontal links
	                .DOUTADOUT(dsp_k0[15:0]), 
	                .DOUTPADOUTP(dsp_k0[17:16]), 
	                .DOUTBDOUT(), 
	                .DOUTPBDOUTP(), 
	
	                // clocking, reset, and enable control
	                .CLKARDCLK(clk),
	                .CLKBWRCLK(clk),
	
	                .ENARDEN(ce),
	                .ENBWREN(ce),
	                .REGCEAREGCE(ce),
	                .REGCEB(ce),
	
	                .RSTRAMARSTRAM(rst),
	                .RSTRAMB(rst),
	                .RSTREGARSTREG(rst),
	                .RSTREGB(rst)
	        );

genvar i;
generate
for (i=0; i<3; i=i+1) begin : dsp_chain0
  if (i==0) begin
    DSP48E2 #(
	.AMULTSEL("A"),.BMULTSEL("B"),
        .A_INPUT("DIRECT"),.B_INPUT("DIRECT"),
        .AREG(1))
    dsp_inst(
	.A(acin00),
	.B(bcin00),
	.P(),
	.C({{13{bcin0[i+1][7]}},bcin0[i+1][7:0],{27{1'b0}}}),
	.PCIN(),
	.PCOUT(p[i+1]),

	// control DSP
	.ALUMODE(4'b0000),
	.INMODE(5'b00100),
	.OPMODE({acin0[i+1][26],acin0[i+1][26],7'b0000101}), 

	// clocking reset and enables.. control signals
	.CLK(clk),
	.RSTA(rst),
	.RSTALLCARRYIN(rst),
	.RSTALUMODE(rst),
	.RSTB(rst),
	.RSTC(rst),
	.RSTCTRL(rst),
	.RSTD(rst),
	.RSTINMODE(rst),
	.RSTM(rst),
	.RSTP(rst),
	.CEA1(ce_a0[i]),
	.CEA2(ce_a0[i]),
	.CEAD(ce),
	.CEALUMODE(ce),
	.CEB1(ce_b),
	.CEB2(ce_b),
	.CEC(ce),
	.CECARRYIN(ce),
	.CECTRL(ce),
	.CED(ce),
	.CEINMODE(ce),
	.CEM(ce),
	.CEP(ce)
);
  end else begin
    DSP48E2 #(
	.AMULTSEL("A"),.BMULTSEL("B"),
        .A_INPUT("DIRECT"),.B_INPUT("DIRECT"),
        .AREG(2))
    dsp_inst(
	.A(acin0[i]),
	.B(bcin0[i]),
	.P(),
	.C({{13{bcin0[i+1][7]}},bcin0[i+1][7:0],{27{1'b0}}}),
	.PCIN(p[i]),
	.PCOUT(p[i+1]),

	// control DSP
	.ALUMODE(4'b0000),
	.INMODE(5'b00100),
	.OPMODE({acin0[i+1][26],acin0[i+1][26],7'b0010101}), 

	// clocking reset and enables.. control signals
	.CLK(clk),
	.RSTA(rst),
	.RSTALLCARRYIN(rst),
	.RSTALUMODE(rst),
	.RSTB(rst),
	.RSTC(rst),
	.RSTCTRL(rst),
	.RSTD(rst),
	.RSTINMODE(rst),
	.RSTM(rst),
	.RSTP(rst),
	.CEA1(ce_a0[i]),
	.CEA2(ce_a0[i]),
	.CEAD(ce),
	.CEALUMODE(ce),
	.CEB1(ce_b),
	.CEB2(ce_b),
	.CEC(ce),
	.CECARRYIN(ce),
	.CECTRL(ce),
	.CED(ce),
	.CEINMODE(ce),
	.CEM(ce),
	.CEP(ce)
);
  end
end
endgenerate



//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// DSP 1 ///////////////////////////////////////

genvar j;
generate
for (j=0; j<3; j=j+1) begin : dsp_chain1
  if (j==0) begin
    DSP48E2 #(
	.AMULTSEL("A"),.BMULTSEL("B"),
        .A_INPUT("DIRECT"),.B_INPUT("DIRECT"),
        .AREG(1))
dsp_inst(
	.A(acin10),
	.B(bcin0[j+3]),
	.P(),
	.C({{13{bcin1[j+1][7]}},bcin1[j+1][7:0],{27{1'b0}}}),
	.PCIN(p[j+3]),
	.PCOUT(p[j+4]),

	// control DSP
	.ALUMODE(4'b0000),
	.INMODE(5'b00100),
	//.OPMODE({acin1_reg[j][26],acin1_reg[j][26],7'b0010101}), 
	.OPMODE({acin1[j+1][26],acin1[j+1][26],7'b0010101}), 

	// clocking reset and enables.. control signals
	.CLK(clk),
	.RSTA(rst),
	.RSTALLCARRYIN(rst),
	.RSTALUMODE(rst),
	.RSTB(rst),
	.RSTC(rst),
	.RSTCTRL(rst),
	.RSTD(rst),
	.RSTINMODE(rst),
	.RSTM(rst),
	.RSTP(rst),
	.CEA1(ce_a1[j]),
	.CEA2(ce_a1[j]),
	.CEAD(ce),
	.CEALUMODE(ce),
	.CEB1(ce_b),
	.CEB2(ce_b),
	.CEC(ce),
	.CECARRYIN(ce),
	.CECTRL(ce),
	.CED(ce),
	.CEINMODE(ce),
	.CEM(ce),
	.CEP(ce)
);
  end else begin
        DSP48E2 #(
	.AMULTSEL("A"),.BMULTSEL("B"),
        .A_INPUT("DIRECT"),.B_INPUT("DIRECT"),
        .AREG(2))
dsp_inst(
	.A(acin1[j]),
	.B(bcin1[j]),
	.P(),
	.C({{13{bcin1[j+1][7]}},bcin1[j+1][7:0],{27{1'b0}}}),
	.PCIN(p[j+3]),
	.PCOUT(p[j+4]),

	// control DSP
	.ALUMODE(4'b0000),
	.INMODE(5'b00100),
	.OPMODE({acin1[j+1][26],acin1[j+1][26],7'b0010101}), 

	// clocking reset and enables.. control signals
	.CLK(clk),
	.RSTA(rst),
	.RSTALLCARRYIN(rst),
	.RSTALUMODE(rst),
	.RSTB(rst),
	.RSTC(rst),
	.RSTCTRL(rst),
	.RSTD(rst),
	.RSTINMODE(rst),
	.RSTM(rst),
	.RSTP(rst),
	.CEA1(ce_a1[j]),
	.CEA2(ce_a1[j]),
	.CEAD(ce),
	.CEALUMODE(ce),
	.CEB1(ce_b),
	.CEB2(ce_b),
	.CEC(ce),
	.CECARRYIN(ce),
	.CECTRL(ce),
	.CED(ce),
	.CEINMODE(ce),
	.CEM(ce),
	.CEP(ce)
);
  end
end
endgenerate


//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// DSP 2 ///////////////////////////////////////

genvar k;
generate
for (k=0; k<2; k=k+1) begin : dsp_chain2
  if (k==0) begin
DSP48E2 #(
	.AMULTSEL("A"),.BMULTSEL("B"),
        .A_INPUT("DIRECT"),.B_INPUT("DIRECT"),
        .AREG(1))
dsp_inst(
	.A(acin20),
	.B(bcin1[k+3]),
	.P(p_out[k]),
	.C({{13{bcin2[k+1][7]}},bcin2[k+1][7:0],{27{1'b0}}}),
	.PCIN(p[k+6]),
	.PCOUT(p[k+7]),

	// control DSP
	.ALUMODE(4'b0000),
	.INMODE(5'b00100),
	//.OPMODE({acin2_reg[k][26],acin2_reg[k][26],7'b0010101}), 
	.OPMODE({acin2[k+1][26],acin2[k+1][26],7'b0010101}), 

	// clocking reset and enables.. control signals
	.CLK(clk),
	.RSTA(rst),
	.RSTALLCARRYIN(rst),
	.RSTALUMODE(rst),
	.RSTB(rst),
	.RSTC(rst),
	.RSTCTRL(rst),
	.RSTD(rst),
	.RSTINMODE(rst),
	.RSTM(rst),
	.RSTP(rst),
	.CEA1(ce_a2[k]),
	.CEA2(ce_a2[k]),
	.CEAD(ce),
	.CEALUMODE(ce),
	.CEB1(ce_b),
	.CEB2(ce_b),
	.CEC(ce),
	.CECARRYIN(ce),
	.CECTRL(ce),
	.CED(ce),
	.CEINMODE(ce),
	.CEM(ce),
	.CEP(ce)
);
  end else begin
    DSP48E2 #(
	.AMULTSEL("A"),.BMULTSEL("B"),
        .A_INPUT("DIRECT"),.B_INPUT("DIRECT"),
        .AREG(2))
dsp_inst(
	.A(acin2[k]),
	.B(bcin2[k]),
	.P(p_out[k]),
	.C({{13{bcin2[k+1][7]}},bcin2[k+1][7:0],{27{1'b0}}}),
	.PCIN(p[k+6]),
	.PCOUT(p[k+7]),

	// control DSP
	.ALUMODE(4'b0000),
	.INMODE(5'b00100),
	.OPMODE({acin2[k+1][26],acin2[k+1][26],7'b0010101}), 

	// clocking reset and enables.. control signals
	.CLK(clk),
	.RSTA(rst),
	.RSTALLCARRYIN(rst),
	.RSTALUMODE(rst),
	.RSTB(rst),
	.RSTC(rst),
	.RSTCTRL(rst),
	.RSTD(rst),
	.RSTINMODE(rst),
	.RSTM(rst),
	.RSTP(rst),
	.CEA1(ce_a2[k]),
	.CEA2(ce_a2[k]),
	.CEAD(ce),
	.CEALUMODE(ce),
	.CEB1(ce_b),
	.CEB2(ce_b),
	.CEC(ce),
	.CECARRYIN(ce),
	.CECTRL(ce),
	.CED(ce),
	.CEINMODE(ce),
	.CEM(ce),
	.CEP(ce)
);

  end
end
endgenerate

DSP48E2 #(
	.AMULTSEL("A"),.BMULTSEL("B"),
        .A_INPUT("DIRECT"),.B_INPUT("DIRECT"),
        .AREG(2))
dsp_inst8(
	.A(acin2[2]),
	.B(bcin2[2]),
	.P(p[9]),
	.C({{13{bcin2[3][7]}},bcin2[3][7:0],{27{1'b0}}}),
	.PCIN(48'd0),
	.PCOUT(),

	// control DSP
	.ALUMODE(4'b0000),
	.INMODE(5'b00100),
	.OPMODE({acin2[3][26],acin2[3][26],7'b0000101}), 

	// clocking reset and enables.. control signals
	.CLK(clk),
	.RSTA(rst),
	.RSTALLCARRYIN(rst),
	.RSTALUMODE(rst),
	.RSTB(rst),
	.RSTC(rst),
	.RSTCTRL(rst),
	.RSTD(rst),
	.RSTINMODE(rst),
	.RSTM(rst),
	.RSTP(rst),
	.CEA1(ce_a2[2]),
	.CEA2(ce_a2[2]),
	.CEAD(ce),
	.CEALUMODE(ce),
	.CEB1(ce_b),
	.CEB2(ce_b),
	.CEC(ce),
	.CECARRYIN(ce),
	.CECTRL(ce),
	.CED(ce),
	.CEINMODE(ce),
	.CEM(ce),
	.CEP(ce)
);

always@(posedge clk) begin
  p8_reg <= p_out[1];
end

always@(posedge clk) begin
  cascade_op      <= {{5{p8_reg[37]}},p8_reg[37:19],{5{p8_reg[18]}},p8_reg[18:0]};
  separate_dsp_op <= {{5{p[9][37]}},p[9][37:19],{5{p[9][18]}},p[9][18:0]};
  new_acc_fab     <= cascade_op + separate_dsp_op;
  new_acc1        <= new_acc_fab + {23'd0,new_acc_fab[23],6'd0};
  data_out        <= {new_acc1[31:24],new_acc1[7:0]}; //truncating output to 16b
end
endmodule
