#include "Vdsp_conv_top.h"
#include "verilated.h"
#ifdef TRACE
#include <verilated_vcd_c.h>
#endif

#include <iostream>
using namespace std;

vluint64_t main_time = 0;       // Current simulation time
// This is a 64-bit integer to reduce wrap over issues and
// allow modulus.  You can also use a double, if you wish.

double sc_time_stamp () {       // Called by $time in Verilog
	return main_time;           // converts to double, to match
	// what SystemC does
}

int main(int argc, char **argv, char **env) {
	Verilated::commandArgs(argc, argv);
	Vdsp_conv_top* top = new Vdsp_conv_top;
#ifdef TRACE
        Verilated::traceEverOn(true);
        VerilatedVcdC   *m_trace;
        m_trace = new VerilatedVcdC;
        top->trace(m_trace,99);
        m_trace->open("verilator.vcd");
#endif
	// TODO: add initialization stuff for generics and add a clock loop
	int time=0;
	bool clk=0;
	top->ce = 1;
	top->rst = 1;
	top->clk = 0;
	int M=5, N=5, K=3;
	while (time <= 1000) {  // run for 16 cycles
		top->rst = (time<2);
		top->clk = clk;
		top->eval(); 
		clk=!clk;
		time++;
#ifdef TRACE
                m_trace->dump(time);
#endif
	}
#ifdef TRACE
        m_trace->close();
#endif
	delete top;
	exit(0);
}
