#!/usr/bin/zsh
rm -f test.xdc
Y=`echo "$1 - 1" | bc -l`

dsp=(72 162 258 )
file1=(438 452 466 )


for i in {0..$Y}
do
	a=${dsp[$i+1]}
	e=${file1[$i+1]}
        echo "# $((i)) in separate loop" >> test.xdc
        echo "# $((a)), $((b)), $((c)), $((d)), $((e))" >> test.xdc
	echo "set_property LOC DSP48E2_X10Y$(( 0 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$(( 1 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$(( 2 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$(( 3 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$(( 4 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$(( 5 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$(( 6 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$(( 7 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$(( 8 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X10Y$(( 9 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((10 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((11 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((12 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((13 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((14 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((15 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((16 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((17 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X11Y$(( 0 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X11Y$(( 1 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$(( 2 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$(( 3 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$(( 4 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$(( 5 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$(( 6 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$(( 7 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$(( 8 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X11Y$(( 9 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((10 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((11 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((12 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((13 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((14 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((15 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((16 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((17 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_inst8}];" >> test.xdc

	echo "set_property LOC RAMB18_X4Y$((96*i+80))     [get_cells {name[$((e + 0))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((96*i+81))     [get_cells {name[$((e + 0))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((96*i+82))     [get_cells {name[$((e + 0))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((96*i+83))     [get_cells {name[$((e + 0))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((96*i+84))     [get_cells {name[$((e + 0))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((96*i+85))     [get_cells {name[$((e + 0))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((96*i+86))     [get_cells {name[$((e + 0))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((96*i+87))     [get_cells {name[$((e + 0))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((96*i+88))     [get_cells {name[$((e + 1))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((96*i+89))     [get_cells {name[$((e + 1))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((96*i+90))     [get_cells {name[$((e + 1))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((96*i+91))     [get_cells {name[$((e + 1))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((96*i+92))     [get_cells {name[$((e + 1))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((96*i+93))     [get_cells {name[$((e + 1))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((96*i+94))     [get_cells {name[$((e + 1))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((96*i+95))     [get_cells {name[$((e + 1))].dut/conv2/bram_inst_rdc4}];" >> test.xdc

	echo "set_property LOC URAM288_X0Y$((64*i+ 60))    [get_cells {name[$((e + 0))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X0Y$((64*i+ 61))    [get_cells {name[$((e + 0))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X0Y$((64*i+ 62))    [get_cells {name[$((e + 1))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X0Y$((64*i+ 63))    [get_cells {name[$((e + 1))].dut/uram_inst_rd}];" >> test.xdc

#######################################
	echo "set_property LOC DSP48E2_X28Y$(( 0 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X28Y$(( 1 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$(( 2 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$(( 3 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$(( 4 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$(( 5 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$(( 6 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$(( 7 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$(( 8 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X28Y$(( 9 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((10 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((11 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((12 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((13 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((14 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((15 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((16 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((17 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X29Y$(( 0 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X29Y$(( 1 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$(( 2 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$(( 3 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$(( 4 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$(( 5 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$(( 6 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$(( 7 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$(( 8 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X29Y$(( 9 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((10 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((11 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((12 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((13 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((14 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((15 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((16 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((17 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_inst8}];" >> test.xdc

	echo "set_property LOC RAMB18_X13Y$((96*i+80))     [get_cells {name[$((e + 2))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((96*i+81))     [get_cells {name[$((e + 2))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((96*i+82))     [get_cells {name[$((e + 2))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((96*i+83))     [get_cells {name[$((e + 2))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((96*i+84))     [get_cells {name[$((e + 2))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((96*i+85))     [get_cells {name[$((e + 2))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((96*i+86))     [get_cells {name[$((e + 2))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((96*i+87))     [get_cells {name[$((e + 2))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((96*i+88))     [get_cells {name[$((e + 3))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((96*i+89))     [get_cells {name[$((e + 3))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((96*i+90))     [get_cells {name[$((e + 3))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((96*i+91))     [get_cells {name[$((e + 3))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((96*i+92))     [get_cells {name[$((e + 3))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((96*i+93))     [get_cells {name[$((e + 3))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((96*i+94))     [get_cells {name[$((e + 3))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((96*i+95))     [get_cells {name[$((e + 3))].dut/conv2/bram_inst_rdc4}];" >> test.xdc

	echo "set_property LOC URAM288_X4Y$((64*i+ 44))    [get_cells {name[$((e + 2))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((64*i+ 45))    [get_cells {name[$((e + 2))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((64*i+ 46))    [get_cells {name[$((e + 3))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((64*i+ 47))    [get_cells {name[$((e + 3))].dut/uram_inst_rd}];" >> test.xdc
#######################################
	echo "set_property LOC DSP48E2_X26Y$(( 0 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X26Y$(( 1 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$(( 2 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$(( 3 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$(( 4 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$(( 5 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$(( 6 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$(( 7 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$(( 8 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X26Y$(( 9 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((10 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((11 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((12 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((13 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((14 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((15 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((16 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((17 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X27Y$(( 0 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X27Y$(( 1 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$(( 2 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$(( 3 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$(( 4 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$(( 5 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$(( 6 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$(( 7 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$(( 8 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X27Y$(( 9 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((10 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((11 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((12 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((13 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((14 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((15 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((16 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((17 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_inst8}];" >> test.xdc

	echo "set_property LOC RAMB18_X9Y$((96*i+64))     [get_cells {name[$((e + 4))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+65))     [get_cells {name[$((e + 4))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+66))     [get_cells {name[$((e + 4))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+67))     [get_cells {name[$((e + 4))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+68))     [get_cells {name[$((e + 4))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+69))     [get_cells {name[$((e + 4))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+70))     [get_cells {name[$((e + 4))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+71))     [get_cells {name[$((e + 4))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+72))     [get_cells {name[$((e + 5))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+73))     [get_cells {name[$((e + 5))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+74))     [get_cells {name[$((e + 5))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+75))     [get_cells {name[$((e + 5))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+76))     [get_cells {name[$((e + 5))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+77))     [get_cells {name[$((e + 5))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+78))     [get_cells {name[$((e + 5))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+79))     [get_cells {name[$((e + 5))].dut/conv2/bram_inst_rdc4}];" >> test.xdc

	echo "set_property LOC URAM288_X4Y$((64*i+ 48))    [get_cells {name[$((e + 4))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((64*i+ 49))    [get_cells {name[$((e + 4))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((64*i+ 50))    [get_cells {name[$((e + 5))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((64*i+ 51))    [get_cells {name[$((e + 5))].dut/uram_inst_rd}];" >> test.xdc
#######################################
	echo "set_property LOC DSP48E2_X24Y$(( 0 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X24Y$(( 1 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$(( 2 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$(( 3 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$(( 4 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$(( 5 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$(( 6 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$(( 7 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$(( 8 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X24Y$(( 9 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((10 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((11 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((12 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((13 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((14 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((15 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((16 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((17 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X25Y$(( 0 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X25Y$(( 1 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$(( 2 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$(( 3 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$(( 4 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$(( 5 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$(( 6 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$(( 7 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$(( 8 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X25Y$(( 9 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((10 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((11 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((12 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((13 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((14 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((15 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((16 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((17 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_inst8}];" >> test.xdc

	echo "set_property LOC RAMB18_X9Y$((96*i+80))     [get_cells {name[$((e + 6))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+81))     [get_cells {name[$((e + 6))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+82))     [get_cells {name[$((e + 6))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+83))     [get_cells {name[$((e + 6))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+84))     [get_cells {name[$((e + 6))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+85))     [get_cells {name[$((e + 6))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+86))     [get_cells {name[$((e + 6))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+87))     [get_cells {name[$((e + 6))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+88))     [get_cells {name[$((e + 7))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+89))     [get_cells {name[$((e + 7))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+90))     [get_cells {name[$((e + 7))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+91))     [get_cells {name[$((e + 7))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+92))     [get_cells {name[$((e + 7))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+93))     [get_cells {name[$((e + 7))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+94))     [get_cells {name[$((e + 7))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((96*i+95))     [get_cells {name[$((e + 7))].dut/conv2/bram_inst_rdc4}];" >> test.xdc

	echo "set_property LOC URAM288_X4Y$((64*i+ 52))    [get_cells {name[$((e + 6))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((64*i+ 53))    [get_cells {name[$((e + 6))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((64*i+ 54))    [get_cells {name[$((e + 7))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((64*i+ 55))    [get_cells {name[$((e + 7))].dut/uram_inst_rd}];" >> test.xdc



#######################################
	echo "set_property LOC DSP48E2_X22Y$(( 0 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X22Y$(( 1 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$(( 2 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$(( 3 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$(( 4 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$(( 5 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$(( 6 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$(( 7 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$(( 8 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X22Y$(( 9 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((10 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((11 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((12 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((13 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((14 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((15 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((16 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((17 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X23Y$(( 0 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X23Y$(( 1 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$(( 2 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$(( 3 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$(( 4 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$(( 5 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$(( 6 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$(( 7 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$(( 8 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X23Y$(( 9 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((10 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((11 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((12 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((13 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((14 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((15 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((16 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((17 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_inst8}];" >> test.xdc

	echo "set_property LOC RAMB18_X8Y$((96*i+80))     [get_cells {name[$((e + 8))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((96*i+81))     [get_cells {name[$((e + 8))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((96*i+82))     [get_cells {name[$((e + 8))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((96*i+83))     [get_cells {name[$((e + 8))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((96*i+84))     [get_cells {name[$((e + 8))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((96*i+85))     [get_cells {name[$((e + 8))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((96*i+86))     [get_cells {name[$((e + 8))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((96*i+87))     [get_cells {name[$((e + 8))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((96*i+88))     [get_cells {name[$((e + 9))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((96*i+89))     [get_cells {name[$((e + 9))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((96*i+90))     [get_cells {name[$((e + 9))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((96*i+91))     [get_cells {name[$((e + 9))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((96*i+92))     [get_cells {name[$((e + 9))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((96*i+93))     [get_cells {name[$((e + 9))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((96*i+94))     [get_cells {name[$((e + 9))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((96*i+95))     [get_cells {name[$((e + 9))].dut/conv2/bram_inst_rdc4}];" >> test.xdc

	echo "set_property LOC URAM288_X4Y$((64*i+ 56))    [get_cells {name[$((e + 8))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((64*i+ 57))    [get_cells {name[$((e + 8))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((64*i+ 58))    [get_cells {name[$((e + 9))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((64*i+ 59))    [get_cells {name[$((e + 9))].dut/uram_inst_rd}];" >> test.xdc

#######################################
	echo "set_property LOC DSP48E2_X20Y$(( 0 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X20Y$(( 1 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$(( 2 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$(( 3 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$(( 4 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$(( 5 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$(( 6 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$(( 7 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$(( 8 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X20Y$(( 9 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((10 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((11 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((12 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((13 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((14 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((15 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((16 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((17 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X21Y$(( 0 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X21Y$(( 1 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$(( 2 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$(( 3 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$(( 4 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$(( 5 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$(( 6 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$(( 7 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$(( 8 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X21Y$(( 9 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((10 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((11 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((12 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((13 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((14 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((15 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((16 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((17 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_inst8}];" >> test.xdc

	echo "set_property LOC RAMB18_X7Y$((96*i+80))     [get_cells {name[$((e + 10))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((96*i+81))     [get_cells {name[$((e + 10))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((96*i+82))     [get_cells {name[$((e + 10))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((96*i+83))     [get_cells {name[$((e + 10))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((96*i+84))     [get_cells {name[$((e + 10))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((96*i+85))     [get_cells {name[$((e + 10))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((96*i+86))     [get_cells {name[$((e + 10))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((96*i+87))     [get_cells {name[$((e + 10))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((96*i+88))     [get_cells {name[$((e + 11))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((96*i+89))     [get_cells {name[$((e + 11))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((96*i+90))     [get_cells {name[$((e + 11))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((96*i+91))     [get_cells {name[$((e + 11))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((96*i+92))     [get_cells {name[$((e + 11))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((96*i+93))     [get_cells {name[$((e + 11))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((96*i+94))     [get_cells {name[$((e + 11))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((96*i+95))     [get_cells {name[$((e + 11))].dut/conv2/bram_inst_rdc4}];" >> test.xdc

	echo "set_property LOC URAM288_X4Y$((64*i+ 60))    [get_cells {name[$((e + 10))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((64*i+ 61))    [get_cells {name[$((e + 10))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((64*i+ 62))    [get_cells {name[$((e + 11))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((64*i+ 63))    [get_cells {name[$((e + 11))].dut/uram_inst_rd}];" >> test.xdc
#######################################
	echo "set_property LOC DSP48E2_X12Y$(( 0 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X12Y$(( 1 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$(( 2 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$(( 3 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$(( 4 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$(( 5 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$(( 6 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$(( 7 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$(( 8 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X12Y$(( 9 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((10 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((11 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((12 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((13 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((14 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((15 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((16 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((17 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X19Y$(( 0 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X19Y$(( 1 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$(( 2 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$(( 3 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$(( 4 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$(( 5 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$(( 6 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$(( 7 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$(( 8 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X19Y$(( 9 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((10 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((11 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((12 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((13 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((14 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((15 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((16 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((17 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_inst8}];" >> test.xdc

	echo "set_property LOC RAMB18_X6Y$((96*i+80))     [get_cells {name[$((e + 12))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((96*i+81))     [get_cells {name[$((e + 12))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((96*i+82))     [get_cells {name[$((e + 12))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((96*i+83))     [get_cells {name[$((e + 12))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((96*i+84))     [get_cells {name[$((e + 12))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((96*i+85))     [get_cells {name[$((e + 12))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((96*i+86))     [get_cells {name[$((e + 12))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((96*i+87))     [get_cells {name[$((e + 12))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((96*i+88))     [get_cells {name[$((e + 13))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((96*i+89))     [get_cells {name[$((e + 13))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((96*i+90))     [get_cells {name[$((e + 13))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((96*i+91))     [get_cells {name[$((e + 13))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((96*i+92))     [get_cells {name[$((e + 13))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((96*i+93))     [get_cells {name[$((e + 13))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((96*i+94))     [get_cells {name[$((e + 13))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((96*i+95))     [get_cells {name[$((e + 13))].dut/conv2/bram_inst_rdc4}];" >> test.xdc

	echo "set_property LOC URAM288_X2Y$((64*i+ 60))    [get_cells {name[$((e + 12))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X2Y$((64*i+ 61))    [get_cells {name[$((e + 12))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X2Y$((64*i+ 62))    [get_cells {name[$((e + 13))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X2Y$((64*i+ 63))    [get_cells {name[$((e + 13))].dut/uram_inst_rd}];" >> test.xdc
#######################################

done

