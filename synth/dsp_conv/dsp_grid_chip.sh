#!/usr/bin/zsh
rm -f test.xdc
echo "create_clock -period 1.000 -waveform {0.000 0.500} [get_nets clk];" >> test.xdc
X=`echo "$1 - 1" | bc -l`
Y=`echo "$2 - 1" | bc -l`
U=`echo "$3 - 1" | bc -l`

dsp=(0 1 4 5 6 7 8 9 10 11 12 13 14 15 16 17 )
bram=(0 1 2 2 3 3 4 5 6 6 7 7 8 9 9 10 )
bram_off=(0 0 0 6 0 6 0 0 0 6 0 6 0 0 6 0 )
uram_y=(4 5 6 7 )
uram_x=(0 1 2 3 )



for i in {0..$Y}
do
	for j in {0..$X}
	do
		for k in {0..$U}
		do
			a=$dsp[((4*$j+$k+1))]
			b=$bram[((4*$j+$k+1))]
			c=$bram_off[((4*$j+$k+1))]
			d=$uram_y[(($k+1))]
			e=$uram_x[(($k+1))]

			echo "set_property LOC DSP48E2_X$(echo "$k")Y$(echo "12*${i}+0" | bc) [get_cells {ys[$i].xs[$j].xint[$k].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
			echo "set_property LOC DSP48E2_X$(echo "$k")Y$(echo "12*${i}+1" | bc) [get_cells {ys[$i].xs[$j].xint[$k].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc
			echo "set_property LOC DSP48E2_X$(echo "$k")Y$(echo "12*${i}+2" | bc) [get_cells {ys[$i].xs[$j].xint[$k].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc
			echo "set_property LOC DSP48E2_X$(echo "$k")Y$(echo "12*${i}+3" | bc) [get_cells {ys[$i].xs[$j].xint[$k].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc
			echo "set_property LOC DSP48E2_X$(echo "$k")Y$(echo "12*${i}+4" | bc) [get_cells {ys[$i].xs[$j].xint[$k].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc
			echo "set_property LOC DSP48E2_X$(echo "$k")Y$(echo "12*${i}+5" | bc) [get_cells {ys[$i].xs[$j].xint[$k].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc
			echo "set_property LOC DSP48E2_X$(echo "$k")Y$(echo "12*${i}+6" | bc) [get_cells {ys[$i].xs[$j].xint[$k].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc
			echo "set_property LOC DSP48E2_X$(echo "$k")Y$(echo "12*${i}+7" | bc) [get_cells {ys[$i].xs[$j].xint[$k].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc
			echo "set_property LOC DSP48E2_X$(echo "$k")Y$(echo "12*${i}+8" | bc) [get_cells {ys[$i].xs[$j].xint[$k].dut/conv1/dsp_inst8}];" >> test.xdc
			echo "set_property LOC RAMB18_X$(echo "$b")Y$(echo "12*${i}+$c+0" | bc) [get_cells {ys[$i].xs[$j].xint[$k].dut/conv1/bram_inst_rdc1}];" >> test.xdc
			echo "set_property LOC RAMB18_X$(echo "$b")Y$(echo "12*${i}+$c+2" | bc) [get_cells {ys[$i].xs[$j].xint[$k].dut/conv1/bram_inst_rdc2}];" >> test.xdc
			echo "set_property LOC RAMB18_X$(echo "$b")Y$(echo "12*${i}+$c+4" | bc) [get_cells {ys[$i].xs[$j].xint[$k].dut/conv1/bram_inst_rdc3}];" >> test.xdc
			echo "set_property LOC RAMB18_X$(echo "$b")Y$(echo "12*${i}+$c+4" | bc) [get_cells {ys[$i].xs[$j].xint[$k].dut/conv1/bram_inst_rdc4}];" >> test.xdc

			echo "set_property LOC URAM288_X${j}Y$(echo "8*${i}+$d" | bc) [get_cells {ys[$i].xs[$j].xint[$k].dut/uram_inst_wr}];" >> test.xdc
			echo "set_property LOC URAM288_X${j}Y$(echo "8*${i}+$e" | bc) [get_cells {ys[$i].xs[$j].xint[$k].dut/uram_inst_rd}];" >> test.xdc
		done
		#xint0


#		#xint 1
#		echo "set_property LOC DSP48E2_X1Y$(echo "12*${i}+0" | bc) [get_cells {ys[$i].xs[$j].xint[1].dut/dsp_chain0[0].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X1Y$(echo "12*${i}+1" | bc) [get_cells {ys[$i].xs[$j].xint[1].dut/dsp_chain0[1].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X1Y$(echo "12*${i}+2" | bc) [get_cells {ys[$i].xs[$j].xint[1].dut/dsp_chain0[2].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X1Y$(echo "12*${i}+3" | bc) [get_cells {ys[$i].xs[$j].xint[1].dut/dsp_chain1[0].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X1Y$(echo "12*${i}+4" | bc) [get_cells {ys[$i].xs[$j].xint[1].dut/dsp_chain1[1].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X1Y$(echo "12*${i}+5" | bc) [get_cells {ys[$i].xs[$j].xint[1].dut/dsp_chain1[2].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X1Y$(echo "12*${i}+6" | bc) [get_cells {ys[$i].xs[$j].xint[1].dut/dsp_chain2[0].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X1Y$(echo "12*${i}+7" | bc) [get_cells {ys[$i].xs[$j].xint[1].dut/dsp_chain2[1].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X1Y$(echo "12*${i}+8" | bc) [get_cells {ys[$i].xs[$j].xint[1].dut/dsp_inst8}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X1Y$(echo "12*${i}+9" | bc) [get_cells {ys[$i].xs[$j].xint[1].dut/dsp_inst9}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X1Y$(echo "12*${i}+10" | bc) [get_cells {ys[$i].xs[$j].xint[1].dut/dsp_inst10}];" >> test.xdc
#		echo "set_property LOC RAMB18_X1Y$(echo "12*${i}+0" | bc) [get_cells {ys[$i].xs[$j].xint[1].dut/bram_inst_rdc1}];" >> test.xdc
#		echo "set_property LOC RAMB18_X1Y$(echo "12*${i}+2" | bc) [get_cells {ys[$i].xs[$j].xint[1].dut/bram_inst_rdc2}];" >> test.xdc
#		echo "set_property LOC RAMB18_X1Y$(echo "12*${i}+4" | bc) [get_cells {ys[$i].xs[$j].xint[1].dut/bram_inst_rdc3}];" >> test.xdc
#		echo "set_property LOC RAMB18_X1Y$(echo "12*${i}+1" | bc) [get_cells {ys[$i].xs[$j].xint[1].dut/bram_inst_rd_kernel0}];" >> test.xdc
#		echo "set_property LOC RAMB18_X1Y$(echo "12*${i}+3" | bc) [get_cells {ys[$i].xs[$j].xint[1].dut/bram_inst_rd_kernel1}];" >> test.xdc
#		echo "set_property LOC RAMB18_X1Y$(echo "12*${i}+5" | bc) [get_cells {ys[$i].xs[$j].xint[1].dut/bram_inst_rd_kernel2}];" >> test.xdc
#		echo "set_property LOC URAM288_X0Y$(echo "8*${i}+5" | bc) [get_cells {ys[$i].xs[$j].xint[1].dut/uram_inst_wr}];" >> test.xdc
#		echo "set_property LOC URAM288_X0Y$(echo "8*${i}+1" | bc) [get_cells {ys[$i].xs[$j].xint[1].dut/uram_inst_rd}];" >> test.xdc
#
#		#xint 2
#		echo "set_property LOC DSP48E2_X4Y$(echo "12*${i}+0" | bc) [get_cells {ys[$i].xs[$j].xint[2].dut/dsp_chain0[0].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X4Y$(echo "12*${i}+1" | bc) [get_cells {ys[$i].xs[$j].xint[2].dut/dsp_chain0[1].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X4Y$(echo "12*${i}+2" | bc) [get_cells {ys[$i].xs[$j].xint[2].dut/dsp_chain0[2].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X4Y$(echo "12*${i}+3" | bc) [get_cells {ys[$i].xs[$j].xint[2].dut/dsp_chain1[0].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X4Y$(echo "12*${i}+4" | bc) [get_cells {ys[$i].xs[$j].xint[2].dut/dsp_chain1[1].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X4Y$(echo "12*${i}+5" | bc) [get_cells {ys[$i].xs[$j].xint[2].dut/dsp_chain1[2].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X4Y$(echo "12*${i}+6" | bc) [get_cells {ys[$i].xs[$j].xint[2].dut/dsp_chain2[0].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X4Y$(echo "12*${i}+7" | bc) [get_cells {ys[$i].xs[$j].xint[2].dut/dsp_chain2[1].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X4Y$(echo "12*${i}+8" | bc) [get_cells {ys[$i].xs[$j].xint[2].dut/dsp_inst8}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X4Y$(echo "12*${i}+9" | bc) [get_cells {ys[$i].xs[$j].xint[2].dut/dsp_inst9}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X4Y$(echo "12*${i}+10" | bc) [get_cells {ys[$i].xs[$j].xint[2].dut/dsp_inst10}];" >> test.xdc
#		echo "set_property LOC RAMB18_X2Y$(echo "12*${i}+0" | bc) [get_cells {ys[$i].xs[$j].xint[2].dut/bram_inst_rdc1}];" >> test.xdc
#		echo "set_property LOC RAMB18_X2Y$(echo "12*${i}+2" | bc) [get_cells {ys[$i].xs[$j].xint[2].dut/bram_inst_rdc2}];" >> test.xdc
#		echo "set_property LOC RAMB18_X2Y$(echo "12*${i}+4" | bc) [get_cells {ys[$i].xs[$j].xint[2].dut/bram_inst_rdc3}];" >> test.xdc
#		echo "set_property LOC RAMB18_X2Y$(echo "12*${i}+1" | bc) [get_cells {ys[$i].xs[$j].xint[2].dut/bram_inst_rd_kernel0}];" >> test.xdc
#		echo "set_property LOC RAMB18_X2Y$(echo "12*${i}+3" | bc) [get_cells {ys[$i].xs[$j].xint[2].dut/bram_inst_rd_kernel1}];" >> test.xdc
#		echo "set_property LOC RAMB18_X2Y$(echo "12*${i}+5" | bc) [get_cells {ys[$i].xs[$j].xint[2].dut/bram_inst_rd_kernel2}];" >> test.xdc
#		echo "set_property LOC URAM288_X0Y$(echo "8*${i}+6" | bc) [get_cells {ys[$i].xs[$j].xint[2].dut/uram_inst_wr}];" >> test.xdc
#		echo "set_property LOC URAM288_X0Y$(echo "8*${i}+2" | bc) [get_cells {ys[$i].xs[$j].xint[2].dut/uram_inst_rd}];" >> test.xdc
#
#		#xint 3
#		echo "set_property LOC DSP48E2_X5Y$(echo "12*${i}+0" | bc) [get_cells {ys[$i].xs[$j].xint[3].dut/dsp_chain0[0].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X5Y$(echo "12*${i}+1" | bc) [get_cells {ys[$i].xs[$j].xint[3].dut/dsp_chain0[1].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X5Y$(echo "12*${i}+2" | bc) [get_cells {ys[$i].xs[$j].xint[3].dut/dsp_chain0[2].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X5Y$(echo "12*${i}+3" | bc) [get_cells {ys[$i].xs[$j].xint[3].dut/dsp_chain1[0].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X5Y$(echo "12*${i}+4" | bc) [get_cells {ys[$i].xs[$j].xint[3].dut/dsp_chain1[1].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X5Y$(echo "12*${i}+5" | bc) [get_cells {ys[$i].xs[$j].xint[3].dut/dsp_chain1[2].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X5Y$(echo "12*${i}+6" | bc) [get_cells {ys[$i].xs[$j].xint[3].dut/dsp_chain2[0].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X5Y$(echo "12*${i}+7" | bc) [get_cells {ys[$i].xs[$j].xint[3].dut/dsp_chain2[1].dsp_inst}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X5Y$(echo "12*${i}+8" | bc) [get_cells {ys[$i].xs[$j].xint[3].dut/dsp_inst8}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X5Y$(echo "12*${i}+9" | bc) [get_cells {ys[$i].xs[$j].xint[3].dut/dsp_inst9}];" >> test.xdc
#		echo "set_property LOC DSP48E2_X5Y$(echo "12*${i}+10" | bc) [get_cells {ys[$i].xs[$j].xint[3].dut/dsp_inst10}];" >> test.xdc
#		echo "set_property LOC RAMB18_X2Y$(echo "12*${i}+6" | bc) [get_cells {ys[$i].xs[$j].xint[3].dut/bram_inst_rdc1}];" >> test.xdc
#		echo "set_property LOC RAMB18_X2Y$(echo "12*${i}+8" | bc) [get_cells {ys[$i].xs[$j].xint[3].dut/bram_inst_rdc2}];" >> test.xdc
#		echo "set_property LOC RAMB18_X2Y$(echo "12*${i}+10" | bc) [get_cells {ys[$i].xs[$j].xint[3].dut/bram_inst_rdc3}];" >> test.xdc
#		echo "set_property LOC RAMB18_X2Y$(echo "12*${i}+7" | bc) [get_cells {ys[$i].xs[$j].xint[3].dut/bram_inst_rd_kernel0}];" >> test.xdc
#		echo "set_property LOC RAMB18_X2Y$(echo "12*${i}+9" | bc) [get_cells {ys[$i].xs[$j].xint[3].dut/bram_inst_rd_kernel1}];" >> test.xdc
#		echo "set_property LOC RAMB18_X2Y$(echo "12*${i}+11" | bc) [get_cells {ys[$i].xs[$j].xint[3].dut/bram_inst_rd_kernel2}];" >> test.xdc
#		echo "set_property LOC URAM288_X0Y$(echo "8*${i}+7" | bc) [get_cells {ys[$i].xs[$j].xint[3].dut/uram_inst_wr}];" >> test.xdc
#		echo "set_property LOC URAM288_X0Y$(echo "8*${i}+3" | bc) [get_cells {ys[$i].xs[$j].xint[3].dut/uram_inst_rd}];" >> test.xdc


	done
done
#for i in {1..1}
#do
#	a=`echo "${i}-1" | bc`
#	echo "create_pblock pblock_${i};"
#	echo "resize_pblock pblock_${i} -add {SLICE_X0Y$(echo "30*(${i}-1)" | bc):SLICE_X41Y$(echo "30*${i}-1" | bc) DSP48E2_X0Y$(echo "12*(${i}-1)" | bc):DSP48E2_X5Y$(echo "12*${i}-1" | bc) RAMB18_X0Y$(echo "12*(${i}-1)" | bc):RAMB18_X2Y$(echo "12*${i}-1" | bc) RAMB36_X0Y$(echo "6*(${i}-1)" | bc):RAMB36_X2Y$(echo "6*${i}-1" | bc) URAM288_X0Y$(echo "8*(${i}-1)" | bc):URAM288_X0Y$(echo "8*${i}-1" | bc)};"
#	echo "add_cells_to_pblock pblock_${i} [get_cells [list {ys[${a}].dut}]];"
#done

