#!/usr/bin/zsh
rm -f test.xdc
X=`echo "$1 - 1" | bc -l`
Y=`echo "$2 - 1" | bc -l`

dsp=(0 0 0 0 0 0 0 0 0 0 6 6 6 6 6 )
dsp1=(0 0 0 0 18 18 18 18 42 42 42 42 )
bram=(0 0 0 0 0 16 16 16 16 16 32 32 32 32 32 )
bram1=(0 0 0 0 32 32 32 32 64 64 64 64 )
uram=(0 0 0 0 0 4 4 4 4 4 8 8 8 8 8 )
uram1=(0 10 20 30 40 64 74 84 94 104 128 138 148 158 168 )
uram2=(4 14 24 34 68 78 88 98 132 142 152 162 )
uram3=(0 14 28 42 56 64 78 92 106 120 128 142 156 170 184 )
uram4=(8 22 36 50 72 86 100 114 136 150 164 178 )

file1=(0 32 64 96 128 146 178 210 242 274 292 324 356 388 420 )
file2=(18 50 82 114 164 196 228 260 310 342 374 406 )

for i in {0..$X}
do
	a=${dsp[$i+1]}
	b=${bram[$i+1]}
	c=${uram[$i+1]}
	c1=${uram1[$i+1]}
	c3=${uram3[$i+1]}
	e=${file1[$i+1]}
        echo "# $((i)) in 2 loop" >> test.xdc
        echo "# $((a)), $((b)), $((c)), $((c1)), $((e))" >> test.xdc
	echo "set_property LOC DSP48E2_X13Y$((18*i+ 0 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+ 1 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+ 2 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+ 3 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+ 4 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+ 5 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+ 6 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+ 7 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+ 8 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X13Y$((18*i+ 9 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+10 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+11 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+12 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+13 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+14 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+15 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+16 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+17 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X14Y$((18*i+ 0 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X14Y$((18*i+ 1 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+ 2 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+ 3 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+ 4 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+ 5 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+ 6 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+ 7 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+ 8 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X14Y$((18*i+ 9 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+10 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+11 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+12 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+13 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+14 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+15 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+16 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+17 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_inst8}];" >> test.xdc

	echo "set_property LOC RAMB18_X6Y$((16*i+ 0 + b))     [get_cells {name[$((e + 0))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((16*i+ 1 + b))     [get_cells {name[$((e + 0))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((16*i+ 2 + b))     [get_cells {name[$((e + 0))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((16*i+ 3 + b))     [get_cells {name[$((e + 0))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((16*i+ 4 + b))     [get_cells {name[$((e + 0))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((16*i+ 5 + b))     [get_cells {name[$((e + 0))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((16*i+ 6 + b))     [get_cells {name[$((e + 0))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((16*i+ 7 + b))     [get_cells {name[$((e + 0))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((16*i+ 8 + b))     [get_cells {name[$((e + 1))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((16*i+ 9 + b))     [get_cells {name[$((e + 1))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((16*i+10 + b))     [get_cells {name[$((e + 1))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((16*i+11 + b))     [get_cells {name[$((e + 1))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((16*i+12 + b))     [get_cells {name[$((e + 1))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((16*i+13 + b))     [get_cells {name[$((e + 1))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((16*i+14 + b))     [get_cells {name[$((e + 1))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X6Y$((16*i+15 + b))     [get_cells {name[$((e + 1))].dut/conv2/bram_inst_rdc4}];" >> test.xdc

	echo "set_property LOC URAM288_X2Y$((12*i+ 0 + c))    [get_cells {name[$((e + 0))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X2Y$((12*i+ 1 + c))    [get_cells {name[$((e + 0))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X2Y$((12*i+ 2 + c))    [get_cells {name[$((e + 1))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X2Y$((12*i+ 3 + c))    [get_cells {name[$((e + 1))].dut/uram_inst_rd}];" >> test.xdc

#######################################
	echo "set_property LOC DSP48E2_X15Y$((18*i+ 0 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+ 1 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+ 2 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+ 3 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+ 4 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+ 5 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+ 6 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+ 7 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+ 8 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X15Y$((18*i+ 9 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+10 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+11 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+12 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+13 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+14 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+15 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+16 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+17 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X16Y$((18*i+ 0 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X16Y$((18*i+ 1 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X16Y$((18*i+ 2 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X16Y$((18*i+ 3 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X16Y$((18*i+ 4 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X16Y$((18*i+ 5 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X16Y$((18*i+ 6 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X16Y$((18*i+ 7 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X16Y$((18*i+ 8 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X16Y$((18*i+ 9 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X16Y$((18*i+10 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X16Y$((18*i+11 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X16Y$((18*i+12 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X16Y$((18*i+13 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X16Y$((18*i+14 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X16Y$((18*i+15 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X16Y$((18*i+16 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X16Y$((18*i+17 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_inst8}];" >> test.xdc

	echo "set_property LOC RAMB18_X7Y$((16*i+ 0 + b))     [get_cells {name[$((e + 2))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((16*i+ 1 + b))     [get_cells {name[$((e + 2))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((16*i+ 2 + b))     [get_cells {name[$((e + 2))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((16*i+ 3 + b))     [get_cells {name[$((e + 2))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((16*i+ 4 + b))     [get_cells {name[$((e + 2))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((16*i+ 5 + b))     [get_cells {name[$((e + 2))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((16*i+ 6 + b))     [get_cells {name[$((e + 2))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((16*i+ 7 + b))     [get_cells {name[$((e + 2))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((16*i+ 8 + b))     [get_cells {name[$((e + 3))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((16*i+ 9 + b))     [get_cells {name[$((e + 3))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((16*i+10 + b))     [get_cells {name[$((e + 3))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((16*i+11 + b))     [get_cells {name[$((e + 3))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((16*i+12 + b))     [get_cells {name[$((e + 3))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((16*i+13 + b))     [get_cells {name[$((e + 3))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((16*i+14 + b))     [get_cells {name[$((e + 3))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X7Y$((16*i+15 + b))     [get_cells {name[$((e + 3))].dut/conv2/bram_inst_rdc4}];" >> test.xdc

	echo "set_property LOC URAM288_X2Y$((12*i+ 4 + c))    [get_cells {name[$((e + 2))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X2Y$((12*i+ 5 + c))    [get_cells {name[$((e + 2))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X2Y$((12*i+ 6 + c))    [get_cells {name[$((e + 3))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X2Y$((12*i+ 7 + c))    [get_cells {name[$((e + 3))].dut/uram_inst_rd}];" >> test.xdc
#######################################

	echo "set_property LOC DSP48E2_X17Y$((18*i+ 0 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X17Y$((18*i+ 1 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X17Y$((18*i+ 2 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X17Y$((18*i+ 3 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X17Y$((18*i+ 4 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X17Y$((18*i+ 5 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X17Y$((18*i+ 6 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X17Y$((18*i+ 7 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X17Y$((18*i+ 8 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X17Y$((18*i+ 9 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X17Y$((18*i+10 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X17Y$((18*i+11 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X17Y$((18*i+12 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X17Y$((18*i+13 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X17Y$((18*i+14 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X17Y$((18*i+15 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X17Y$((18*i+16 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X17Y$((18*i+17 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X18Y$((18*i+ 0 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X18Y$((18*i+ 1 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X18Y$((18*i+ 2 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X18Y$((18*i+ 3 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X18Y$((18*i+ 4 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X18Y$((18*i+ 5 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X18Y$((18*i+ 6 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X18Y$((18*i+ 7 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X18Y$((18*i+ 8 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X18Y$((18*i+ 9 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X18Y$((18*i+10 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X18Y$((18*i+11 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X18Y$((18*i+12 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X18Y$((18*i+13 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X18Y$((18*i+14 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X18Y$((18*i+15 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X18Y$((18*i+16 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X18Y$((18*i+17 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_inst8}];" >> test.xdc

	echo "set_property LOC RAMB18_X8Y$((16*i+ 0 + b))     [get_cells {name[$((e + 4))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((16*i+ 1 + b))     [get_cells {name[$((e + 4))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((16*i+ 2 + b))     [get_cells {name[$((e + 4))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((16*i+ 3 + b))     [get_cells {name[$((e + 4))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((16*i+ 4 + b))     [get_cells {name[$((e + 4))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((16*i+ 5 + b))     [get_cells {name[$((e + 4))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((16*i+ 6 + b))     [get_cells {name[$((e + 4))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((16*i+ 7 + b))     [get_cells {name[$((e + 4))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((16*i+ 8 + b))     [get_cells {name[$((e + 5))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((16*i+ 9 + b))     [get_cells {name[$((e + 5))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((16*i+10 + b))     [get_cells {name[$((e + 5))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((16*i+11 + b))     [get_cells {name[$((e + 5))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((16*i+12 + b))     [get_cells {name[$((e + 5))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((16*i+13 + b))     [get_cells {name[$((e + 5))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((16*i+14 + b))     [get_cells {name[$((e + 5))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X8Y$((16*i+15 + b))     [get_cells {name[$((e + 5))].dut/conv2/bram_inst_rdc4}];" >> test.xdc

	echo "set_property LOC URAM288_X2Y$((12*i+ 8 + c))    [get_cells {name[$((e + 4))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X2Y$((12*i+ 9 + c))    [get_cells {name[$((e + 4))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X2Y$((12*i+ 10 + c))   [get_cells {name[$((e + 5))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X2Y$((12*i+ 11 + c))   [get_cells {name[$((e + 5))].dut/uram_inst_rd}];" >> test.xdc
#######################################
	echo "set_property LOC DSP48E2_X30Y$((18*i+ 0 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+ 1 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+ 2 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+ 3 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+ 4 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+ 5 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+ 6 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+ 7 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+ 8 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X30Y$((18*i+ 9 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+10 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+11 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+12 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+13 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+14 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+15 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+16 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+17 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X31Y$((18*i+ 0 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X31Y$((18*i+ 1 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+ 2 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+ 3 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+ 4 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+ 5 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+ 6 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+ 7 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+ 8 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X31Y$((18*i+ 9 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+10 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+11 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+12 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+13 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+14 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+15 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+16 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+17 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_inst8}];" >> test.xdc

	echo "set_property LOC RAMB18_X13Y$((16*i+ 0 + b))     [get_cells {name[$((e + 6))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((16*i+ 1 + b))     [get_cells {name[$((e + 6))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((16*i+ 2 + b))     [get_cells {name[$((e + 6))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((16*i+ 3 + b))     [get_cells {name[$((e + 6))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((16*i+ 4 + b))     [get_cells {name[$((e + 6))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((16*i+ 5 + b))     [get_cells {name[$((e + 6))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((16*i+ 6 + b))     [get_cells {name[$((e + 6))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((16*i+ 7 + b))     [get_cells {name[$((e + 6))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((16*i+ 8 + b))     [get_cells {name[$((e + 7))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((16*i+ 9 + b))     [get_cells {name[$((e + 7))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((16*i+10 + b))     [get_cells {name[$((e + 7))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((16*i+11 + b))     [get_cells {name[$((e + 7))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((16*i+12 + b))     [get_cells {name[$((e + 7))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((16*i+13 + b))     [get_cells {name[$((e + 7))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((16*i+14 + b))     [get_cells {name[$((e + 7))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X13Y$((16*i+15 + b))     [get_cells {name[$((e + 7))].dut/conv2/bram_inst_rdc4}];" >> test.xdc

	echo "set_property LOC URAM288_X4Y$((c1 + 0))    [get_cells {name[$((e + 6))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((c1 + 1))    [get_cells {name[$((e + 6))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((c1 + 2))    [get_cells {name[$((e + 7))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((c1 + 3))    [get_cells {name[$((e + 7))].dut/uram_inst_rd}];" >> test.xdc


#######################################
#### for left side of the chip

	echo "set_property LOC DSP48E2_X0Y$((18*i+ 0 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X0Y$((18*i+ 1 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X0Y$((18*i+ 2 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X0Y$((18*i+ 3 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X0Y$((18*i+ 4 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X0Y$((18*i+ 5 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X0Y$((18*i+ 6 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X0Y$((18*i+ 7 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X0Y$((18*i+ 8 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X0Y$((18*i+ 9 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X0Y$((18*i+10 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X0Y$((18*i+11 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X0Y$((18*i+12 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X0Y$((18*i+13 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X0Y$((18*i+14 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X0Y$((18*i+15 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X0Y$((18*i+16 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X0Y$((18*i+17 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X1Y$((18*i+ 0 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X1Y$((18*i+ 1 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X1Y$((18*i+ 2 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X1Y$((18*i+ 3 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X1Y$((18*i+ 4 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X1Y$((18*i+ 5 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X1Y$((18*i+ 6 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X1Y$((18*i+ 7 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X1Y$((18*i+ 8 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X1Y$((18*i+ 9 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X1Y$((18*i+10 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X1Y$((18*i+11 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X1Y$((18*i+12 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X1Y$((18*i+13 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X1Y$((18*i+14 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X1Y$((18*i+15 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X1Y$((18*i+16 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X1Y$((18*i+17 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_inst8}];" >> test.xdc

	echo "set_property LOC RAMB18_X0Y$((16*i+ 0 + b))     [get_cells {name[$((e + 8))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X0Y$((16*i+ 1 + b))     [get_cells {name[$((e + 8))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X0Y$((16*i+ 2 + b))     [get_cells {name[$((e + 8))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X0Y$((16*i+ 3 + b))     [get_cells {name[$((e + 8))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X0Y$((16*i+ 4 + b))     [get_cells {name[$((e + 8))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X0Y$((16*i+ 5 + b))     [get_cells {name[$((e + 8))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X0Y$((16*i+ 6 + b))     [get_cells {name[$((e + 8))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X0Y$((16*i+ 7 + b))     [get_cells {name[$((e + 8))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X0Y$((16*i+ 8 + b))     [get_cells {name[$((e + 9))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X0Y$((16*i+ 9 + b))     [get_cells {name[$((e + 9))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X0Y$((16*i+10 + b))     [get_cells {name[$((e + 9))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X0Y$((16*i+11 + b))     [get_cells {name[$((e + 9))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X0Y$((16*i+12 + b))     [get_cells {name[$((e + 9))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X0Y$((16*i+13 + b))     [get_cells {name[$((e + 9))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X0Y$((16*i+14 + b))     [get_cells {name[$((e + 9))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X0Y$((16*i+15 + b))     [get_cells {name[$((e + 9))].dut/conv2/bram_inst_rdc4}];" >> test.xdc

	echo "set_property LOC URAM288_X0Y$((12*i+ 0 + c))    [get_cells {name[$((e + 8))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X0Y$((12*i+ 1 + c))    [get_cells {name[$((e + 8))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X0Y$((12*i+ 2 + c))    [get_cells {name[$((e + 9))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X0Y$((12*i+ 3 + c))    [get_cells {name[$((e + 9))].dut/uram_inst_rd}];" >> test.xdc

#######################################
	echo "set_property LOC DSP48E2_X2Y$((18*i+ 0 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X2Y$((18*i+ 1 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+ 2 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+ 3 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+ 4 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+ 5 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+ 6 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+ 7 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+ 8 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X2Y$((18*i+ 9 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+10 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+11 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+12 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+13 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+14 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+15 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+16 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+17 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X3Y$((18*i+ 0 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X3Y$((18*i+ 1 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+ 2 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+ 3 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+ 4 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+ 5 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+ 6 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+ 7 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+ 8 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X3Y$((18*i+ 9 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+10 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+11 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+12 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+13 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+14 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+15 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+16 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+17 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_inst8}];" >> test.xdc

	echo "set_property LOC RAMB18_X1Y$((16*i+ 0 + b))     [get_cells {name[$((e + 10))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X1Y$((16*i+ 1 + b))     [get_cells {name[$((e + 10))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X1Y$((16*i+ 2 + b))     [get_cells {name[$((e + 10))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X1Y$((16*i+ 3 + b))     [get_cells {name[$((e + 10))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X1Y$((16*i+ 4 + b))     [get_cells {name[$((e + 10))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X1Y$((16*i+ 5 + b))     [get_cells {name[$((e + 10))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X1Y$((16*i+ 6 + b))     [get_cells {name[$((e + 10))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X1Y$((16*i+ 7 + b))     [get_cells {name[$((e + 10))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X1Y$((16*i+ 8 + b))     [get_cells {name[$((e + 11))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X1Y$((16*i+ 9 + b))     [get_cells {name[$((e + 11))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X1Y$((16*i+10 + b))     [get_cells {name[$((e + 11))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X1Y$((16*i+11 + b))     [get_cells {name[$((e + 11))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X1Y$((16*i+12 + b))     [get_cells {name[$((e + 11))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X1Y$((16*i+13 + b))     [get_cells {name[$((e + 11))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X1Y$((16*i+14 + b))     [get_cells {name[$((e + 11))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X1Y$((16*i+15 + b))     [get_cells {name[$((e + 11))].dut/conv2/bram_inst_rdc4}];" >> test.xdc

	echo "set_property LOC URAM288_X0Y$((12*i+ 4 + c))    [get_cells {name[$((e + 10))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X0Y$((12*i+ 5 + c))    [get_cells {name[$((e + 10))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X0Y$((12*i+ 6 + c))    [get_cells {name[$((e + 11))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X0Y$((12*i+ 7 + c))    [get_cells {name[$((e + 11))].dut/uram_inst_rd}];" >> test.xdc
#######################################
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 0 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 1 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 2 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 3 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 4 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 5 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 6 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 7 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 8 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 9 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+10 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+11 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+12 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+13 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+14 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+15 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+16 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+17 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 0 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 1 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 2 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 3 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 4 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 5 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 6 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 7 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 8 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 9 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+10 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+11 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+12 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+13 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+14 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+15 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+16 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+17 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_inst8}];" >> test.xdc

	echo "set_property LOC RAMB18_X2Y$((16*i+ 0 + b))     [get_cells {name[$((e + 12))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X2Y$((16*i+ 1 + b))     [get_cells {name[$((e + 12))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X2Y$((16*i+ 2 + b))     [get_cells {name[$((e + 12))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X2Y$((16*i+ 3 + b))     [get_cells {name[$((e + 12))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X2Y$((16*i+ 4 + b))     [get_cells {name[$((e + 12))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X2Y$((16*i+ 5 + b))     [get_cells {name[$((e + 12))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X2Y$((16*i+ 6 + b))     [get_cells {name[$((e + 12))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X2Y$((16*i+ 7 + b))     [get_cells {name[$((e + 12))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X2Y$((16*i+ 8 + b))     [get_cells {name[$((e + 13))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X2Y$((16*i+ 9 + b))     [get_cells {name[$((e + 13))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X2Y$((16*i+10 + b))     [get_cells {name[$((e + 13))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X2Y$((16*i+11 + b))     [get_cells {name[$((e + 13))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X2Y$((16*i+12 + b))     [get_cells {name[$((e + 13))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X2Y$((16*i+13 + b))     [get_cells {name[$((e + 13))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X2Y$((16*i+14 + b))     [get_cells {name[$((e + 13))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X2Y$((16*i+15 + b))     [get_cells {name[$((e + 13))].dut/conv2/bram_inst_rdc4}];" >> test.xdc

	echo "set_property LOC URAM288_X0Y$((12*i+ 8 + c))    [get_cells {name[$((e + 12))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X0Y$((12*i+ 9 + c))    [get_cells {name[$((e + 12))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X0Y$((12*i+ 10 + c))    [get_cells {name[$((e + 13))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X0Y$((12*i+ 11 + c))    [get_cells {name[$((e + 13))].dut/uram_inst_rd}];" >> test.xdc
#######################################
	echo "set_property LOC DSP48E2_X6Y$((18*i+ 0 + a))   [get_cells {name[$((e + 14))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X6Y$((18*i+ 1 + a))   [get_cells {name[$((e + 14))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+ 2 + a))   [get_cells {name[$((e + 14))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+ 3 + a))   [get_cells {name[$((e + 14))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+ 4 + a))   [get_cells {name[$((e + 14))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+ 5 + a))   [get_cells {name[$((e + 14))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+ 6 + a))   [get_cells {name[$((e + 14))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+ 7 + a))   [get_cells {name[$((e + 14))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+ 8 + a))   [get_cells {name[$((e + 14))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X6Y$((18*i+ 9 + a))   [get_cells {name[$((e + 14))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+10 + a))   [get_cells {name[$((e + 14))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+11 + a))   [get_cells {name[$((e + 14))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+12 + a))   [get_cells {name[$((e + 14))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+13 + a))   [get_cells {name[$((e + 14))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+14 + a))   [get_cells {name[$((e + 14))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+15 + a))   [get_cells {name[$((e + 14))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+16 + a))   [get_cells {name[$((e + 14))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+17 + a))   [get_cells {name[$((e + 14))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X7Y$((18*i+ 0 + a))   [get_cells {name[$((e + 15))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X7Y$((18*i+ 1 + a))   [get_cells {name[$((e + 15))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+ 2 + a))   [get_cells {name[$((e + 15))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+ 3 + a))   [get_cells {name[$((e + 15))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+ 4 + a))   [get_cells {name[$((e + 15))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+ 5 + a))   [get_cells {name[$((e + 15))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+ 6 + a))   [get_cells {name[$((e + 15))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+ 7 + a))   [get_cells {name[$((e + 15))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+ 8 + a))   [get_cells {name[$((e + 15))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X7Y$((18*i+ 9 + a))   [get_cells {name[$((e + 15))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+10 + a))   [get_cells {name[$((e + 15))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+11 + a))   [get_cells {name[$((e + 15))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+12 + a))   [get_cells {name[$((e + 15))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+13 + a))   [get_cells {name[$((e + 15))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+14 + a))   [get_cells {name[$((e + 15))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+15 + a))   [get_cells {name[$((e + 15))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+16 + a))   [get_cells {name[$((e + 15))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+17 + a))   [get_cells {name[$((e + 15))].dut/conv2/dsp_inst8}];" >> test.xdc

	echo "set_property LOC RAMB18_X3Y$((16*i+ 0 + b))     [get_cells {name[$((e + 14))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X3Y$((16*i+ 1 + b))     [get_cells {name[$((e + 14))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X3Y$((16*i+ 2 + b))     [get_cells {name[$((e + 14))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X3Y$((16*i+ 3 + b))     [get_cells {name[$((e + 14))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X3Y$((16*i+ 4 + b))     [get_cells {name[$((e + 14))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X3Y$((16*i+ 5 + b))     [get_cells {name[$((e + 14))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X3Y$((16*i+ 6 + b))     [get_cells {name[$((e + 14))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X3Y$((16*i+ 7 + b))     [get_cells {name[$((e + 14))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X3Y$((16*i+ 8 + b))     [get_cells {name[$((e + 15))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X3Y$((16*i+ 9 + b))     [get_cells {name[$((e + 15))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X3Y$((16*i+10 + b))     [get_cells {name[$((e + 15))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X3Y$((16*i+11 + b))     [get_cells {name[$((e + 15))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X3Y$((16*i+12 + b))     [get_cells {name[$((e + 15))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X3Y$((16*i+13 + b))     [get_cells {name[$((e + 15))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X3Y$((16*i+14 + b))     [get_cells {name[$((e + 15))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X3Y$((16*i+15 + b))     [get_cells {name[$((e + 15))].dut/conv2/bram_inst_rdc4}];" >> test.xdc

	echo "set_property LOC URAM288_X1Y$((c3 + 0))    [get_cells {name[$((e + 14))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X1Y$((c3 + 1))    [get_cells {name[$((e + 14))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X1Y$((c3 + 2))    [get_cells {name[$((e + 15))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X1Y$((c3 + 3))    [get_cells {name[$((e + 15))].dut/uram_inst_rd}];" >> test.xdc
#######################################
	echo "set_property LOC DSP48E2_X8Y$((18*i+ 0 + a))   [get_cells {name[$((e + 16))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X8Y$((18*i+ 1 + a))   [get_cells {name[$((e + 16))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+ 2 + a))   [get_cells {name[$((e + 16))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+ 3 + a))   [get_cells {name[$((e + 16))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+ 4 + a))   [get_cells {name[$((e + 16))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+ 5 + a))   [get_cells {name[$((e + 16))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+ 6 + a))   [get_cells {name[$((e + 16))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+ 7 + a))   [get_cells {name[$((e + 16))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+ 8 + a))   [get_cells {name[$((e + 16))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X8Y$((18*i+ 9 + a))   [get_cells {name[$((e + 16))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+10 + a))   [get_cells {name[$((e + 16))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+11 + a))   [get_cells {name[$((e + 16))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+12 + a))   [get_cells {name[$((e + 16))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+13 + a))   [get_cells {name[$((e + 16))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+14 + a))   [get_cells {name[$((e + 16))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+15 + a))   [get_cells {name[$((e + 16))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+16 + a))   [get_cells {name[$((e + 16))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+17 + a))   [get_cells {name[$((e + 16))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X9Y$((18*i+ 0 + a))   [get_cells {name[$((e + 17))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X9Y$((18*i+ 1 + a))   [get_cells {name[$((e + 17))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+ 2 + a))   [get_cells {name[$((e + 17))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+ 3 + a))   [get_cells {name[$((e + 17))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+ 4 + a))   [get_cells {name[$((e + 17))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+ 5 + a))   [get_cells {name[$((e + 17))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+ 6 + a))   [get_cells {name[$((e + 17))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+ 7 + a))   [get_cells {name[$((e + 17))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+ 8 + a))   [get_cells {name[$((e + 17))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X9Y$((18*i+ 9 + a))   [get_cells {name[$((e + 17))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+10 + a))   [get_cells {name[$((e + 17))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+11 + a))   [get_cells {name[$((e + 17))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+12 + a))   [get_cells {name[$((e + 17))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+13 + a))   [get_cells {name[$((e + 17))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+14 + a))   [get_cells {name[$((e + 17))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+15 + a))   [get_cells {name[$((e + 17))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+16 + a))   [get_cells {name[$((e + 17))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+17 + a))   [get_cells {name[$((e + 17))].dut/conv2/dsp_inst8}];" >> test.xdc

	echo "set_property LOC RAMB18_X4Y$((16*i+ 0 + b))     [get_cells {name[$((e + 16))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((16*i+ 1 + b))     [get_cells {name[$((e + 16))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((16*i+ 2 + b))     [get_cells {name[$((e + 16))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((16*i+ 3 + b))     [get_cells {name[$((e + 16))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((16*i+ 4 + b))     [get_cells {name[$((e + 16))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((16*i+ 5 + b))     [get_cells {name[$((e + 16))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((16*i+ 6 + b))     [get_cells {name[$((e + 16))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((16*i+ 7 + b))     [get_cells {name[$((e + 16))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((16*i+ 8 + b))     [get_cells {name[$((e + 17))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((16*i+ 9 + b))     [get_cells {name[$((e + 17))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((16*i+10 + b))     [get_cells {name[$((e + 17))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((16*i+11 + b))     [get_cells {name[$((e + 17))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((16*i+12 + b))     [get_cells {name[$((e + 17))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((16*i+13 + b))     [get_cells {name[$((e + 17))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((16*i+14 + b))     [get_cells {name[$((e + 17))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X4Y$((16*i+15 + b))     [get_cells {name[$((e + 17))].dut/conv2/bram_inst_rdc4}];" >> test.xdc

	echo "set_property LOC URAM288_X1Y$((c3 + 4))    [get_cells {name[$((e + 16))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X1Y$((c3 + 5))    [get_cells {name[$((e + 16))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X1Y$((c3 + 6))    [get_cells {name[$((e + 17))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X1Y$((c3 + 7))    [get_cells {name[$((e + 17))].dut/uram_inst_rd}];" >> test.xdc
#######################################

done

#######################################
for j in {0..$Y}
do
	a=${dsp1[$j+1]}
	b=${bram1[$j+1]}
	c2=${uram2[$j+1]}
	c4=${uram4[$j+1]}
	e=${file2[$j+1]}
        echo "# $((j)) in 3 loop" >> test.xdc
        echo "# $((a)), $((b)), $((c2)), $((e))" >> test.xdc
	echo "set_property LOC DSP48E2_X19Y$((18*j+ 0 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((18*j+ 1 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((18*j+ 2 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((18*j+ 3 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((18*j+ 4 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((18*j+ 5 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((18*j+ 6 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((18*j+ 7 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((18*j+ 8 + a))   [get_cells {name[$((e + 9))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X19Y$((18*j+ 9 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((18*j+10 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((18*j+11 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((18*j+12 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((18*j+13 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((18*j+14 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((18*j+15 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((18*j+16 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X19Y$((18*j+17 + a))   [get_cells {name[$((e + 9))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X20Y$((18*j+ 0 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X20Y$((18*j+ 1 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*j+ 2 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*j+ 3 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*j+ 4 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*j+ 5 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*j+ 6 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*j+ 7 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*j+ 8 + a))   [get_cells {name[$((e + 10))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X20Y$((18*j+ 9 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*j+10 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*j+11 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*j+12 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*j+13 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*j+14 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*j+15 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*j+16 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*j+17 + a))   [get_cells {name[$((e + 10))].dut/conv2/dsp_inst8}];" >> test.xdc

	echo "set_property LOC RAMB18_X9Y$((16*j+ 0 + b))     [get_cells {name[$((e + 9))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((16*j+ 1 + b))     [get_cells {name[$((e + 9))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((16*j+ 2 + b))     [get_cells {name[$((e + 9))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((16*j+ 3 + b))     [get_cells {name[$((e + 9))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((16*j+ 4 + b))     [get_cells {name[$((e + 9))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((16*j+ 5 + b))     [get_cells {name[$((e + 9))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((16*j+ 6 + b))     [get_cells {name[$((e + 9))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((16*j+ 7 + b))     [get_cells {name[$((e + 9))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((16*j+ 8 + b))     [get_cells {name[$((e + 10))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((16*j+ 9 + b))     [get_cells {name[$((e + 10))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((16*j+10 + b))     [get_cells {name[$((e + 10))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((16*j+11 + b))     [get_cells {name[$((e + 10))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((16*j+12 + b))     [get_cells {name[$((e + 10))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((16*j+13 + b))     [get_cells {name[$((e + 10))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((16*j+14 + b))     [get_cells {name[$((e + 10))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X9Y$((16*j+15 + b))     [get_cells {name[$((e + 10))].dut/conv2/bram_inst_rdc4}];" >> test.xdc

	echo "set_property LOC URAM288_X3Y$((16*j + 0))    [get_cells {name[$((e + 9))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X3Y$((16*j + 1))    [get_cells {name[$((e + 9))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X3Y$((16*j + 2))    [get_cells {name[$((e + 10))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X3Y$((16*j + 3))    [get_cells {name[$((e + 10))].dut/uram_inst_rd}];" >> test.xdc
#######################################

	echo "set_property LOC DSP48E2_X21Y$((18*j+ 0 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*j+ 1 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*j+ 2 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*j+ 3 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*j+ 4 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*j+ 5 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*j+ 6 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*j+ 7 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*j+ 8 + a))   [get_cells {name[$((e + 0))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X21Y$((18*j+ 9 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*j+10 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*j+11 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*j+12 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*j+13 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*j+14 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*j+15 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*j+16 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*j+17 + a))   [get_cells {name[$((e + 0))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X22Y$((18*j+ 0 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X22Y$((18*j+ 1 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*j+ 2 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*j+ 3 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*j+ 4 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*j+ 5 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*j+ 6 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*j+ 7 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*j+ 8 + a))   [get_cells {name[$((e + 1))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X22Y$((18*j+ 9 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*j+10 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*j+11 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*j+12 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*j+13 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*j+14 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*j+15 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*j+16 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*j+17 + a))   [get_cells {name[$((e + 1))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X23Y$((18*j+ 0 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X23Y$((18*j+ 1 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*j+ 2 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*j+ 3 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*j+ 4 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*j+ 5 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*j+ 6 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*j+ 7 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*j+ 8 + a))   [get_cells {name[$((e + 2))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X23Y$((18*j+ 9 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*j+10 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*j+11 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*j+12 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*j+13 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*j+14 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*j+15 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*j+16 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*j+17 + a))   [get_cells {name[$((e + 2))].dut/conv2/dsp_inst8}];" >> test.xdc

	echo "set_property LOC RAMB18_X10Y$((24*j+ 0))     [get_cells {name[$((e + 0))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+ 1))     [get_cells {name[$((e + 0))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+ 2))     [get_cells {name[$((e + 0))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+ 3))     [get_cells {name[$((e + 0))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+ 4))     [get_cells {name[$((e + 0))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+ 5))     [get_cells {name[$((e + 0))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+ 6))     [get_cells {name[$((e + 0))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+ 7))     [get_cells {name[$((e + 0))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+ 8))     [get_cells {name[$((e + 1))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+ 9))     [get_cells {name[$((e + 1))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+10))     [get_cells {name[$((e + 1))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+11))     [get_cells {name[$((e + 1))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+12))     [get_cells {name[$((e + 1))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+13))     [get_cells {name[$((e + 1))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+14))     [get_cells {name[$((e + 1))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+15))     [get_cells {name[$((e + 1))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+16))     [get_cells {name[$((e + 2))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+17))     [get_cells {name[$((e + 2))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+18))     [get_cells {name[$((e + 2))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+19))     [get_cells {name[$((e + 2))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+20))     [get_cells {name[$((e + 2))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+21))     [get_cells {name[$((e + 2))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+22))     [get_cells {name[$((e + 2))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X10Y$((24*j+23))     [get_cells {name[$((e + 2))].dut/conv2/bram_inst_rdc4}];" >> test.xdc

	echo "set_property LOC URAM288_X3Y$((16*j + 4 ))    [get_cells {name[$((e + 0))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X3Y$((16*j + 5 ))    [get_cells {name[$((e + 0))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X3Y$((16*j + 6 ))    [get_cells {name[$((e + 1))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X3Y$((16*j + 7 ))    [get_cells {name[$((e + 1))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X3Y$((16*j + 8 ))    [get_cells {name[$((e + 2))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X3Y$((16*j + 9 ))    [get_cells {name[$((e + 2))].dut/uram_inst_rd}];" >> test.xdc
#######################################

	echo "set_property LOC DSP48E2_X24Y$((18*j+ 0 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*j+ 1 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*j+ 2 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*j+ 3 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*j+ 4 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*j+ 5 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*j+ 6 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*j+ 7 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*j+ 8 + a))   [get_cells {name[$((e + 3))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X24Y$((18*j+ 9 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*j+10 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*j+11 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*j+12 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*j+13 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*j+14 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*j+15 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*j+16 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*j+17 + a))   [get_cells {name[$((e + 3))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X25Y$((18*j+ 0 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X25Y$((18*j+ 1 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*j+ 2 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*j+ 3 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*j+ 4 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*j+ 5 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*j+ 6 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*j+ 7 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*j+ 8 + a))   [get_cells {name[$((e + 4))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X25Y$((18*j+ 9 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*j+10 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*j+11 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*j+12 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*j+13 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*j+14 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*j+15 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*j+16 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*j+17 + a))   [get_cells {name[$((e + 4))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X26Y$((18*j+ 0 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X26Y$((18*j+ 1 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*j+ 2 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*j+ 3 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*j+ 4 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*j+ 5 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*j+ 6 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*j+ 7 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*j+ 8 + a))   [get_cells {name[$((e + 5))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X26Y$((18*j+ 9 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*j+10 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*j+11 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*j+12 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*j+13 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*j+14 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*j+15 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*j+16 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*j+17 + a))   [get_cells {name[$((e + 5))].dut/conv2/dsp_inst8}];" >> test.xdc

	echo "set_property LOC RAMB18_X11Y$((24*j+ 0))     [get_cells {name[$((e + 3))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+ 1))     [get_cells {name[$((e + 3))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+ 2))     [get_cells {name[$((e + 3))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+ 3))     [get_cells {name[$((e + 3))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+ 4))     [get_cells {name[$((e + 3))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+ 5))     [get_cells {name[$((e + 3))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+ 6))     [get_cells {name[$((e + 3))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+ 7))     [get_cells {name[$((e + 3))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+ 8))     [get_cells {name[$((e + 4))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+ 9))     [get_cells {name[$((e + 4))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+10))     [get_cells {name[$((e + 4))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+11))     [get_cells {name[$((e + 4))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+12))     [get_cells {name[$((e + 4))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+13))     [get_cells {name[$((e + 4))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+14))     [get_cells {name[$((e + 4))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+15))     [get_cells {name[$((e + 4))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+16))     [get_cells {name[$((e + 5))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+17))     [get_cells {name[$((e + 5))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+18))     [get_cells {name[$((e + 5))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+19))     [get_cells {name[$((e + 5))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+20))     [get_cells {name[$((e + 5))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+21))     [get_cells {name[$((e + 5))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+22))     [get_cells {name[$((e + 5))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X11Y$((24*j+23))     [get_cells {name[$((e + 5))].dut/conv2/bram_inst_rdc4}];" >> test.xdc

	echo "set_property LOC URAM288_X3Y$((16*j + 10 ))    [get_cells {name[$((e + 3))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X3Y$((16*j + 11 ))    [get_cells {name[$((e + 3))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X3Y$((16*j + 12 ))    [get_cells {name[$((e + 4))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X3Y$((16*j + 13 ))    [get_cells {name[$((e + 4))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X3Y$((16*j + 14 ))    [get_cells {name[$((e + 5))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X3Y$((16*j + 15 ))    [get_cells {name[$((e + 5))].dut/uram_inst_rd}];" >> test.xdc
#######################################
		
	echo "set_property LOC DSP48E2_X27Y$((18*j+ 0 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*j+ 1 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*j+ 2 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*j+ 3 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*j+ 4 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*j+ 5 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*j+ 6 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*j+ 7 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*j+ 8 + a))   [get_cells {name[$((e + 6))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X27Y$((18*j+ 9 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*j+10 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*j+11 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*j+12 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*j+13 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*j+14 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*j+15 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*j+16 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*j+17 + a))   [get_cells {name[$((e + 6))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X28Y$((18*j+ 0 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X28Y$((18*j+ 1 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*j+ 2 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*j+ 3 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*j+ 4 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*j+ 5 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*j+ 6 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*j+ 7 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*j+ 8 + a))   [get_cells {name[$((e + 7))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X28Y$((18*j+ 9 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*j+10 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*j+11 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*j+12 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*j+13 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*j+14 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*j+15 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*j+16 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*j+17 + a))   [get_cells {name[$((e + 7))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X29Y$((18*j+ 0 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X29Y$((18*j+ 1 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*j+ 2 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*j+ 3 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*j+ 4 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*j+ 5 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*j+ 6 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*j+ 7 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*j+ 8 + a))   [get_cells {name[$((e + 8))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X29Y$((18*j+ 9 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*j+10 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*j+11 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*j+12 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*j+13 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*j+14 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*j+15 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*j+16 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*j+17 + a))   [get_cells {name[$((e + 8))].dut/conv2/dsp_inst8}];" >> test.xdc

	echo "set_property LOC RAMB18_X12Y$((24*j+ 0))     [get_cells {name[$((e + 6))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+ 1))     [get_cells {name[$((e + 6))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+ 2))     [get_cells {name[$((e + 6))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+ 3))     [get_cells {name[$((e + 6))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+ 4))     [get_cells {name[$((e + 6))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+ 5))     [get_cells {name[$((e + 6))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+ 6))     [get_cells {name[$((e + 6))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+ 7))     [get_cells {name[$((e + 6))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+ 8))     [get_cells {name[$((e + 7))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+ 9))     [get_cells {name[$((e + 7))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+10))     [get_cells {name[$((e + 7))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+11))     [get_cells {name[$((e + 7))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+12))     [get_cells {name[$((e + 7))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+13))     [get_cells {name[$((e + 7))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+14))     [get_cells {name[$((e + 7))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+15))     [get_cells {name[$((e + 7))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+16))     [get_cells {name[$((e + 8))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+17))     [get_cells {name[$((e + 8))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+18))     [get_cells {name[$((e + 8))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+19))     [get_cells {name[$((e + 8))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+20))     [get_cells {name[$((e + 8))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+21))     [get_cells {name[$((e + 8))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+22))     [get_cells {name[$((e + 8))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X12Y$((24*j+23))     [get_cells {name[$((e + 8))].dut/conv2/bram_inst_rdc4}];" >> test.xdc

	echo "set_property LOC URAM288_X4Y$((c2 + 0 ))    [get_cells {name[$((e + 6))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((c2 + 1 ))    [get_cells {name[$((e + 6))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((c2 + 2 ))    [get_cells {name[$((e + 7))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((c2 + 3 ))    [get_cells {name[$((e + 7))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((c2 + 4 ))    [get_cells {name[$((e + 8))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X4Y$((c2 + 5 ))    [get_cells {name[$((e + 8))].dut/uram_inst_rd}];" >> test.xdc
#######################################
#### for left side of the chip
	echo "set_property LOC DSP48E2_X10Y$((18*j+ 0 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*j+ 1 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*j+ 2 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*j+ 3 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*j+ 4 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*j+ 5 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*j+ 6 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*j+ 7 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*j+ 8 + a))   [get_cells {name[$((e + 11))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X10Y$((18*j+ 9 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*j+10 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*j+11 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*j+12 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*j+13 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*j+14 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*j+15 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*j+16 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*j+17 + a))   [get_cells {name[$((e + 11))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X11Y$((18*j+ 0 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X11Y$((18*j+ 1 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*j+ 2 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*j+ 3 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*j+ 4 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*j+ 5 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*j+ 6 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*j+ 7 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*j+ 8 + a))   [get_cells {name[$((e + 12))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X11Y$((18*j+ 9 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*j+10 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*j+11 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*j+12 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*j+13 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*j+14 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*j+15 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*j+16 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*j+17 + a))   [get_cells {name[$((e + 12))].dut/conv2/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X12Y$((18*j+ 0 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain0[0].dsp_inst}];" >> test.xdc
	echo "set_property LOC DSP48E2_X12Y$((18*j+ 1 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*j+ 2 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*j+ 3 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*j+ 4 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*j+ 5 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*j+ 6 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*j+ 7 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*j+ 8 + a))   [get_cells {name[$((e + 13))].dut/conv1/dsp_inst8}];" >> test.xdc
	echo "set_property LOC DSP48E2_X12Y$((18*j+ 9 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain0[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*j+10 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain0[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*j+11 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain0[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*j+12 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain1[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*j+13 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain1[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*j+14 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain1[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*j+15 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain2[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*j+16 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_chain2[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*j+17 + a))   [get_cells {name[$((e + 13))].dut/conv2/dsp_inst8}];" >> test.xdc

	echo "set_property LOC RAMB18_X5Y$((24*j+ 0))     [get_cells {name[$((e + 11))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+ 1))     [get_cells {name[$((e + 11))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+ 2))     [get_cells {name[$((e + 11))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+ 3))     [get_cells {name[$((e + 11))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+ 4))     [get_cells {name[$((e + 11))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+ 5))     [get_cells {name[$((e + 11))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+ 6))     [get_cells {name[$((e + 11))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+ 7))     [get_cells {name[$((e + 11))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+ 8))     [get_cells {name[$((e + 12))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+ 9))     [get_cells {name[$((e + 12))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+10))     [get_cells {name[$((e + 12))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+11))     [get_cells {name[$((e + 12))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+12))     [get_cells {name[$((e + 12))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+13))     [get_cells {name[$((e + 12))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+14))     [get_cells {name[$((e + 12))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+15))     [get_cells {name[$((e + 12))].dut/conv2/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+16))     [get_cells {name[$((e + 13))].dut/conv1/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+17))     [get_cells {name[$((e + 13))].dut/conv2/bram_inst_rdc1}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+18))     [get_cells {name[$((e + 13))].dut/conv1/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+19))     [get_cells {name[$((e + 13))].dut/conv2/bram_inst_rdc2}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+20))     [get_cells {name[$((e + 13))].dut/conv1/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+21))     [get_cells {name[$((e + 13))].dut/conv2/bram_inst_rdc3}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+22))     [get_cells {name[$((e + 13))].dut/conv1/bram_inst_rdc4}];" >> test.xdc
	echo "set_property LOC RAMB18_X5Y$((24*j+23))     [get_cells {name[$((e + 13))].dut/conv2/bram_inst_rdc4}];" >> test.xdc

	echo "set_property LOC URAM288_X1Y$((c4 + 0))    [get_cells {name[$((e + 11))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X1Y$((c4 + 1))    [get_cells {name[$((e + 11))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X1Y$((c4 + 2))    [get_cells {name[$((e + 12))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X1Y$((c4 + 3))    [get_cells {name[$((e + 12))].dut/uram_inst_rd}];" >> test.xdc
	echo "set_property LOC URAM288_X1Y$((c4 + 4))    [get_cells {name[$((e + 13))].dut/uram_inst_wr}];" >> test.xdc
	echo "set_property LOC URAM288_X1Y$((c4 + 5))    [get_cells {name[$((e + 13))].dut/uram_inst_rd}];" >> test.xdc
#######################################

done
