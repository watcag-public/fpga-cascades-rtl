create_clock -period 1.500 -waveform {0.000 0.750} [get_nets clk];

# DSP 0, 1 BRAM 0 x 5
#select_objects [get_cells {name[*].dut/*/dsp_chain[*].dsp_inst}]
#select_objects [get_sites -regexp {DSP48E2_X.*Y([1][8][6-9]|[1][9][0-9]|[2][0-6][0-9]|[2][7][0-5])}]
#select_objects [get_sites -regexp {RAMB18_X.*Y([0-9]|[1-5][0-9]|[6][0-4]|[9][6-9]|[1][0-5][0-9]|[1][6][0]|[1][9][2-9]|[2][0-4][0-9]|[2][5][0-6])}]


set_property LOC DSP48E2_X0Y0   [get_cells {name[0].dut/mm1/dsp_chain[0].dsp_inst}];
set_property LOC DSP48E2_X0Y1   [get_cells {name[0].dut/mm1/dsp_chain[1].dsp_inst}];
set_property LOC DSP48E2_X0Y2   [get_cells {name[0].dut/mm1/dsp_chain[2].dsp_inst}];
set_property LOC DSP48E2_X0Y3   [get_cells {name[0].dut/mm1/dsp_chain[3].dsp_inst}];
set_property LOC DSP48E2_X0Y4   [get_cells {name[0].dut/mm1/dsp_chain[4].dsp_inst}];
set_property LOC DSP48E2_X0Y5   [get_cells {name[0].dut/mm1/dsp_chain[5].dsp_inst}];
set_property LOC DSP48E2_X0Y6   [get_cells {name[0].dut/mm1/dsp_chain[6].dsp_inst}];
set_property LOC DSP48E2_X0Y7   [get_cells {name[0].dut/mm1/dsp_chain[7].dsp_inst}];
set_property LOC DSP48E2_X0Y8   [get_cells {name[0].dut/mm1/dsp_chain[8].dsp_inst}];
set_property LOC RAMB18_X0Y0    [get_cells {name[0].dut/bram1}];
set_property LOC RAMB18_X0Y1    [get_cells {name[0].dut/bram2}];
set_property LOC RAMB18_X0Y2    [get_cells {name[0].dut/bram3}];
set_property LOC RAMB18_X0Y3    [get_cells {name[0].dut/bram4}];
set_property LOC RAMB18_X0Y4    [get_cells {name[0].dut/bram5}];
set_property LOC RAMB18_X0Y5    [get_cells {name[0].dut/bram6}];
set_property LOC RAMB18_X0Y6    [get_cells {name[0].dut/bram7}];
set_property LOC RAMB18_X0Y7    [get_cells {name[0].dut/bram8}];
set_property LOC RAMB18_X0Y8    [get_cells {name[0].dut/bram9}];
set_property LOC RAMB18_X0Y9    [get_cells {name[0].dut/mm1/bram_inst_wr}];
set_property LOC URAM288_X0Y0   [get_cells {name[0].dut/mm1/uram_inst_rd}];

