#!/usr/bin/zsh
rm -f test.xdc
X=`echo "$1 - 1" | bc -l`
Y=`echo "$2 - 1" | bc -l`
Z=`echo "$3 - 1" | bc -l`

dsp=(0 0 0 0 0 0 0 0 0 0 6 6 6 6 6 )
bram=(0 0 0 0 0 31 31 31 31 31 62 62 62 62 62 )
uram=(0 0 0 0 0 4 4 4 4 4 8 8 8 8 8 )


dsp1=(0 0 0 0 18 18 18 18 42 42 42 42 )
bram1=(0 0 0 0 32 32 32 32 64 64 64 64 )
uram1=(0 10 20 30 40 64 74 84 94 104 128 138 148 158 168 )
uram2=(4 14 24 34 68 78 88 98 132 142 152 162 )
uram3=(0 14 28 42 56 64 78 92 106 120 128 142 156 170 184 )
uram4=(8 22 36 50 72 86 100 114 136 150 164 178 )

file1=(0 14 28 42 56 146 178 210 242 274 292 324 356 388 420 )
file2=(18 50 82 114 164 196 228 260 310 342 374 406 )

for i in {0..$X}
do
	a=${dsp[$i+1]}
	b=${bram[$i+1]}
	c=${uram[$i+1]}
        dsp_col1=0
        dsp_col2=1
        bram_col=0
        uram_col=0

#	c1=${uram1[$i+1]}
#	c3=${uram3[$i+1]}
	e=${file1[$i+1]}
        echo "# $((i)) in 5 loop" >> test.xdc
        echo "# $((a)), $((b)), $((c)), $((e))" >> test.xdc
#######################################
#### for left side of the chip

	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 0 + a))   [get_cells {name[$((e + 0))].dut/mm1/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 1 + a))   [get_cells {name[$((e + 0))].dut/mm1/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 2 + a))   [get_cells {name[$((e + 0))].dut/mm1/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 3 + a))   [get_cells {name[$((e + 0))].dut/mm1/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 4 + a))   [get_cells {name[$((e + 0))].dut/mm1/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 5 + a))   [get_cells {name[$((e + 0))].dut/mm1/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 6 + a))   [get_cells {name[$((e + 0))].dut/mm1/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 7 + a))   [get_cells {name[$((e + 0))].dut/mm1/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 8 + a))   [get_cells {name[$((e + 0))].dut/mm1/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 9 + a))   [get_cells {name[$((e + 0))].dut/mm2/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+10 + a))   [get_cells {name[$((e + 0))].dut/mm2/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+11 + a))   [get_cells {name[$((e + 0))].dut/mm2/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+12 + a))   [get_cells {name[$((e + 0))].dut/mm2/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+13 + a))   [get_cells {name[$((e + 0))].dut/mm2/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+14 + a))   [get_cells {name[$((e + 0))].dut/mm2/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+15 + a))   [get_cells {name[$((e + 0))].dut/mm2/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+16 + a))   [get_cells {name[$((e + 0))].dut/mm2/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+17 + a))   [get_cells {name[$((e + 0))].dut/mm2/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 0 + a))   [get_cells {name[$((e + 0))].dut/mm3/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 1 + a))   [get_cells {name[$((e + 0))].dut/mm3/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 2 + a))   [get_cells {name[$((e + 0))].dut/mm3/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 3 + a))   [get_cells {name[$((e + 0))].dut/mm3/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 4 + a))   [get_cells {name[$((e + 0))].dut/mm3/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 5 + a))   [get_cells {name[$((e + 0))].dut/mm3/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 6 + a))   [get_cells {name[$((e + 0))].dut/mm3/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 7 + a))   [get_cells {name[$((e + 0))].dut/mm3/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 8 + a))   [get_cells {name[$((e + 0))].dut/mm3/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 9 + a))   [get_cells {name[$((e + 0))].dut/mm4/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+10 + a))   [get_cells {name[$((e + 0))].dut/mm4/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+11 + a))   [get_cells {name[$((e + 0))].dut/mm4/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+12 + a))   [get_cells {name[$((e + 0))].dut/mm4/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+13 + a))   [get_cells {name[$((e + 0))].dut/mm4/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+14 + a))   [get_cells {name[$((e + 0))].dut/mm4/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+15 + a))   [get_cells {name[$((e + 0))].dut/mm4/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+16 + a))   [get_cells {name[$((e + 0))].dut/mm4/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+17 + a))   [get_cells {name[$((e + 0))].dut/mm4/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 0 + b))    [get_cells {name[$((e + 0))].dut/bram1}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 1 + b))    [get_cells {name[$((e + 0))].dut/bram2}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 2 + b))    [get_cells {name[$((e + 0))].dut/bram3}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 3 + b))    [get_cells {name[$((e + 0))].dut/bram4}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 4 + b))    [get_cells {name[$((e + 0))].dut/bram5}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 5 + b))    [get_cells {name[$((e + 0))].dut/bram6}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 6 + b))    [get_cells {name[$((e + 0))].dut/bram7}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 7 + b))    [get_cells {name[$((e + 0))].dut/bram8}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 8 + b))    [get_cells {name[$((e + 0))].dut/bram9}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 9 + b))    [get_cells {name[$((e + 0))].dut/mm1/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+10 + b))    [get_cells {name[$((e + 0))].dut/mm2/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+11 + b))    [get_cells {name[$((e + 0))].dut/mm3/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+12 + b))    [get_cells {name[$((e + 0))].dut/mm4/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC URAM288_X$((uram_col))Y$((12*i+ 0 + c))   [get_cells {name[$((e + 0))].dut/mm1/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X$((uram_col))Y$((12*i+ 1 + c))   [get_cells {name[$((e + 0))].dut/mm2/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X$((uram_col))Y$((12*i+ 2 + c))   [get_cells {name[$((e + 0))].dut/mm3/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X$((uram_col))Y$((12*i+ 3 + c))   [get_cells {name[$((e + 0))].dut/mm4/uram_inst_rd}];" >> test.xdc;

done

#######################################
	echo "set_property LOC DSP48E2_X2Y$((18*i+ 0 + a))   [get_cells {name[$((e + 1))].dut/mm1/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+ 1 + a))   [get_cells {name[$((e + 1))].dut/mm1/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+ 2 + a))   [get_cells {name[$((e + 1))].dut/mm1/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+ 3 + a))   [get_cells {name[$((e + 1))].dut/mm1/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+ 4 + a))   [get_cells {name[$((e + 1))].dut/mm1/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+ 5 + a))   [get_cells {name[$((e + 1))].dut/mm1/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+ 6 + a))   [get_cells {name[$((e + 1))].dut/mm1/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+ 7 + a))   [get_cells {name[$((e + 1))].dut/mm1/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+ 8 + a))   [get_cells {name[$((e + 1))].dut/mm1/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+ 9 + a))   [get_cells {name[$((e + 1))].dut/mm2/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+10 + a))   [get_cells {name[$((e + 1))].dut/mm2/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+11 + a))   [get_cells {name[$((e + 1))].dut/mm2/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+12 + a))   [get_cells {name[$((e + 1))].dut/mm2/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+13 + a))   [get_cells {name[$((e + 1))].dut/mm2/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+14 + a))   [get_cells {name[$((e + 1))].dut/mm2/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+15 + a))   [get_cells {name[$((e + 1))].dut/mm2/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+16 + a))   [get_cells {name[$((e + 1))].dut/mm2/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X2Y$((18*i+17 + a))   [get_cells {name[$((e + 1))].dut/mm2/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+ 0 + a))   [get_cells {name[$((e + 1))].dut/mm3/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+ 1 + a))   [get_cells {name[$((e + 1))].dut/mm3/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+ 2 + a))   [get_cells {name[$((e + 1))].dut/mm3/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+ 3 + a))   [get_cells {name[$((e + 1))].dut/mm3/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+ 4 + a))   [get_cells {name[$((e + 1))].dut/mm3/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+ 5 + a))   [get_cells {name[$((e + 1))].dut/mm3/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+ 6 + a))   [get_cells {name[$((e + 1))].dut/mm3/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+ 7 + a))   [get_cells {name[$((e + 1))].dut/mm3/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+ 8 + a))   [get_cells {name[$((e + 1))].dut/mm3/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+ 9 + a))   [get_cells {name[$((e + 1))].dut/mm4/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+10 + a))   [get_cells {name[$((e + 1))].dut/mm4/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+11 + a))   [get_cells {name[$((e + 1))].dut/mm4/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+12 + a))   [get_cells {name[$((e + 1))].dut/mm4/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+13 + a))   [get_cells {name[$((e + 1))].dut/mm4/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+14 + a))   [get_cells {name[$((e + 1))].dut/mm4/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+15 + a))   [get_cells {name[$((e + 1))].dut/mm4/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+16 + a))   [get_cells {name[$((e + 1))].dut/mm4/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X3Y$((18*i+17 + a))   [get_cells {name[$((e + 1))].dut/mm4/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC RAMB18_X1Y$((13*i+ 0 + b))    [get_cells {name[$((e + 1))].dut/bram1}];" >> test.xdc;
	echo "set_property LOC RAMB18_X1Y$((13*i+ 1 + b))    [get_cells {name[$((e + 1))].dut/bram2}];" >> test.xdc;
	echo "set_property LOC RAMB18_X1Y$((13*i+ 2 + b))    [get_cells {name[$((e + 1))].dut/bram3}];" >> test.xdc;
	echo "set_property LOC RAMB18_X1Y$((13*i+ 3 + b))    [get_cells {name[$((e + 1))].dut/bram4}];" >> test.xdc;
	echo "set_property LOC RAMB18_X1Y$((13*i+ 4 + b))    [get_cells {name[$((e + 1))].dut/bram5}];" >> test.xdc;
	echo "set_property LOC RAMB18_X1Y$((13*i+ 5 + b))    [get_cells {name[$((e + 1))].dut/bram6}];" >> test.xdc;
	echo "set_property LOC RAMB18_X1Y$((13*i+ 6 + b))    [get_cells {name[$((e + 1))].dut/bram7}];" >> test.xdc;
	echo "set_property LOC RAMB18_X1Y$((13*i+ 7 + b))    [get_cells {name[$((e + 1))].dut/bram8}];" >> test.xdc;
	echo "set_property LOC RAMB18_X1Y$((13*i+ 8 + b))    [get_cells {name[$((e + 1))].dut/bram9}];" >> test.xdc;
	echo "set_property LOC RAMB18_X1Y$((13*i+ 9 + b))    [get_cells {name[$((e + 1))].dut/mm1/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X1Y$((13*i+10 + b))    [get_cells {name[$((e + 1))].dut/mm2/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X1Y$((13*i+11 + b))    [get_cells {name[$((e + 1))].dut/mm3/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X1Y$((13*i+12 + b))    [get_cells {name[$((e + 1))].dut/mm4/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC URAM288_X0Y$((12*i+ 4 + c))   [get_cells {name[$((e + 1))].dut/mm1/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X0Y$((12*i+ 5 + c))   [get_cells {name[$((e + 1))].dut/mm2/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X0Y$((12*i+ 6 + c))   [get_cells {name[$((e + 1))].dut/mm3/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X0Y$((12*i+ 7 + c))   [get_cells {name[$((e + 1))].dut/mm4/uram_inst_rd}];" >> test.xdc;

#######################################

#######################################
	echo "set_property LOC DSP48E2_X6Y$((18*i+ 0 + a))   [get_cells {name[$((e + 3))].dut/mm1/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+ 1 + a))   [get_cells {name[$((e + 3))].dut/mm1/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+ 2 + a))   [get_cells {name[$((e + 3))].dut/mm1/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+ 3 + a))   [get_cells {name[$((e + 3))].dut/mm1/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+ 4 + a))   [get_cells {name[$((e + 3))].dut/mm1/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+ 5 + a))   [get_cells {name[$((e + 3))].dut/mm1/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+ 6 + a))   [get_cells {name[$((e + 3))].dut/mm1/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+ 7 + a))   [get_cells {name[$((e + 3))].dut/mm1/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+ 8 + a))   [get_cells {name[$((e + 3))].dut/mm1/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+ 9 + a))   [get_cells {name[$((e + 3))].dut/mm2/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+10 + a))   [get_cells {name[$((e + 3))].dut/mm2/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+11 + a))   [get_cells {name[$((e + 3))].dut/mm2/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+12 + a))   [get_cells {name[$((e + 3))].dut/mm2/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+13 + a))   [get_cells {name[$((e + 3))].dut/mm2/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+14 + a))   [get_cells {name[$((e + 3))].dut/mm2/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+15 + a))   [get_cells {name[$((e + 3))].dut/mm2/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+16 + a))   [get_cells {name[$((e + 3))].dut/mm2/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X6Y$((18*i+17 + a))   [get_cells {name[$((e + 3))].dut/mm2/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+ 0 + a))   [get_cells {name[$((e + 3))].dut/mm3/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+ 1 + a))   [get_cells {name[$((e + 3))].dut/mm3/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+ 2 + a))   [get_cells {name[$((e + 3))].dut/mm3/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+ 3 + a))   [get_cells {name[$((e + 3))].dut/mm3/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+ 4 + a))   [get_cells {name[$((e + 3))].dut/mm3/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+ 5 + a))   [get_cells {name[$((e + 3))].dut/mm3/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+ 6 + a))   [get_cells {name[$((e + 3))].dut/mm3/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+ 7 + a))   [get_cells {name[$((e + 3))].dut/mm3/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+ 8 + a))   [get_cells {name[$((e + 3))].dut/mm3/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+ 9 + a))   [get_cells {name[$((e + 3))].dut/mm4/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+10 + a))   [get_cells {name[$((e + 3))].dut/mm4/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+11 + a))   [get_cells {name[$((e + 3))].dut/mm4/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+12 + a))   [get_cells {name[$((e + 3))].dut/mm4/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+13 + a))   [get_cells {name[$((e + 3))].dut/mm4/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+14 + a))   [get_cells {name[$((e + 3))].dut/mm4/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+15 + a))   [get_cells {name[$((e + 3))].dut/mm4/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+16 + a))   [get_cells {name[$((e + 3))].dut/mm4/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X7Y$((18*i+17 + a))   [get_cells {name[$((e + 3))].dut/mm4/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC RAMB18_X3Y$((13*i+ 0 + b))    [get_cells {name[$((e + 3))].dut/bram1}];" >> test.xdc;
	echo "set_property LOC RAMB18_X3Y$((13*i+ 1 + b))    [get_cells {name[$((e + 3))].dut/bram2}];" >> test.xdc;
	echo "set_property LOC RAMB18_X3Y$((13*i+ 2 + b))    [get_cells {name[$((e + 3))].dut/bram3}];" >> test.xdc;
	echo "set_property LOC RAMB18_X3Y$((13*i+ 3 + b))    [get_cells {name[$((e + 3))].dut/bram4}];" >> test.xdc;
	echo "set_property LOC RAMB18_X3Y$((13*i+ 4 + b))    [get_cells {name[$((e + 3))].dut/bram5}];" >> test.xdc;
	echo "set_property LOC RAMB18_X3Y$((13*i+ 5 + b))    [get_cells {name[$((e + 3))].dut/bram6}];" >> test.xdc;
	echo "set_property LOC RAMB18_X3Y$((13*i+ 6 + b))    [get_cells {name[$((e + 3))].dut/bram7}];" >> test.xdc;
	echo "set_property LOC RAMB18_X3Y$((13*i+ 7 + b))    [get_cells {name[$((e + 3))].dut/bram8}];" >> test.xdc;
	echo "set_property LOC RAMB18_X3Y$((13*i+ 8 + b))    [get_cells {name[$((e + 3))].dut/bram9}];" >> test.xdc;
	echo "set_property LOC RAMB18_X3Y$((13*i+ 9 + b))    [get_cells {name[$((e + 3))].dut/mm1/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X3Y$((13*i+10 + b))    [get_cells {name[$((e + 3))].dut/mm2/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X3Y$((13*i+11 + b))    [get_cells {name[$((e + 3))].dut/mm3/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X3Y$((13*i+12 + b))    [get_cells {name[$((e + 3))].dut/mm4/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC URAM288_X1Y$((12*i+ 0 + c))   [get_cells {name[$((e + 3))].dut/mm1/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X1Y$((12*i+ 1 + c))   [get_cells {name[$((e + 3))].dut/mm2/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X1Y$((12*i+ 2 + c))   [get_cells {name[$((e + 3))].dut/mm3/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X1Y$((12*i+ 3 + c))   [get_cells {name[$((e + 3))].dut/mm4/uram_inst_rd}];" >> test.xdc;

#######################################
	echo "set_property LOC DSP48E2_X8Y$((18*i+ 0 + a))   [get_cells {name[$((e + 4))].dut/mm1/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+ 1 + a))   [get_cells {name[$((e + 4))].dut/mm1/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+ 2 + a))   [get_cells {name[$((e + 4))].dut/mm1/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+ 3 + a))   [get_cells {name[$((e + 4))].dut/mm1/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+ 4 + a))   [get_cells {name[$((e + 4))].dut/mm1/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+ 5 + a))   [get_cells {name[$((e + 4))].dut/mm1/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+ 6 + a))   [get_cells {name[$((e + 4))].dut/mm1/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+ 7 + a))   [get_cells {name[$((e + 4))].dut/mm1/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+ 8 + a))   [get_cells {name[$((e + 4))].dut/mm1/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+ 9 + a))   [get_cells {name[$((e + 4))].dut/mm2/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+10 + a))   [get_cells {name[$((e + 4))].dut/mm2/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+11 + a))   [get_cells {name[$((e + 4))].dut/mm2/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+12 + a))   [get_cells {name[$((e + 4))].dut/mm2/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+13 + a))   [get_cells {name[$((e + 4))].dut/mm2/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+14 + a))   [get_cells {name[$((e + 4))].dut/mm2/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+15 + a))   [get_cells {name[$((e + 4))].dut/mm2/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+16 + a))   [get_cells {name[$((e + 4))].dut/mm2/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X8Y$((18*i+17 + a))   [get_cells {name[$((e + 4))].dut/mm2/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+ 0 + a))   [get_cells {name[$((e + 4))].dut/mm3/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+ 1 + a))   [get_cells {name[$((e + 4))].dut/mm3/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+ 2 + a))   [get_cells {name[$((e + 4))].dut/mm3/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+ 3 + a))   [get_cells {name[$((e + 4))].dut/mm3/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+ 4 + a))   [get_cells {name[$((e + 4))].dut/mm3/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+ 5 + a))   [get_cells {name[$((e + 4))].dut/mm3/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+ 6 + a))   [get_cells {name[$((e + 4))].dut/mm3/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+ 7 + a))   [get_cells {name[$((e + 4))].dut/mm3/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+ 8 + a))   [get_cells {name[$((e + 4))].dut/mm3/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+ 9 + a))   [get_cells {name[$((e + 4))].dut/mm4/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+10 + a))   [get_cells {name[$((e + 4))].dut/mm4/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+11 + a))   [get_cells {name[$((e + 4))].dut/mm4/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+12 + a))   [get_cells {name[$((e + 4))].dut/mm4/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+13 + a))   [get_cells {name[$((e + 4))].dut/mm4/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+14 + a))   [get_cells {name[$((e + 4))].dut/mm4/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+15 + a))   [get_cells {name[$((e + 4))].dut/mm4/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+16 + a))   [get_cells {name[$((e + 4))].dut/mm4/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X9Y$((18*i+17 + a))   [get_cells {name[$((e + 4))].dut/mm4/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC RAMB18_X4Y$((13*i+ 0 + b))    [get_cells {name[$((e + 4))].dut/bram1}];" >> test.xdc;
	echo "set_property LOC RAMB18_X4Y$((13*i+ 1 + b))    [get_cells {name[$((e + 4))].dut/bram2}];" >> test.xdc;
	echo "set_property LOC RAMB18_X4Y$((13*i+ 2 + b))    [get_cells {name[$((e + 4))].dut/bram3}];" >> test.xdc;
	echo "set_property LOC RAMB18_X4Y$((13*i+ 3 + b))    [get_cells {name[$((e + 4))].dut/bram4}];" >> test.xdc;
	echo "set_property LOC RAMB18_X4Y$((13*i+ 4 + b))    [get_cells {name[$((e + 4))].dut/bram5}];" >> test.xdc;
	echo "set_property LOC RAMB18_X4Y$((13*i+ 5 + b))    [get_cells {name[$((e + 4))].dut/bram6}];" >> test.xdc;
	echo "set_property LOC RAMB18_X4Y$((13*i+ 6 + b))    [get_cells {name[$((e + 4))].dut/bram7}];" >> test.xdc;
	echo "set_property LOC RAMB18_X4Y$((13*i+ 7 + b))    [get_cells {name[$((e + 4))].dut/bram8}];" >> test.xdc;
	echo "set_property LOC RAMB18_X4Y$((13*i+ 8 + b))    [get_cells {name[$((e + 4))].dut/bram9}];" >> test.xdc;
	echo "set_property LOC RAMB18_X4Y$((13*i+ 9 + b))    [get_cells {name[$((e + 4))].dut/mm1/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X4Y$((13*i+10 + b))    [get_cells {name[$((e + 4))].dut/mm2/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X4Y$((13*i+11 + b))    [get_cells {name[$((e + 4))].dut/mm3/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X4Y$((13*i+12 + b))    [get_cells {name[$((e + 4))].dut/mm4/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC URAM288_X1Y$((12*i+ 4 + c))   [get_cells {name[$((e + 4))].dut/mm1/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X1Y$((12*i+ 5 + c))   [get_cells {name[$((e + 4))].dut/mm2/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X1Y$((12*i+ 6 + c))   [get_cells {name[$((e + 4))].dut/mm3/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X1Y$((12*i+ 7 + c))   [get_cells {name[$((e + 4))].dut/mm4/uram_inst_rd}];" >> test.xdc;

#######################################
	echo "set_property LOC DSP48E2_X10Y$((18*i+ 0 + a))   [get_cells {name[$((e + 5))].dut/mm1/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*i+ 1 + a))   [get_cells {name[$((e + 5))].dut/mm1/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*i+ 2 + a))   [get_cells {name[$((e + 5))].dut/mm1/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*i+ 3 + a))   [get_cells {name[$((e + 5))].dut/mm1/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*i+ 4 + a))   [get_cells {name[$((e + 5))].dut/mm1/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*i+ 5 + a))   [get_cells {name[$((e + 5))].dut/mm1/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*i+ 6 + a))   [get_cells {name[$((e + 5))].dut/mm1/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*i+ 7 + a))   [get_cells {name[$((e + 5))].dut/mm1/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*i+ 8 + a))   [get_cells {name[$((e + 5))].dut/mm1/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*i+ 9 + a))   [get_cells {name[$((e + 5))].dut/mm2/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*i+10 + a))   [get_cells {name[$((e + 5))].dut/mm2/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*i+11 + a))   [get_cells {name[$((e + 5))].dut/mm2/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*i+12 + a))   [get_cells {name[$((e + 5))].dut/mm2/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*i+13 + a))   [get_cells {name[$((e + 5))].dut/mm2/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*i+14 + a))   [get_cells {name[$((e + 5))].dut/mm2/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*i+15 + a))   [get_cells {name[$((e + 5))].dut/mm2/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*i+16 + a))   [get_cells {name[$((e + 5))].dut/mm2/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X10Y$((18*i+17 + a))   [get_cells {name[$((e + 5))].dut/mm2/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*i+ 0 + a))   [get_cells {name[$((e + 5))].dut/mm3/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*i+ 1 + a))   [get_cells {name[$((e + 5))].dut/mm3/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*i+ 2 + a))   [get_cells {name[$((e + 5))].dut/mm3/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*i+ 3 + a))   [get_cells {name[$((e + 5))].dut/mm3/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*i+ 4 + a))   [get_cells {name[$((e + 5))].dut/mm3/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*i+ 5 + a))   [get_cells {name[$((e + 5))].dut/mm3/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*i+ 6 + a))   [get_cells {name[$((e + 5))].dut/mm3/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*i+ 7 + a))   [get_cells {name[$((e + 5))].dut/mm3/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*i+ 8 + a))   [get_cells {name[$((e + 5))].dut/mm3/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*i+ 9 + a))   [get_cells {name[$((e + 5))].dut/mm4/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*i+10 + a))   [get_cells {name[$((e + 5))].dut/mm4/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*i+11 + a))   [get_cells {name[$((e + 5))].dut/mm4/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*i+12 + a))   [get_cells {name[$((e + 5))].dut/mm4/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*i+13 + a))   [get_cells {name[$((e + 5))].dut/mm4/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*i+14 + a))   [get_cells {name[$((e + 5))].dut/mm4/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*i+15 + a))   [get_cells {name[$((e + 5))].dut/mm4/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*i+16 + a))   [get_cells {name[$((e + 5))].dut/mm4/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X11Y$((18*i+17 + a))   [get_cells {name[$((e + 5))].dut/mm4/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC RAMB18_X5Y$((13*i+ 0 + b))     [get_cells {name[$((e + 5))].dut/bram1}];" >> test.xdc;
	echo "set_property LOC RAMB18_X5Y$((13*i+ 1 + b))     [get_cells {name[$((e + 5))].dut/bram2}];" >> test.xdc;
	echo "set_property LOC RAMB18_X5Y$((13*i+ 2 + b))     [get_cells {name[$((e + 5))].dut/bram3}];" >> test.xdc;
	echo "set_property LOC RAMB18_X5Y$((13*i+ 3 + b))     [get_cells {name[$((e + 5))].dut/bram4}];" >> test.xdc;
	echo "set_property LOC RAMB18_X5Y$((13*i+ 4 + b))     [get_cells {name[$((e + 5))].dut/bram5}];" >> test.xdc;
	echo "set_property LOC RAMB18_X5Y$((13*i+ 5 + b))     [get_cells {name[$((e + 5))].dut/bram6}];" >> test.xdc;
	echo "set_property LOC RAMB18_X5Y$((13*i+ 6 + b))     [get_cells {name[$((e + 5))].dut/bram7}];" >> test.xdc;
	echo "set_property LOC RAMB18_X5Y$((13*i+ 7 + b))     [get_cells {name[$((e + 5))].dut/bram8}];" >> test.xdc;
	echo "set_property LOC RAMB18_X5Y$((13*i+ 8 + b))     [get_cells {name[$((e + 5))].dut/bram9}];" >> test.xdc;
	echo "set_property LOC RAMB18_X5Y$((13*i+ 9 + b))     [get_cells {name[$((e + 5))].dut/mm1/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X5Y$((13*i+10 + b))     [get_cells {name[$((e + 5))].dut/mm2/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X5Y$((13*i+11 + b))     [get_cells {name[$((e + 5))].dut/mm3/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X5Y$((13*i+12 + b))     [get_cells {name[$((e + 5))].dut/mm4/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC URAM288_X1Y$((12*i+ 8 + c))    [get_cells {name[$((e + 5))].dut/mm1/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X1Y$((12*i+ 9 + c))    [get_cells {name[$((e + 5))].dut/mm2/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X1Y$((12*i+ 10 + c))   [get_cells {name[$((e + 5))].dut/mm3/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X1Y$((12*i+ 11 + c))   [get_cells {name[$((e + 5))].dut/mm4/uram_inst_rd}];" >> test.xdc;

#######################################
	echo "set_property LOC DSP48E2_X30Y$((18*i+ 0 + a))   [get_cells {name[$((e + 6))].dut/mm1/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+ 1 + a))   [get_cells {name[$((e + 6))].dut/mm1/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+ 2 + a))   [get_cells {name[$((e + 6))].dut/mm1/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+ 3 + a))   [get_cells {name[$((e + 6))].dut/mm1/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+ 4 + a))   [get_cells {name[$((e + 6))].dut/mm1/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+ 5 + a))   [get_cells {name[$((e + 6))].dut/mm1/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+ 6 + a))   [get_cells {name[$((e + 6))].dut/mm1/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+ 7 + a))   [get_cells {name[$((e + 6))].dut/mm1/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+ 8 + a))   [get_cells {name[$((e + 6))].dut/mm1/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+ 9 + a))   [get_cells {name[$((e + 6))].dut/mm2/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+10 + a))   [get_cells {name[$((e + 6))].dut/mm2/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+11 + a))   [get_cells {name[$((e + 6))].dut/mm2/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+12 + a))   [get_cells {name[$((e + 6))].dut/mm2/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+13 + a))   [get_cells {name[$((e + 6))].dut/mm2/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+14 + a))   [get_cells {name[$((e + 6))].dut/mm2/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+15 + a))   [get_cells {name[$((e + 6))].dut/mm2/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+16 + a))   [get_cells {name[$((e + 6))].dut/mm2/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X30Y$((18*i+17 + a))   [get_cells {name[$((e + 6))].dut/mm2/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+ 0 + a))   [get_cells {name[$((e + 6))].dut/mm3/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+ 1 + a))   [get_cells {name[$((e + 6))].dut/mm3/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+ 2 + a))   [get_cells {name[$((e + 6))].dut/mm3/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+ 3 + a))   [get_cells {name[$((e + 6))].dut/mm3/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+ 4 + a))   [get_cells {name[$((e + 6))].dut/mm3/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+ 5 + a))   [get_cells {name[$((e + 6))].dut/mm3/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+ 6 + a))   [get_cells {name[$((e + 6))].dut/mm3/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+ 7 + a))   [get_cells {name[$((e + 6))].dut/mm3/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+ 8 + a))   [get_cells {name[$((e + 6))].dut/mm3/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+ 9 + a))   [get_cells {name[$((e + 6))].dut/mm4/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+10 + a))   [get_cells {name[$((e + 6))].dut/mm4/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+11 + a))   [get_cells {name[$((e + 6))].dut/mm4/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+12 + a))   [get_cells {name[$((e + 6))].dut/mm4/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+13 + a))   [get_cells {name[$((e + 6))].dut/mm4/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+14 + a))   [get_cells {name[$((e + 6))].dut/mm4/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+15 + a))   [get_cells {name[$((e + 6))].dut/mm4/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+16 + a))   [get_cells {name[$((e + 6))].dut/mm4/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X31Y$((18*i+17 + a))   [get_cells {name[$((e + 6))].dut/mm4/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC RAMB18_X13Y$((13*i+ 0 + b))     [get_cells {name[$((e + 6))].dut/bram1}];" >> test.xdc;
	echo "set_property LOC RAMB18_X13Y$((13*i+ 1 + b))     [get_cells {name[$((e + 6))].dut/bram2}];" >> test.xdc;
	echo "set_property LOC RAMB18_X13Y$((13*i+ 2 + b))     [get_cells {name[$((e + 6))].dut/bram3}];" >> test.xdc;
	echo "set_property LOC RAMB18_X13Y$((13*i+ 3 + b))     [get_cells {name[$((e + 6))].dut/bram4}];" >> test.xdc;
	echo "set_property LOC RAMB18_X13Y$((13*i+ 4 + b))     [get_cells {name[$((e + 6))].dut/bram5}];" >> test.xdc;
	echo "set_property LOC RAMB18_X13Y$((13*i+ 5 + b))     [get_cells {name[$((e + 6))].dut/bram6}];" >> test.xdc;
	echo "set_property LOC RAMB18_X13Y$((13*i+ 6 + b))     [get_cells {name[$((e + 6))].dut/bram7}];" >> test.xdc;
	echo "set_property LOC RAMB18_X13Y$((13*i+ 7 + b))     [get_cells {name[$((e + 6))].dut/bram8}];" >> test.xdc;
	echo "set_property LOC RAMB18_X13Y$((13*i+ 8 + b))     [get_cells {name[$((e + 6))].dut/bram9}];" >> test.xdc;
	echo "set_property LOC RAMB18_X13Y$((13*i+ 9 + b))     [get_cells {name[$((e + 6))].dut/mm1/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X13Y$((13*i+10 + b))     [get_cells {name[$((e + 6))].dut/mm2/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X13Y$((13*i+11 + b))     [get_cells {name[$((e + 6))].dut/mm3/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X13Y$((13*i+12 + b))     [get_cells {name[$((e + 6))].dut/mm4/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC URAM288_X4Y$((12*i+ 0 + c))    [get_cells {name[$((e + 6))].dut/mm1/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X4Y$((12*i+ 1 + c))    [get_cells {name[$((e + 6))].dut/mm2/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X4Y$((12*i+ 2 + c))   [get_cells {name[$((e + 6))].dut/mm3/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X4Y$((12*i+ 3 + c))   [get_cells {name[$((e + 6))].dut/mm4/uram_inst_rd}];" >> test.xdc;

#######################################
	echo "set_property LOC DSP48E2_X28Y$((18*i+ 0 + a))   [get_cells {name[$((e + 7))].dut/mm1/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*i+ 1 + a))   [get_cells {name[$((e + 7))].dut/mm1/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*i+ 2 + a))   [get_cells {name[$((e + 7))].dut/mm1/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*i+ 3 + a))   [get_cells {name[$((e + 7))].dut/mm1/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*i+ 4 + a))   [get_cells {name[$((e + 7))].dut/mm1/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*i+ 5 + a))   [get_cells {name[$((e + 7))].dut/mm1/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*i+ 6 + a))   [get_cells {name[$((e + 7))].dut/mm1/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*i+ 7 + a))   [get_cells {name[$((e + 7))].dut/mm1/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*i+ 8 + a))   [get_cells {name[$((e + 7))].dut/mm1/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*i+ 9 + a))   [get_cells {name[$((e + 7))].dut/mm2/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*i+10 + a))   [get_cells {name[$((e + 7))].dut/mm2/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*i+11 + a))   [get_cells {name[$((e + 7))].dut/mm2/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*i+12 + a))   [get_cells {name[$((e + 7))].dut/mm2/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*i+13 + a))   [get_cells {name[$((e + 7))].dut/mm2/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*i+14 + a))   [get_cells {name[$((e + 7))].dut/mm2/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*i+15 + a))   [get_cells {name[$((e + 7))].dut/mm2/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*i+16 + a))   [get_cells {name[$((e + 7))].dut/mm2/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X28Y$((18*i+17 + a))   [get_cells {name[$((e + 7))].dut/mm2/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*i+ 0 + a))   [get_cells {name[$((e + 7))].dut/mm3/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*i+ 1 + a))   [get_cells {name[$((e + 7))].dut/mm3/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*i+ 2 + a))   [get_cells {name[$((e + 7))].dut/mm3/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*i+ 3 + a))   [get_cells {name[$((e + 7))].dut/mm3/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*i+ 4 + a))   [get_cells {name[$((e + 7))].dut/mm3/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*i+ 5 + a))   [get_cells {name[$((e + 7))].dut/mm3/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*i+ 6 + a))   [get_cells {name[$((e + 7))].dut/mm3/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*i+ 7 + a))   [get_cells {name[$((e + 7))].dut/mm3/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*i+ 8 + a))   [get_cells {name[$((e + 7))].dut/mm3/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*i+ 9 + a))   [get_cells {name[$((e + 7))].dut/mm4/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*i+10 + a))   [get_cells {name[$((e + 7))].dut/mm4/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*i+11 + a))   [get_cells {name[$((e + 7))].dut/mm4/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*i+12 + a))   [get_cells {name[$((e + 7))].dut/mm4/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*i+13 + a))   [get_cells {name[$((e + 7))].dut/mm4/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*i+14 + a))   [get_cells {name[$((e + 7))].dut/mm4/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*i+15 + a))   [get_cells {name[$((e + 7))].dut/mm4/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*i+16 + a))   [get_cells {name[$((e + 7))].dut/mm4/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X29Y$((18*i+17 + a))   [get_cells {name[$((e + 7))].dut/mm4/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC RAMB18_X12Y$((13*i+ 0 + b))    [get_cells {name[$((e + 7))].dut/bram1}];" >> test.xdc;
	echo "set_property LOC RAMB18_X12Y$((13*i+ 1 + b))    [get_cells {name[$((e + 7))].dut/bram2}];" >> test.xdc;
	echo "set_property LOC RAMB18_X12Y$((13*i+ 2 + b))    [get_cells {name[$((e + 7))].dut/bram3}];" >> test.xdc;
	echo "set_property LOC RAMB18_X12Y$((13*i+ 3 + b))    [get_cells {name[$((e + 7))].dut/bram4}];" >> test.xdc;
	echo "set_property LOC RAMB18_X12Y$((13*i+ 4 + b))    [get_cells {name[$((e + 7))].dut/bram5}];" >> test.xdc;
	echo "set_property LOC RAMB18_X12Y$((13*i+ 5 + b))    [get_cells {name[$((e + 7))].dut/bram6}];" >> test.xdc;
	echo "set_property LOC RAMB18_X12Y$((13*i+ 6 + b))    [get_cells {name[$((e + 7))].dut/bram7}];" >> test.xdc;
	echo "set_property LOC RAMB18_X12Y$((13*i+ 7 + b))    [get_cells {name[$((e + 7))].dut/bram8}];" >> test.xdc;
	echo "set_property LOC RAMB18_X12Y$((13*i+ 8 + b))    [get_cells {name[$((e + 7))].dut/bram9}];" >> test.xdc;
	echo "set_property LOC RAMB18_X12Y$((13*i+ 9 + b))    [get_cells {name[$((e + 7))].dut/mm1/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X12Y$((13*i+10 + b))    [get_cells {name[$((e + 7))].dut/mm2/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X12Y$((13*i+11 + b))    [get_cells {name[$((e + 7))].dut/mm3/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X12Y$((13*i+12 + b))    [get_cells {name[$((e + 7))].dut/mm4/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC URAM288_X4Y$((12*i+ 4 + c))   [get_cells {name[$((e + 7))].dut/mm1/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X4Y$((12*i+ 5 + c))   [get_cells {name[$((e + 7))].dut/mm2/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X4Y$((12*i+ 6 + c))   [get_cells {name[$((e + 7))].dut/mm3/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X4Y$((12*i+ 7 + c))   [get_cells {name[$((e + 7))].dut/mm4/uram_inst_rd}];" >> test.xdc;

#######################################
	echo "set_property LOC DSP48E2_X26Y$((18*i+ 0 + a))   [get_cells {name[$((e + 8))].dut/mm1/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*i+ 1 + a))   [get_cells {name[$((e + 8))].dut/mm1/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*i+ 2 + a))   [get_cells {name[$((e + 8))].dut/mm1/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*i+ 3 + a))   [get_cells {name[$((e + 8))].dut/mm1/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*i+ 4 + a))   [get_cells {name[$((e + 8))].dut/mm1/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*i+ 5 + a))   [get_cells {name[$((e + 8))].dut/mm1/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*i+ 6 + a))   [get_cells {name[$((e + 8))].dut/mm1/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*i+ 7 + a))   [get_cells {name[$((e + 8))].dut/mm1/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*i+ 8 + a))   [get_cells {name[$((e + 8))].dut/mm1/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*i+ 9 + a))   [get_cells {name[$((e + 8))].dut/mm2/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*i+10 + a))   [get_cells {name[$((e + 8))].dut/mm2/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*i+11 + a))   [get_cells {name[$((e + 8))].dut/mm2/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*i+12 + a))   [get_cells {name[$((e + 8))].dut/mm2/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*i+13 + a))   [get_cells {name[$((e + 8))].dut/mm2/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*i+14 + a))   [get_cells {name[$((e + 8))].dut/mm2/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*i+15 + a))   [get_cells {name[$((e + 8))].dut/mm2/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*i+16 + a))   [get_cells {name[$((e + 8))].dut/mm2/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X26Y$((18*i+17 + a))   [get_cells {name[$((e + 8))].dut/mm2/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*i+ 0 + a))   [get_cells {name[$((e + 8))].dut/mm3/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*i+ 1 + a))   [get_cells {name[$((e + 8))].dut/mm3/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*i+ 2 + a))   [get_cells {name[$((e + 8))].dut/mm3/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*i+ 3 + a))   [get_cells {name[$((e + 8))].dut/mm3/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*i+ 4 + a))   [get_cells {name[$((e + 8))].dut/mm3/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*i+ 5 + a))   [get_cells {name[$((e + 8))].dut/mm3/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*i+ 6 + a))   [get_cells {name[$((e + 8))].dut/mm3/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*i+ 7 + a))   [get_cells {name[$((e + 8))].dut/mm3/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*i+ 8 + a))   [get_cells {name[$((e + 8))].dut/mm3/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*i+ 9 + a))   [get_cells {name[$((e + 8))].dut/mm4/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*i+10 + a))   [get_cells {name[$((e + 8))].dut/mm4/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*i+11 + a))   [get_cells {name[$((e + 8))].dut/mm4/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*i+12 + a))   [get_cells {name[$((e + 8))].dut/mm4/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*i+13 + a))   [get_cells {name[$((e + 8))].dut/mm4/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*i+14 + a))   [get_cells {name[$((e + 8))].dut/mm4/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*i+15 + a))   [get_cells {name[$((e + 8))].dut/mm4/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*i+16 + a))   [get_cells {name[$((e + 8))].dut/mm4/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X27Y$((18*i+17 + a))   [get_cells {name[$((e + 8))].dut/mm4/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC RAMB18_X11Y$((13*i+ 0 + b))     [get_cells {name[$((e + 8))].dut/bram1}];" >> test.xdc;
	echo "set_property LOC RAMB18_X11Y$((13*i+ 1 + b))     [get_cells {name[$((e + 8))].dut/bram2}];" >> test.xdc;
	echo "set_property LOC RAMB18_X11Y$((13*i+ 2 + b))     [get_cells {name[$((e + 8))].dut/bram3}];" >> test.xdc;
	echo "set_property LOC RAMB18_X11Y$((13*i+ 3 + b))     [get_cells {name[$((e + 8))].dut/bram4}];" >> test.xdc;
	echo "set_property LOC RAMB18_X11Y$((13*i+ 4 + b))     [get_cells {name[$((e + 8))].dut/bram5}];" >> test.xdc;
	echo "set_property LOC RAMB18_X11Y$((13*i+ 5 + b))     [get_cells {name[$((e + 8))].dut/bram6}];" >> test.xdc;
	echo "set_property LOC RAMB18_X11Y$((13*i+ 6 + b))     [get_cells {name[$((e + 8))].dut/bram7}];" >> test.xdc;
	echo "set_property LOC RAMB18_X11Y$((13*i+ 7 + b))     [get_cells {name[$((e + 8))].dut/bram8}];" >> test.xdc;
	echo "set_property LOC RAMB18_X11Y$((13*i+ 8 + b))     [get_cells {name[$((e + 8))].dut/bram9}];" >> test.xdc;
	echo "set_property LOC RAMB18_X11Y$((13*i+ 9 + b))     [get_cells {name[$((e + 8))].dut/mm1/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X11Y$((13*i+10 + b))     [get_cells {name[$((e + 8))].dut/mm2/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X11Y$((13*i+11 + b))     [get_cells {name[$((e + 8))].dut/mm3/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X11Y$((13*i+12 + b))     [get_cells {name[$((e + 8))].dut/mm4/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC URAM288_X4Y$((12*i+ 8 + c))    [get_cells {name[$((e + 8))].dut/mm1/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X4Y$((12*i+ 9 + c))    [get_cells {name[$((e + 8))].dut/mm2/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X4Y$((12*i+ 10 + c))   [get_cells {name[$((e + 8))].dut/mm3/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X4Y$((12*i+ 11 + c))   [get_cells {name[$((e + 8))].dut/mm4/uram_inst_rd}];" >> test.xdc;

#######################################
	echo "set_property LOC DSP48E2_X24Y$((18*i+ 0 + a))   [get_cells {name[$((e + 9))].dut/mm1/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*i+ 1 + a))   [get_cells {name[$((e + 9))].dut/mm1/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*i+ 2 + a))   [get_cells {name[$((e + 9))].dut/mm1/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*i+ 3 + a))   [get_cells {name[$((e + 9))].dut/mm1/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*i+ 4 + a))   [get_cells {name[$((e + 9))].dut/mm1/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*i+ 5 + a))   [get_cells {name[$((e + 9))].dut/mm1/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*i+ 6 + a))   [get_cells {name[$((e + 9))].dut/mm1/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*i+ 7 + a))   [get_cells {name[$((e + 9))].dut/mm1/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*i+ 8 + a))   [get_cells {name[$((e + 9))].dut/mm1/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*i+ 9 + a))   [get_cells {name[$((e + 9))].dut/mm2/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*i+10 + a))   [get_cells {name[$((e + 9))].dut/mm2/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*i+11 + a))   [get_cells {name[$((e + 9))].dut/mm2/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*i+12 + a))   [get_cells {name[$((e + 9))].dut/mm2/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*i+13 + a))   [get_cells {name[$((e + 9))].dut/mm2/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*i+14 + a))   [get_cells {name[$((e + 9))].dut/mm2/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*i+15 + a))   [get_cells {name[$((e + 9))].dut/mm2/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*i+16 + a))   [get_cells {name[$((e + 9))].dut/mm2/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X24Y$((18*i+17 + a))   [get_cells {name[$((e + 9))].dut/mm2/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*i+ 0 + a))   [get_cells {name[$((e + 9))].dut/mm3/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*i+ 1 + a))   [get_cells {name[$((e + 9))].dut/mm3/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*i+ 2 + a))   [get_cells {name[$((e + 9))].dut/mm3/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*i+ 3 + a))   [get_cells {name[$((e + 9))].dut/mm3/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*i+ 4 + a))   [get_cells {name[$((e + 9))].dut/mm3/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*i+ 5 + a))   [get_cells {name[$((e + 9))].dut/mm3/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*i+ 6 + a))   [get_cells {name[$((e + 9))].dut/mm3/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*i+ 7 + a))   [get_cells {name[$((e + 9))].dut/mm3/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*i+ 8 + a))   [get_cells {name[$((e + 9))].dut/mm3/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*i+ 9 + a))   [get_cells {name[$((e + 9))].dut/mm4/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*i+10 + a))   [get_cells {name[$((e + 9))].dut/mm4/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*i+11 + a))   [get_cells {name[$((e + 9))].dut/mm4/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*i+12 + a))   [get_cells {name[$((e + 9))].dut/mm4/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*i+13 + a))   [get_cells {name[$((e + 9))].dut/mm4/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*i+14 + a))   [get_cells {name[$((e + 9))].dut/mm4/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*i+15 + a))   [get_cells {name[$((e + 9))].dut/mm4/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*i+16 + a))   [get_cells {name[$((e + 9))].dut/mm4/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X25Y$((18*i+17 + a))   [get_cells {name[$((e + 9))].dut/mm4/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC RAMB18_X10Y$((13*i+ 0 + b))     [get_cells {name[$((e + 9))].dut/bram1}];" >> test.xdc;
	echo "set_property LOC RAMB18_X10Y$((13*i+ 1 + b))     [get_cells {name[$((e + 9))].dut/bram2}];" >> test.xdc;
	echo "set_property LOC RAMB18_X10Y$((13*i+ 2 + b))     [get_cells {name[$((e + 9))].dut/bram3}];" >> test.xdc;
	echo "set_property LOC RAMB18_X10Y$((13*i+ 3 + b))     [get_cells {name[$((e + 9))].dut/bram4}];" >> test.xdc;
	echo "set_property LOC RAMB18_X10Y$((13*i+ 4 + b))     [get_cells {name[$((e + 9))].dut/bram5}];" >> test.xdc;
	echo "set_property LOC RAMB18_X10Y$((13*i+ 5 + b))     [get_cells {name[$((e + 9))].dut/bram6}];" >> test.xdc;
	echo "set_property LOC RAMB18_X10Y$((13*i+ 6 + b))     [get_cells {name[$((e + 9))].dut/bram7}];" >> test.xdc;
	echo "set_property LOC RAMB18_X10Y$((13*i+ 7 + b))     [get_cells {name[$((e + 9))].dut/bram8}];" >> test.xdc;
	echo "set_property LOC RAMB18_X10Y$((13*i+ 8 + b))     [get_cells {name[$((e + 9))].dut/bram9}];" >> test.xdc;
	echo "set_property LOC RAMB18_X10Y$((13*i+ 9 + b))     [get_cells {name[$((e + 9))].dut/mm1/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X10Y$((13*i+10 + b))     [get_cells {name[$((e + 9))].dut/mm2/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X10Y$((13*i+11 + b))     [get_cells {name[$((e + 9))].dut/mm3/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X10Y$((13*i+12 + b))     [get_cells {name[$((e + 9))].dut/mm4/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC URAM288_X3Y$((12*i+ 0 + c))    [get_cells {name[$((e + 9))].dut/mm1/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X3Y$((12*i+ 1 + c))    [get_cells {name[$((e + 9))].dut/mm2/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X3Y$((12*i+ 2 + c))   [get_cells {name[$((e + 9))].dut/mm3/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X3Y$((12*i+ 3 + c))   [get_cells {name[$((e + 9))].dut/mm4/uram_inst_rd}];" >> test.xdc;

#######################################
	echo "set_property LOC DSP48E2_X22Y$((18*i+ 0 + a))   [get_cells {name[$((e + 10))].dut/mm1/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*i+ 1 + a))   [get_cells {name[$((e + 10))].dut/mm1/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*i+ 2 + a))   [get_cells {name[$((e + 10))].dut/mm1/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*i+ 3 + a))   [get_cells {name[$((e + 10))].dut/mm1/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*i+ 4 + a))   [get_cells {name[$((e + 10))].dut/mm1/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*i+ 5 + a))   [get_cells {name[$((e + 10))].dut/mm1/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*i+ 6 + a))   [get_cells {name[$((e + 10))].dut/mm1/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*i+ 7 + a))   [get_cells {name[$((e + 10))].dut/mm1/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*i+ 8 + a))   [get_cells {name[$((e + 10))].dut/mm1/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*i+ 9 + a))   [get_cells {name[$((e + 10))].dut/mm2/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*i+10 + a))   [get_cells {name[$((e + 10))].dut/mm2/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*i+11 + a))   [get_cells {name[$((e + 10))].dut/mm2/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*i+12 + a))   [get_cells {name[$((e + 10))].dut/mm2/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*i+13 + a))   [get_cells {name[$((e + 10))].dut/mm2/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*i+14 + a))   [get_cells {name[$((e + 10))].dut/mm2/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*i+15 + a))   [get_cells {name[$((e + 10))].dut/mm2/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*i+16 + a))   [get_cells {name[$((e + 10))].dut/mm2/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X22Y$((18*i+17 + a))   [get_cells {name[$((e + 10))].dut/mm2/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*i+ 0 + a))   [get_cells {name[$((e + 10))].dut/mm3/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*i+ 1 + a))   [get_cells {name[$((e + 10))].dut/mm3/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*i+ 2 + a))   [get_cells {name[$((e + 10))].dut/mm3/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*i+ 3 + a))   [get_cells {name[$((e + 10))].dut/mm3/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*i+ 4 + a))   [get_cells {name[$((e + 10))].dut/mm3/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*i+ 5 + a))   [get_cells {name[$((e + 10))].dut/mm3/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*i+ 6 + a))   [get_cells {name[$((e + 10))].dut/mm3/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*i+ 7 + a))   [get_cells {name[$((e + 10))].dut/mm3/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*i+ 8 + a))   [get_cells {name[$((e + 10))].dut/mm3/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*i+ 9 + a))   [get_cells {name[$((e + 10))].dut/mm4/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*i+10 + a))   [get_cells {name[$((e + 10))].dut/mm4/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*i+11 + a))   [get_cells {name[$((e + 10))].dut/mm4/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*i+12 + a))   [get_cells {name[$((e + 10))].dut/mm4/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*i+13 + a))   [get_cells {name[$((e + 10))].dut/mm4/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*i+14 + a))   [get_cells {name[$((e + 10))].dut/mm4/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*i+15 + a))   [get_cells {name[$((e + 10))].dut/mm4/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*i+16 + a))   [get_cells {name[$((e + 10))].dut/mm4/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X23Y$((18*i+17 + a))   [get_cells {name[$((e + 10))].dut/mm4/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC RAMB18_X9Y$((13*i+ 0 + b))    [get_cells {name[$((e + 10))].dut/bram1}];" >> test.xdc;
	echo "set_property LOC RAMB18_X9Y$((13*i+ 1 + b))    [get_cells {name[$((e + 10))].dut/bram2}];" >> test.xdc;
	echo "set_property LOC RAMB18_X9Y$((13*i+ 2 + b))    [get_cells {name[$((e + 10))].dut/bram3}];" >> test.xdc;
	echo "set_property LOC RAMB18_X9Y$((13*i+ 3 + b))    [get_cells {name[$((e + 10))].dut/bram4}];" >> test.xdc;
	echo "set_property LOC RAMB18_X9Y$((13*i+ 4 + b))    [get_cells {name[$((e + 10))].dut/bram5}];" >> test.xdc;
	echo "set_property LOC RAMB18_X9Y$((13*i+ 5 + b))    [get_cells {name[$((e + 10))].dut/bram6}];" >> test.xdc;
	echo "set_property LOC RAMB18_X9Y$((13*i+ 6 + b))    [get_cells {name[$((e + 10))].dut/bram7}];" >> test.xdc;
	echo "set_property LOC RAMB18_X9Y$((13*i+ 7 + b))    [get_cells {name[$((e + 10))].dut/bram8}];" >> test.xdc;
	echo "set_property LOC RAMB18_X9Y$((13*i+ 8 + b))    [get_cells {name[$((e + 10))].dut/bram9}];" >> test.xdc;
	echo "set_property LOC RAMB18_X9Y$((13*i+ 9 + b))    [get_cells {name[$((e + 10))].dut/mm1/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X9Y$((13*i+10 + b))    [get_cells {name[$((e + 10))].dut/mm2/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X9Y$((13*i+11 + b))    [get_cells {name[$((e + 10))].dut/mm3/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X9Y$((13*i+12 + b))    [get_cells {name[$((e + 10))].dut/mm4/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC URAM288_X3Y$((12*i+ 4 + c))   [get_cells {name[$((e + 10))].dut/mm1/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X3Y$((12*i+ 5 + c))   [get_cells {name[$((e + 10))].dut/mm2/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X3Y$((12*i+ 6 + c))   [get_cells {name[$((e + 10))].dut/mm3/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X3Y$((12*i+ 7 + c))   [get_cells {name[$((e + 10))].dut/mm4/uram_inst_rd}];" >> test.xdc;

#######################################
	echo "set_property LOC DSP48E2_X20Y$((18*i+ 0 + a))   [get_cells {name[$((e + 11))].dut/mm1/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*i+ 1 + a))   [get_cells {name[$((e + 11))].dut/mm1/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*i+ 2 + a))   [get_cells {name[$((e + 11))].dut/mm1/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*i+ 3 + a))   [get_cells {name[$((e + 11))].dut/mm1/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*i+ 4 + a))   [get_cells {name[$((e + 11))].dut/mm1/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*i+ 5 + a))   [get_cells {name[$((e + 11))].dut/mm1/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*i+ 6 + a))   [get_cells {name[$((e + 11))].dut/mm1/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*i+ 7 + a))   [get_cells {name[$((e + 11))].dut/mm1/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*i+ 8 + a))   [get_cells {name[$((e + 11))].dut/mm1/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*i+ 9 + a))   [get_cells {name[$((e + 11))].dut/mm2/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*i+10 + a))   [get_cells {name[$((e + 11))].dut/mm2/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*i+11 + a))   [get_cells {name[$((e + 11))].dut/mm2/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*i+12 + a))   [get_cells {name[$((e + 11))].dut/mm2/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*i+13 + a))   [get_cells {name[$((e + 11))].dut/mm2/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*i+14 + a))   [get_cells {name[$((e + 11))].dut/mm2/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*i+15 + a))   [get_cells {name[$((e + 11))].dut/mm2/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*i+16 + a))   [get_cells {name[$((e + 11))].dut/mm2/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X20Y$((18*i+17 + a))   [get_cells {name[$((e + 11))].dut/mm2/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*i+ 0 + a))   [get_cells {name[$((e + 11))].dut/mm3/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*i+ 1 + a))   [get_cells {name[$((e + 11))].dut/mm3/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*i+ 2 + a))   [get_cells {name[$((e + 11))].dut/mm3/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*i+ 3 + a))   [get_cells {name[$((e + 11))].dut/mm3/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*i+ 4 + a))   [get_cells {name[$((e + 11))].dut/mm3/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*i+ 5 + a))   [get_cells {name[$((e + 11))].dut/mm3/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*i+ 6 + a))   [get_cells {name[$((e + 11))].dut/mm3/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*i+ 7 + a))   [get_cells {name[$((e + 11))].dut/mm3/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*i+ 8 + a))   [get_cells {name[$((e + 11))].dut/mm3/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*i+ 9 + a))   [get_cells {name[$((e + 11))].dut/mm4/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*i+10 + a))   [get_cells {name[$((e + 11))].dut/mm4/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*i+11 + a))   [get_cells {name[$((e + 11))].dut/mm4/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*i+12 + a))   [get_cells {name[$((e + 11))].dut/mm4/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*i+13 + a))   [get_cells {name[$((e + 11))].dut/mm4/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*i+14 + a))   [get_cells {name[$((e + 11))].dut/mm4/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*i+15 + a))   [get_cells {name[$((e + 11))].dut/mm4/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*i+16 + a))   [get_cells {name[$((e + 11))].dut/mm4/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X21Y$((18*i+17 + a))   [get_cells {name[$((e + 11))].dut/mm4/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC RAMB18_X8Y$((13*i+ 0 + b))     [get_cells {name[$((e + 11))].dut/bram1}];" >> test.xdc;
	echo "set_property LOC RAMB18_X8Y$((13*i+ 1 + b))     [get_cells {name[$((e + 11))].dut/bram2}];" >> test.xdc;
	echo "set_property LOC RAMB18_X8Y$((13*i+ 2 + b))     [get_cells {name[$((e + 11))].dut/bram3}];" >> test.xdc;
	echo "set_property LOC RAMB18_X8Y$((13*i+ 3 + b))     [get_cells {name[$((e + 11))].dut/bram4}];" >> test.xdc;
	echo "set_property LOC RAMB18_X8Y$((13*i+ 4 + b))     [get_cells {name[$((e + 11))].dut/bram5}];" >> test.xdc;
	echo "set_property LOC RAMB18_X8Y$((13*i+ 5 + b))     [get_cells {name[$((e + 11))].dut/bram6}];" >> test.xdc;
	echo "set_property LOC RAMB18_X8Y$((13*i+ 6 + b))     [get_cells {name[$((e + 11))].dut/bram7}];" >> test.xdc;
	echo "set_property LOC RAMB18_X8Y$((13*i+ 7 + b))     [get_cells {name[$((e + 11))].dut/bram8}];" >> test.xdc;
	echo "set_property LOC RAMB18_X8Y$((13*i+ 8 + b))     [get_cells {name[$((e + 11))].dut/bram9}];" >> test.xdc;
	echo "set_property LOC RAMB18_X8Y$((13*i+ 9 + b))     [get_cells {name[$((e + 11))].dut/mm1/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X8Y$((13*i+10 + b))     [get_cells {name[$((e + 11))].dut/mm2/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X8Y$((13*i+11 + b))     [get_cells {name[$((e + 11))].dut/mm3/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X8Y$((13*i+12 + b))     [get_cells {name[$((e + 11))].dut/mm4/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC URAM288_X3Y$((12*i+ 8 + c))    [get_cells {name[$((e + 11))].dut/mm1/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X3Y$((12*i+ 9 + c))    [get_cells {name[$((e + 11))].dut/mm2/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X3Y$((12*i+ 10 + c))   [get_cells {name[$((e + 11))].dut/mm3/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X3Y$((12*i+ 11 + c))   [get_cells {name[$((e + 11))].dut/mm4/uram_inst_rd}];" >> test.xdc;

#######################################
	echo "set_property LOC DSP48E2_X14Y$((18*i+ 0 + a))   [get_cells {name[$((e + 12))].dut/mm1/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+ 1 + a))   [get_cells {name[$((e + 12))].dut/mm1/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+ 2 + a))   [get_cells {name[$((e + 12))].dut/mm1/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+ 3 + a))   [get_cells {name[$((e + 12))].dut/mm1/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+ 4 + a))   [get_cells {name[$((e + 12))].dut/mm1/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+ 5 + a))   [get_cells {name[$((e + 12))].dut/mm1/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+ 6 + a))   [get_cells {name[$((e + 12))].dut/mm1/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+ 7 + a))   [get_cells {name[$((e + 12))].dut/mm1/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+ 8 + a))   [get_cells {name[$((e + 12))].dut/mm1/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+ 9 + a))   [get_cells {name[$((e + 12))].dut/mm2/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+10 + a))   [get_cells {name[$((e + 12))].dut/mm2/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+11 + a))   [get_cells {name[$((e + 12))].dut/mm2/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+12 + a))   [get_cells {name[$((e + 12))].dut/mm2/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+13 + a))   [get_cells {name[$((e + 12))].dut/mm2/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+14 + a))   [get_cells {name[$((e + 12))].dut/mm2/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+15 + a))   [get_cells {name[$((e + 12))].dut/mm2/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+16 + a))   [get_cells {name[$((e + 12))].dut/mm2/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X14Y$((18*i+17 + a))   [get_cells {name[$((e + 12))].dut/mm2/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+ 0 + a))   [get_cells {name[$((e + 12))].dut/mm3/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+ 1 + a))   [get_cells {name[$((e + 12))].dut/mm3/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+ 2 + a))   [get_cells {name[$((e + 12))].dut/mm3/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+ 3 + a))   [get_cells {name[$((e + 12))].dut/mm3/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+ 4 + a))   [get_cells {name[$((e + 12))].dut/mm3/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+ 5 + a))   [get_cells {name[$((e + 12))].dut/mm3/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+ 6 + a))   [get_cells {name[$((e + 12))].dut/mm3/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+ 7 + a))   [get_cells {name[$((e + 12))].dut/mm3/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+ 8 + a))   [get_cells {name[$((e + 12))].dut/mm3/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+ 9 + a))   [get_cells {name[$((e + 12))].dut/mm4/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+10 + a))   [get_cells {name[$((e + 12))].dut/mm4/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+11 + a))   [get_cells {name[$((e + 12))].dut/mm4/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+12 + a))   [get_cells {name[$((e + 12))].dut/mm4/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+13 + a))   [get_cells {name[$((e + 12))].dut/mm4/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+14 + a))   [get_cells {name[$((e + 12))].dut/mm4/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+15 + a))   [get_cells {name[$((e + 12))].dut/mm4/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+16 + a))   [get_cells {name[$((e + 12))].dut/mm4/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X15Y$((18*i+17 + a))   [get_cells {name[$((e + 12))].dut/mm4/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC RAMB18_X7Y$((13*i+ 0 + b))     [get_cells {name[$((e + 12))].dut/bram1}];" >> test.xdc;
	echo "set_property LOC RAMB18_X7Y$((13*i+ 1 + b))     [get_cells {name[$((e + 12))].dut/bram2}];" >> test.xdc;
	echo "set_property LOC RAMB18_X7Y$((13*i+ 2 + b))     [get_cells {name[$((e + 12))].dut/bram3}];" >> test.xdc;
	echo "set_property LOC RAMB18_X7Y$((13*i+ 3 + b))     [get_cells {name[$((e + 12))].dut/bram4}];" >> test.xdc;
	echo "set_property LOC RAMB18_X7Y$((13*i+ 4 + b))     [get_cells {name[$((e + 12))].dut/bram5}];" >> test.xdc;
	echo "set_property LOC RAMB18_X7Y$((13*i+ 5 + b))     [get_cells {name[$((e + 12))].dut/bram6}];" >> test.xdc;
	echo "set_property LOC RAMB18_X7Y$((13*i+ 6 + b))     [get_cells {name[$((e + 12))].dut/bram7}];" >> test.xdc;
	echo "set_property LOC RAMB18_X7Y$((13*i+ 7 + b))     [get_cells {name[$((e + 12))].dut/bram8}];" >> test.xdc;
	echo "set_property LOC RAMB18_X7Y$((13*i+ 8 + b))     [get_cells {name[$((e + 12))].dut/bram9}];" >> test.xdc;
	echo "set_property LOC RAMB18_X7Y$((13*i+ 9 + b))     [get_cells {name[$((e + 12))].dut/mm1/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X7Y$((13*i+10 + b))     [get_cells {name[$((e + 12))].dut/mm2/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X7Y$((13*i+11 + b))     [get_cells {name[$((e + 12))].dut/mm3/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X7Y$((13*i+12 + b))     [get_cells {name[$((e + 12))].dut/mm4/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC URAM288_X2Y$((8*i+ 0 + c))    [get_cells {name[$((e + 12))].dut/mm1/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X2Y$((8*i+ 1 + c))    [get_cells {name[$((e + 12))].dut/mm2/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X2Y$((8*i+ 2 + c))   [get_cells {name[$((e + 12))].dut/mm3/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X2Y$((8*i+ 3 + c))   [get_cells {name[$((e + 12))].dut/mm4/uram_inst_rd}];" >> test.xdc;

#######################################
	echo "set_property LOC DSP48E2_X12Y$((18*i+ 0 + a))   [get_cells {name[$((e + 13))].dut/mm1/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*i+ 1 + a))   [get_cells {name[$((e + 13))].dut/mm1/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*i+ 2 + a))   [get_cells {name[$((e + 13))].dut/mm1/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*i+ 3 + a))   [get_cells {name[$((e + 13))].dut/mm1/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*i+ 4 + a))   [get_cells {name[$((e + 13))].dut/mm1/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*i+ 5 + a))   [get_cells {name[$((e + 13))].dut/mm1/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*i+ 6 + a))   [get_cells {name[$((e + 13))].dut/mm1/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*i+ 7 + a))   [get_cells {name[$((e + 13))].dut/mm1/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*i+ 8 + a))   [get_cells {name[$((e + 13))].dut/mm1/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*i+ 9 + a))   [get_cells {name[$((e + 13))].dut/mm2/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*i+10 + a))   [get_cells {name[$((e + 13))].dut/mm2/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*i+11 + a))   [get_cells {name[$((e + 13))].dut/mm2/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*i+12 + a))   [get_cells {name[$((e + 13))].dut/mm2/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*i+13 + a))   [get_cells {name[$((e + 13))].dut/mm2/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*i+14 + a))   [get_cells {name[$((e + 13))].dut/mm2/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*i+15 + a))   [get_cells {name[$((e + 13))].dut/mm2/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*i+16 + a))   [get_cells {name[$((e + 13))].dut/mm2/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X12Y$((18*i+17 + a))   [get_cells {name[$((e + 13))].dut/mm2/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+ 0 + a))   [get_cells {name[$((e + 13))].dut/mm3/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+ 1 + a))   [get_cells {name[$((e + 13))].dut/mm3/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+ 2 + a))   [get_cells {name[$((e + 13))].dut/mm3/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+ 3 + a))   [get_cells {name[$((e + 13))].dut/mm3/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+ 4 + a))   [get_cells {name[$((e + 13))].dut/mm3/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+ 5 + a))   [get_cells {name[$((e + 13))].dut/mm3/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+ 6 + a))   [get_cells {name[$((e + 13))].dut/mm3/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+ 7 + a))   [get_cells {name[$((e + 13))].dut/mm3/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+ 8 + a))   [get_cells {name[$((e + 13))].dut/mm3/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+ 9 + a))   [get_cells {name[$((e + 13))].dut/mm4/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+10 + a))   [get_cells {name[$((e + 13))].dut/mm4/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+11 + a))   [get_cells {name[$((e + 13))].dut/mm4/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+12 + a))   [get_cells {name[$((e + 13))].dut/mm4/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+13 + a))   [get_cells {name[$((e + 13))].dut/mm4/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+14 + a))   [get_cells {name[$((e + 13))].dut/mm4/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+15 + a))   [get_cells {name[$((e + 13))].dut/mm4/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+16 + a))   [get_cells {name[$((e + 13))].dut/mm4/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X13Y$((18*i+17 + a))   [get_cells {name[$((e + 13))].dut/mm4/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC RAMB18_X6Y$((13*i+ 0 + b))    [get_cells {name[$((e + 13))].dut/bram1}];" >> test.xdc;
	echo "set_property LOC RAMB18_X6Y$((13*i+ 1 + b))    [get_cells {name[$((e + 13))].dut/bram2}];" >> test.xdc;
	echo "set_property LOC RAMB18_X6Y$((13*i+ 2 + b))    [get_cells {name[$((e + 13))].dut/bram3}];" >> test.xdc;
	echo "set_property LOC RAMB18_X6Y$((13*i+ 3 + b))    [get_cells {name[$((e + 13))].dut/bram4}];" >> test.xdc;
	echo "set_property LOC RAMB18_X6Y$((13*i+ 4 + b))    [get_cells {name[$((e + 13))].dut/bram5}];" >> test.xdc;
	echo "set_property LOC RAMB18_X6Y$((13*i+ 5 + b))    [get_cells {name[$((e + 13))].dut/bram6}];" >> test.xdc;
	echo "set_property LOC RAMB18_X6Y$((13*i+ 6 + b))    [get_cells {name[$((e + 13))].dut/bram7}];" >> test.xdc;
	echo "set_property LOC RAMB18_X6Y$((13*i+ 7 + b))    [get_cells {name[$((e + 13))].dut/bram8}];" >> test.xdc;
	echo "set_property LOC RAMB18_X6Y$((13*i+ 8 + b))    [get_cells {name[$((e + 13))].dut/bram9}];" >> test.xdc;
	echo "set_property LOC RAMB18_X6Y$((13*i+ 9 + b))    [get_cells {name[$((e + 13))].dut/mm1/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X6Y$((13*i+10 + b))    [get_cells {name[$((e + 13))].dut/mm2/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X6Y$((13*i+11 + b))    [get_cells {name[$((e + 13))].dut/mm3/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X6Y$((13*i+12 + b))    [get_cells {name[$((e + 13))].dut/mm4/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC URAM288_X2Y$((8*i+ 4 + c))   [get_cells {name[$((e + 13))].dut/mm1/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X2Y$((8*i+ 5 + c))   [get_cells {name[$((e + 13))].dut/mm2/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X2Y$((8*i+ 6 + c))   [get_cells {name[$((e + 13))].dut/mm3/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X2Y$((8*i+ 7 + c))   [get_cells {name[$((e + 13))].dut/mm4/uram_inst_rd}];" >> test.xdc;

#######################################

done


# run 4 times
for i in {0..$Y}
do
	a=${dsp[$i+1]}
	b=${bram[$i+1]}
	c=${uram[$i+1]}

#	c1=${uram1[$i+1]}
#	c3=${uram3[$i+1]}
	e=${file1[$i+1]}
        echo "# $((i)) in 4 loop" >> test.xdc
        echo "# $((a)), $((b)), $((c)), $((e))" >> test.xdc


	echo "set_property LOC DSP48E2_X4Y$((18*i+ 0 + a))   [get_cells {name[$((e + 2))].dut/mm1/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 1 + a))   [get_cells {name[$((e + 2))].dut/mm1/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 2 + a))   [get_cells {name[$((e + 2))].dut/mm1/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 3 + a))   [get_cells {name[$((e + 2))].dut/mm1/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 4 + a))   [get_cells {name[$((e + 2))].dut/mm1/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 5 + a))   [get_cells {name[$((e + 2))].dut/mm1/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 6 + a))   [get_cells {name[$((e + 2))].dut/mm1/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 7 + a))   [get_cells {name[$((e + 2))].dut/mm1/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 8 + a))   [get_cells {name[$((e + 2))].dut/mm1/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 9 + a))   [get_cells {name[$((e + 2))].dut/mm2/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+10 + a))   [get_cells {name[$((e + 2))].dut/mm2/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+11 + a))   [get_cells {name[$((e + 2))].dut/mm2/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+12 + a))   [get_cells {name[$((e + 2))].dut/mm2/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+13 + a))   [get_cells {name[$((e + 2))].dut/mm2/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+14 + a))   [get_cells {name[$((e + 2))].dut/mm2/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+15 + a))   [get_cells {name[$((e + 2))].dut/mm2/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+16 + a))   [get_cells {name[$((e + 2))].dut/mm2/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+17 + a))   [get_cells {name[$((e + 2))].dut/mm2/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 0 + a))   [get_cells {name[$((e + 2))].dut/mm3/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 1 + a))   [get_cells {name[$((e + 2))].dut/mm3/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 2 + a))   [get_cells {name[$((e + 2))].dut/mm3/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 3 + a))   [get_cells {name[$((e + 2))].dut/mm3/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 4 + a))   [get_cells {name[$((e + 2))].dut/mm3/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 5 + a))   [get_cells {name[$((e + 2))].dut/mm3/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 6 + a))   [get_cells {name[$((e + 2))].dut/mm3/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 7 + a))   [get_cells {name[$((e + 2))].dut/mm3/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 8 + a))   [get_cells {name[$((e + 2))].dut/mm3/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 9 + a))   [get_cells {name[$((e + 2))].dut/mm4/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+10 + a))   [get_cells {name[$((e + 2))].dut/mm4/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+11 + a))   [get_cells {name[$((e + 2))].dut/mm4/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+12 + a))   [get_cells {name[$((e + 2))].dut/mm4/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+13 + a))   [get_cells {name[$((e + 2))].dut/mm4/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+14 + a))   [get_cells {name[$((e + 2))].dut/mm4/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+15 + a))   [get_cells {name[$((e + 2))].dut/mm4/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+16 + a))   [get_cells {name[$((e + 2))].dut/mm4/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+17 + a))   [get_cells {name[$((e + 2))].dut/mm4/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+ 0 + b))    [get_cells {name[$((e + 2))].dut/bram1}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+ 1 + b))    [get_cells {name[$((e + 2))].dut/bram2}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+ 2 + b))    [get_cells {name[$((e + 2))].dut/bram3}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+ 3 + b))    [get_cells {name[$((e + 2))].dut/bram4}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+ 4 + b))    [get_cells {name[$((e + 2))].dut/bram5}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+ 5 + b))    [get_cells {name[$((e + 2))].dut/bram6}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+ 6 + b))    [get_cells {name[$((e + 2))].dut/bram7}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+ 7 + b))    [get_cells {name[$((e + 2))].dut/bram8}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+ 8 + b))    [get_cells {name[$((e + 2))].dut/bram9}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+ 9 + b))    [get_cells {name[$((e + 2))].dut/mm1/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+10 + b))    [get_cells {name[$((e + 2))].dut/mm2/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+11 + b))    [get_cells {name[$((e + 2))].dut/mm3/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+12 + b))    [get_cells {name[$((e + 2))].dut/mm4/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC URAM288_X0Y$((12*i+ 8 + c))   [get_cells {name[$((e + 2))].dut/mm1/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X0Y$((12*i+ 9 + c))   [get_cells {name[$((e + 2))].dut/mm2/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X0Y$((12*i+ 10 + c))   [get_cells {name[$((e + 2))].dut/mm3/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X0Y$((12*i+ 11 + c))   [get_cells {name[$((e + 2))].dut/mm4/uram_inst_rd}];" >> test.xdc;
done

# separate entries 9 9 18
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 0 + a))   [get_cells {name[$((e + 2))].dut/mm1/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 1 + a))   [get_cells {name[$((e + 2))].dut/mm1/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 2 + a))   [get_cells {name[$((e + 2))].dut/mm1/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 3 + a))   [get_cells {name[$((e + 2))].dut/mm1/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 4 + a))   [get_cells {name[$((e + 2))].dut/mm1/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 5 + a))   [get_cells {name[$((e + 2))].dut/mm1/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 6 + a))   [get_cells {name[$((e + 2))].dut/mm1/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 7 + a))   [get_cells {name[$((e + 2))].dut/mm1/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+ 8 + a))   [get_cells {name[$((e + 2))].dut/mm1/dsp_chain[8].dsp_inst}];" >> test.xdc;

	echo "set_property LOC DSP48E2_X4Y$((18*i+0 + a))   [get_cells {name[$((e + 2))].dut/mm2/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+1 + a))   [get_cells {name[$((e + 2))].dut/mm2/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+2 + a))   [get_cells {name[$((e + 2))].dut/mm2/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+3 + a))   [get_cells {name[$((e + 2))].dut/mm2/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+4 + a))   [get_cells {name[$((e + 2))].dut/mm2/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+5 + a))   [get_cells {name[$((e + 2))].dut/mm2/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+6 + a))   [get_cells {name[$((e + 2))].dut/mm2/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+7 + a))   [get_cells {name[$((e + 2))].dut/mm2/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X4Y$((18*i+8 + a))   [get_cells {name[$((e + 2))].dut/mm2/dsp_chain[8].dsp_inst}];" >> test.xdc;

	echo "set_property LOC DSP48E2_X5Y$((18*i+ 0 + a))   [get_cells {name[$((e + 2))].dut/mm3/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 1 + a))   [get_cells {name[$((e + 2))].dut/mm3/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 2 + a))   [get_cells {name[$((e + 2))].dut/mm3/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 3 + a))   [get_cells {name[$((e + 2))].dut/mm3/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 4 + a))   [get_cells {name[$((e + 2))].dut/mm3/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 5 + a))   [get_cells {name[$((e + 2))].dut/mm3/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 6 + a))   [get_cells {name[$((e + 2))].dut/mm3/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 7 + a))   [get_cells {name[$((e + 2))].dut/mm3/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 8 + a))   [get_cells {name[$((e + 2))].dut/mm3/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+ 9 + a))   [get_cells {name[$((e + 2))].dut/mm4/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+10 + a))   [get_cells {name[$((e + 2))].dut/mm4/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+11 + a))   [get_cells {name[$((e + 2))].dut/mm4/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+12 + a))   [get_cells {name[$((e + 2))].dut/mm4/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+13 + a))   [get_cells {name[$((e + 2))].dut/mm4/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+14 + a))   [get_cells {name[$((e + 2))].dut/mm4/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+15 + a))   [get_cells {name[$((e + 2))].dut/mm4/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+16 + a))   [get_cells {name[$((e + 2))].dut/mm4/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X5Y$((18*i+17 + a))   [get_cells {name[$((e + 2))].dut/mm4/dsp_chain[8].dsp_inst}];" >> test.xdc;

	echo "set_property LOC RAMB18_X2Y$((13*i+ 0 + b))    [get_cells {name[$((e + 2))].dut/bram1}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+ 1 + b))    [get_cells {name[$((e + 2))].dut/bram2}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+ 2 + b))    [get_cells {name[$((e + 2))].dut/bram3}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+ 3 + b))    [get_cells {name[$((e + 2))].dut/bram4}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+ 4 + b))    [get_cells {name[$((e + 2))].dut/bram5}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+ 5 + b))    [get_cells {name[$((e + 2))].dut/bram6}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+ 6 + b))    [get_cells {name[$((e + 2))].dut/bram7}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+ 7 + b))    [get_cells {name[$((e + 2))].dut/bram8}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+ 8 + b))    [get_cells {name[$((e + 2))].dut/bram9}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+ 9 + b))    [get_cells {name[$((e + 2))].dut/mm1/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+10 + b))    [get_cells {name[$((e + 2))].dut/mm2/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+11 + b))    [get_cells {name[$((e + 2))].dut/mm3/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X2Y$((13*i+12 + b))    [get_cells {name[$((e + 2))].dut/mm4/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC URAM288_X0Y$((12*i+ 8 + c))   [get_cells {name[$((e + 2))].dut/mm1/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X0Y$((12*i+ 9 + c))   [get_cells {name[$((e + 2))].dut/mm2/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X0Y$((12*i+ 10 + c))   [get_cells {name[$((e + 2))].dut/mm3/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X0Y$((12*i+ 11 + c))   [get_cells {name[$((e + 2))].dut/mm4/uram_inst_rd}];" >> test.xdc;

