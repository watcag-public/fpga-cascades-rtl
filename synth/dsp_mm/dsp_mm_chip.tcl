read_verilog dsp_mm.v dsp_mm_top.v dsp_mm_chip.sv
read_xdc dsp_mm_chip.xdc;
synth_design -mode out_of_context -part xcvu37p-fsvh2892-3-e-es1 -top dsp_mm_chip;
write_checkpoint -force -file dsp_mm_chip_synth.dcp


opt_design; place_design; route_design; report_utilization; report_timing;
write_checkpoint -force -file dsp_mm_chip_place-and-route.dcp
exit
