#!/usr/bin/zsh
rm -f test.xdc


#for i in {0..2}
#do
#	a=0
#	b=0
#	c=8
#        dsp_col1=6
#        dsp_col2=7
#        bram_col=3
#        uram_col=0
#
#	e=10 #file offset
########################################
##### for left side of the chip
#
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 0 + a))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[0].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 1 + a))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[1].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 2 + a))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[2].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 3 + a))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[3].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 4 + a))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[4].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 5 + a))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[5].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 6 + a))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[6].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 7 + a))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[7].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 8 + a))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[8].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 9 + a))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[0].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+10 + a))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[1].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+11 + a))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[2].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+12 + a))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[3].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+13 + a))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[4].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+14 + a))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[5].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+15 + a))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[6].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+16 + a))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[7].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+17 + a))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[8].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 0 + a))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[0].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 1 + a))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[1].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 2 + a))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[2].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 3 + a))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[3].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 4 + a))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[4].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 5 + a))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[5].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 6 + a))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[6].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 7 + a))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[7].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 8 + a))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[8].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 9 + a))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[0].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+10 + a))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[1].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+11 + a))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[2].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+12 + a))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[3].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+13 + a))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[4].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+14 + a))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[5].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+15 + a))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[6].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+16 + a))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[7].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+17 + a))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[8].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 0 + b))    [get_cells {name[$((e + i))].dut/bram1}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 1 + b))    [get_cells {name[$((e + i))].dut/bram2}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 2 + b))    [get_cells {name[$((e + i))].dut/bram3}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 3 + b))    [get_cells {name[$((e + i))].dut/bram4}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 4 + b))    [get_cells {name[$((e + i))].dut/bram5}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 5 + b))    [get_cells {name[$((e + i))].dut/bram6}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 6 + b))    [get_cells {name[$((e + i))].dut/bram7}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 7 + b))    [get_cells {name[$((e + i))].dut/bram8}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 8 + b))    [get_cells {name[$((e + i))].dut/bram9}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 9 + b))    [get_cells {name[$((e + i))].dut/mm1/bram_inst_wr}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+10 + b))    [get_cells {name[$((e + i))].dut/mm2/bram_inst_wr}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+11 + b))    [get_cells {name[$((e + i))].dut/mm3/bram_inst_wr}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+12 + b))    [get_cells {name[$((e + i))].dut/mm4/bram_inst_wr}];" >> test.xdc;
#	echo "set_property LOC URAM288_X$((uram_col))Y$((12*i+ 0 + c))   [get_cells {name[$((e + i))].dut/mm1/uram_inst_rd}];" >> test.xdc;
#	echo "set_property LOC URAM288_X$((uram_col))Y$((12*i+ 1 + c))   [get_cells {name[$((e + i))].dut/mm2/uram_inst_rd}];" >> test.xdc;
#	echo "set_property LOC URAM288_X$((uram_col))Y$((12*i+ 2 + c))   [get_cells {name[$((e + i))].dut/mm3/uram_inst_rd}];" >> test.xdc;
#	echo "set_property LOC URAM288_X$((uram_col))Y$((12*i+ 3 + c))   [get_cells {name[$((e + i))].dut/mm4/uram_inst_rd}];" >> test.xdc;
#
#done

#for i in {0..1}
#do
#	a=54
#	b=52
#	c=56
#        dsp_off=72
#        dsp_col1=4
#        dsp_col2=5
#        dsp_col3=6
#        bram_col=2
#        uram_col=0
#
#	e=14 #file offset
########################################
##### for left side of the chip
#
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((9*i+ 0 + dsp_off))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[0].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((9*i+ 1 + dsp_off))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[1].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((9*i+ 2 + dsp_off))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[2].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((9*i+ 3 + dsp_off))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[3].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((9*i+ 4 + dsp_off))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[4].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((9*i+ 5 + dsp_off))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[5].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((9*i+ 6 + dsp_off))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[6].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((9*i+ 7 + dsp_off))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[7].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((9*i+ 8 + dsp_off))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[8].dsp_inst}];" >> test.xdc;
#
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((9*i+0 + dsp_off))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[0].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((9*i+1 + dsp_off))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[1].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((9*i+2 + dsp_off))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[2].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((9*i+3 + dsp_off))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[3].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((9*i+4 + dsp_off))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[4].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((9*i+5 + dsp_off))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[5].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((9*i+6 + dsp_off))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[6].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((9*i+7 + dsp_off))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[7].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((9*i+8 + dsp_off))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[8].dsp_inst}];" >> test.xdc;
#
#	echo "set_property LOC DSP48E2_X$((dsp_col3))Y$((18*i+ 0 + a))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[0].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col3))Y$((18*i+ 1 + a))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[1].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col3))Y$((18*i+ 2 + a))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[2].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col3))Y$((18*i+ 3 + a))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[3].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col3))Y$((18*i+ 4 + a))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[4].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col3))Y$((18*i+ 5 + a))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[5].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col3))Y$((18*i+ 6 + a))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[6].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col3))Y$((18*i+ 7 + a))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[7].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col3))Y$((18*i+ 8 + a))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[8].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col3))Y$((18*i+ 9 + a))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[0].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col3))Y$((18*i+10 + a))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[1].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col3))Y$((18*i+11 + a))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[2].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col3))Y$((18*i+12 + a))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[3].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col3))Y$((18*i+13 + a))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[4].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col3))Y$((18*i+14 + a))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[5].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col3))Y$((18*i+15 + a))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[6].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col3))Y$((18*i+16 + a))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[7].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC DSP48E2_X$((dsp_col3))Y$((18*i+17 + a))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[8].dsp_inst}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 0 + b))    [get_cells {name[$((e + i))].dut/bram1}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 1 + b))    [get_cells {name[$((e + i))].dut/bram2}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 2 + b))    [get_cells {name[$((e + i))].dut/bram3}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 3 + b))    [get_cells {name[$((e + i))].dut/bram4}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 4 + b))    [get_cells {name[$((e + i))].dut/bram5}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 5 + b))    [get_cells {name[$((e + i))].dut/bram6}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 6 + b))    [get_cells {name[$((e + i))].dut/bram7}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 7 + b))    [get_cells {name[$((e + i))].dut/bram8}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 8 + b))    [get_cells {name[$((e + i))].dut/bram9}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 9 + b))    [get_cells {name[$((e + i))].dut/mm1/bram_inst_wr}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+10 + b))    [get_cells {name[$((e + i))].dut/mm2/bram_inst_wr}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+11 + b))    [get_cells {name[$((e + i))].dut/mm3/bram_inst_wr}];" >> test.xdc;
#	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+12 + b))    [get_cells {name[$((e + i))].dut/mm4/bram_inst_wr}];" >> test.xdc;
#	echo "set_property LOC URAM288_X$((uram_col))Y$((4*i+ 0 + c))   [get_cells {name[$((e + i))].dut/mm1/uram_inst_rd}];" >> test.xdc;
#	echo "set_property LOC URAM288_X$((uram_col))Y$((4*i+ 1 + c))   [get_cells {name[$((e + i))].dut/mm2/uram_inst_rd}];" >> test.xdc;
#	echo "set_property LOC URAM288_X$((uram_col))Y$((4*i+ 2 + c))   [get_cells {name[$((e + i))].dut/mm3/uram_inst_rd}];" >> test.xdc;
#	echo "set_property LOC URAM288_X$((uram_col))Y$((4*i+ 3 + c))   [get_cells {name[$((e + i))].dut/mm4/uram_inst_rd}];" >> test.xdc;
#
#done

col1_ar=(27 27 28 28 28 )
col2_ar=(28 28 29 29 29 )
dsp_ar1=(0 0 0 0 0 )
dsp_ar2=(0 0 0 0 0 )
bram_ar=(0 13 13 13 13 )
bramcol_ar=(13 12 12 12 12 )
for i in {0..4}
do
	#a=0
        a1=${dsp_ar1[$i+1]}
        a2=${dsp_ar2[$i+1]}
	b=${bram_ar[$i+1]}
	c=4
        dsp_col1=${col1_ar[$i+1]}
        dsp_col2=${col2_ar[$i+1]}
        bram_col=${bramcol_ar[$i+1]}
        uram_col=4

	e=69 #file offset
#######################################
#### for left side of the chip
        echo "####### $((i))" >> test.xdc;

	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 0 + a1))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 1 + a1))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 2 + a1))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 3 + a1))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 4 + a1))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 5 + a1))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 6 + a1))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 7 + a1))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 8 + a1))   [get_cells {name[$((e + i))].dut/mm1/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+ 9 + a1))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+10 + a1))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+11 + a1))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+12 + a1))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+13 + a1))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+14 + a1))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+15 + a1))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+16 + a1))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col1))Y$((18*i+17 + a1))   [get_cells {name[$((e + i))].dut/mm2/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 0 + a2))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 1 + a2))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 2 + a2))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 3 + a2))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 4 + a2))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 5 + a2))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 6 + a2))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 7 + a2))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 8 + a2))   [get_cells {name[$((e + i))].dut/mm3/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+ 9 + a2))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[0].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+10 + a2))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[1].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+11 + a2))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[2].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+12 + a2))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[3].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+13 + a2))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[4].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+14 + a2))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[5].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+15 + a2))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[6].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+16 + a2))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[7].dsp_inst}];" >> test.xdc;
	echo "set_property LOC DSP48E2_X$((dsp_col2))Y$((18*i+17 + a2))   [get_cells {name[$((e + i))].dut/mm4/dsp_chain[8].dsp_inst}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 0 + b))    [get_cells {name[$((e + i))].dut/bram1}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 1 + b))    [get_cells {name[$((e + i))].dut/bram2}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 2 + b))    [get_cells {name[$((e + i))].dut/bram3}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 3 + b))    [get_cells {name[$((e + i))].dut/bram4}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 4 + b))    [get_cells {name[$((e + i))].dut/bram5}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 5 + b))    [get_cells {name[$((e + i))].dut/bram6}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 6 + b))    [get_cells {name[$((e + i))].dut/bram7}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 7 + b))    [get_cells {name[$((e + i))].dut/bram8}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 8 + b))    [get_cells {name[$((e + i))].dut/bram9}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+ 9 + b))    [get_cells {name[$((e + i))].dut/mm1/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+10 + b))    [get_cells {name[$((e + i))].dut/mm2/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+11 + b))    [get_cells {name[$((e + i))].dut/mm3/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC RAMB18_X$((bram_col))Y$((13*i+12 + b))    [get_cells {name[$((e + i))].dut/mm4/bram_inst_wr}];" >> test.xdc;
	echo "set_property LOC URAM288_X$((uram_col))Y$((12*i+ 0 + c))   [get_cells {name[$((e + i))].dut/mm1/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X$((uram_col))Y$((12*i+ 1 + c))   [get_cells {name[$((e + i))].dut/mm2/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X$((uram_col))Y$((12*i+ 2 + c))   [get_cells {name[$((e + i))].dut/mm3/uram_inst_rd}];" >> test.xdc;
	echo "set_property LOC URAM288_X$((uram_col))Y$((12*i+ 3 + c))   [get_cells {name[$((e + i))].dut/mm4/uram_inst_rd}];" >> test.xdc;

done



