set_property SRC_FILE_INFO {cfile:/home/t3garg/Work/systolic-array-vu9p_local/synth/dsp_conv_block/dsp_conv_chip.xdc rfile:../dsp_conv_chip.xdc id:1} [current_design]
set_property src_info {type:XDC file:1 line:3 export:INPUT save:INPUT read:READ} [current_design]
set_property LOC DSP48E2_X0Y0   [get_cells {name[0].dut/conv1/dsp_chain0[0].dsp_inst}];
set_property src_info {type:XDC file:1 line:4 export:INPUT save:INPUT read:READ} [current_design]
set_property LOC DSP48E2_X0Y1   [get_cells {name[0].dut/conv1/dsp_chain0[1].dsp_inst}];
set_property src_info {type:XDC file:1 line:5 export:INPUT save:INPUT read:READ} [current_design]
set_property LOC DSP48E2_X0Y2   [get_cells {name[0].dut/conv1/dsp_chain0[2].dsp_inst}];
set_property src_info {type:XDC file:1 line:6 export:INPUT save:INPUT read:READ} [current_design]
set_property LOC DSP48E2_X0Y3   [get_cells {name[0].dut/conv1/dsp_chain1[0].dsp_inst}];
set_property src_info {type:XDC file:1 line:7 export:INPUT save:INPUT read:READ} [current_design]
set_property LOC DSP48E2_X0Y4   [get_cells {name[0].dut/conv1/dsp_chain1[1].dsp_inst}];
set_property src_info {type:XDC file:1 line:8 export:INPUT save:INPUT read:READ} [current_design]
set_property LOC DSP48E2_X0Y5   [get_cells {name[0].dut/conv1/dsp_chain1[2].dsp_inst}];
set_property src_info {type:XDC file:1 line:9 export:INPUT save:INPUT read:READ} [current_design]
set_property LOC DSP48E2_X0Y6   [get_cells {name[0].dut/conv1/dsp_chain2[0].dsp_inst}];
set_property src_info {type:XDC file:1 line:10 export:INPUT save:INPUT read:READ} [current_design]
set_property LOC DSP48E2_X0Y7   [get_cells {name[0].dut/conv1/dsp_chain2[1].dsp_inst}];
set_property src_info {type:XDC file:1 line:11 export:INPUT save:INPUT read:READ} [current_design]
set_property LOC DSP48E2_X0Y8   [get_cells {name[0].dut/conv1/dsp_inst8}];
set_property src_info {type:XDC file:1 line:12 export:INPUT save:INPUT read:READ} [current_design]
set_property LOC RAMB18_X0Y0     [get_cells {name[0].dut/conv1/bram_inst_rdc1}];
set_property src_info {type:XDC file:1 line:13 export:INPUT save:INPUT read:READ} [current_design]
set_property LOC RAMB18_X0Y2     [get_cells {name[0].dut/conv1/bram_inst_rdc2}];
set_property src_info {type:XDC file:1 line:14 export:INPUT save:INPUT read:READ} [current_design]
set_property LOC RAMB18_X0Y4     [get_cells {name[0].dut/conv1/bram_inst_rdc3}];
set_property src_info {type:XDC file:1 line:15 export:INPUT save:INPUT read:READ} [current_design]
set_property LOC RAMB18_X0Y6     [get_cells {name[0].dut/conv1/bram_inst_rdc4}];
set_property src_info {type:XDC file:1 line:16 export:INPUT save:INPUT read:READ} [current_design]
set_property LOC URAM288_X0Y0    [get_cells {name[0].dut/uram_inst_wr}];
set_property src_info {type:XDC file:1 line:17 export:INPUT save:INPUT read:READ} [current_design]
set_property LOC URAM288_X0Y1    [get_cells {name[0].dut/uram_inst_rd}];
