read_verilog addr_gen.v dsp_conv_nocasc.v dsp_conv_nocasc_top.v dsp_conv_nocasc_chip.sv
read_xdc dsp_conv_nocasc_chip.xdc;
synth_design -mode out_of_context -part xcvu37p-fsvh2892-3-e-es1 -top dsp_conv_nocasc_chip;
write_checkpoint -force -file dsp_conv_nocasc_chip_synth.dcp


opt_design; place_design; route_design; report_utilization; report_timing;
write_checkpoint -force -file dsp_conv_nocasc_chip_place-and-route.dcp
exit
