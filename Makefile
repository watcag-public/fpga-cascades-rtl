clean:
	rm -Rf *.log *.jou

dspconv_top:
	PWD=`pwd`; mkdir -p bin/dspconv_top_solo; cd bin/dspconv_top_solo; \
	cp $(PWD)/sim/new_pixels_*.hex . ; \
	cp $(PWD)/sim/k_pixels_*.hex . ; \
	verilator -trace -CFLAGS "-DTRACE" -Wno-WIDTH -Wno-PINMISSING -Wno-REALCVT -Wno-MULTIDRIVEN -Wno-PINMISSING -Wno-INITIALDLY -Wno-COMBDLY -Wno-STMTDLY -Wno-UNOPTFLAT -Wno-CASEX -I$(PWD)/sim -cc dsp_conv_top --exe $(PWD)/sim/tb/dsp_conv_top_tb.c --top-module dsp_conv_top ; \
	make -C obj_dir -j -f Vdsp_conv_top.mk Vdsp_conv_top ;\
	./obj_dir/Vdsp_conv_top > log.txt | cat ;\
	cd $(PWD)

dspconvnocasc_top:
	PWD=`pwd`; mkdir -p bin/dspconvnocasc_top_solo; cd bin/dspconvnocasc_top_solo; \
	cp $(PWD)/sim/new_pixels_*.hex . ; \
	cp $(PWD)/sim/k_pixels_*.hex . ; \
	verilator -trace -CFLAGS "-DTRACE" -Wno-WIDTH -Wno-PINMISSING -Wno-REALCVT -Wno-MULTIDRIVEN -Wno-PINMISSING -Wno-INITIALDLY -Wno-COMBDLY -Wno-STMTDLY -Wno-UNOPTFLAT -Wno-CASEX -I$(PWD)/sim -cc dsp_conv_nocasc_top --exe $(PWD)/sim/tb/dsp_conv_nocasc_top_tb.c --top-module dsp_conv_nocasc_top ; \
	make -C obj_dir -j -f Vdsp_conv_nocasc_top.mk Vdsp_conv_nocasc_top ;\
	./obj_dir/Vdsp_conv_nocasc_top > log.txt | cat ;\
	cd $(PWD)

dspmm:
	PWD=`pwd`; mkdir -p bin/dspmm_solo; cd bin/dspmm_solo; \
	cp $(PWD)/sim/new_pixels_*.hex . ; \
	cp $(PWD)/sim/k_pixels_*.hex . ; \
	verilator -trace -CFLAGS "-DTRACE" -Wno-WIDTH -Wno-PINMISSING -Wno-REALCVT -Wno-MULTIDRIVEN -Wno-PINMISSING -Wno-INITIALDLY -Wno-COMBDLY -Wno-STMTDLY -Wno-UNOPTFLAT -Wno-CASEX -I$(PWD)/sim -cc dsp_mm_top --exe $(PWD)/sim/tb/dsp_mm_top_tb.c --top-module dsp_mm_top ; \
	make -C obj_dir -j -f Vdsp_mm_top.mk Vdsp_mm_top ;\
	./obj_dir/Vdsp_mm_top > log.txt | cat ;\
	cd $(PWD)
